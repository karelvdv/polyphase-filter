# A Polyphase Filter for GPUs and Multi-Core Processors
(c) 2012 Karel van der Veldt  
Licensed under GPLv3


## 1. DESCRIPTION & NOTES

This a short explanation on how to compile the code for my thesis and some other notes.

Compiling for CUDA and OpenCL takes a long time, you will just have to wait until it's finished.

The OpenCL .cl files are (pre)compiled along with the rest of the program. It's done this way because recompiling the .cl files every time the program runs takes quite some time. When the OpenCL implementation is compiled it creates a few extra files in the src/opencl folder called ppf_k_*.h and ppf_k_*.bin. They are created from the ppf_k_*.cl files. If you change those you have to delete the ppf_k_*.h files so they are recreated. It's done this way to speed up recompilations when the .cl files don't change.

The OpenCL implementation automatically compiles for either NVIDIA or ATI depending on which API/drivers are installed.

The source code and everything else related to my thesis is licensed under the GPLv3 license, unless stated otherwise.



## 2. INSTRUCTIONS

Quickstart:

1. cd to the folder this readme is in.
2. (CUDA only) svn co https://gforge.sci.utah.edu/svn/findcuda/trunk@1177 src/cuda/FindCUDA
3. mkdir build
4. cd build
5. cmake ../src -D...=ON (read sections 3 and 4 first)
6. make
7. binaries are in ../



## 3. BUILD FLAGS

CUDA and OpenCL have additional build flags defined in CMakeLists.txt in the appropriate folders. If you change a build flag you have to run make again. There are a few more than the ones mentioned but they are only for testing.


### 3.1 CUDA BUILD FLAGS

OPT_FLAGS:

* -DPAGELOCKED_MEMORY -- Allocate the input buffer as pagelocked/pinned. IMPORTANT: If enabled I/O transfers implicitly *always* occur, regardless of other I/O settings.


### 3.2 OpenCL BUILD FLAGS

OPT_FLAGS:

* -DPAGELOCKED_MEMORY -- Allocate the input buffer as pagelocked/pinned. IMPORTANT: If enabled I/O transfers implicitly *always* occur, regardless of other I/O settings.

OPENCL_FFT: Defines which FFT library to use.

* 0 -- Apple
* 1 -- AMD
* 2 -- Fixstars


## 4. CMAKE OPTIONS

You must choose which implementations you want to build by setting the appropriate cmake flags:

* -DCPU=ON
* -DCUDA=ON
* -DMICROGRID=ON
* -DOPENCL=ON

Example:

> cmake ../src -DCPU=ON -DCUDA=ON


## 5. RUNNING THE PROGRAM

Run the program without arguments to get a list of execution modes. The exact arguments depend on the platform.