/*
    A Polyphase Filter for GPUs and Multi-Core Processors
    Copyright (C) 2012 Karel van der Veldt (karel.vd.veldt@uva.nl)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
//#include <stdio.h>
#include <stdlib.h>
//#include <memory.h>
#include <assert.h>
#include <svp/delegate.h>
#include "fir.h"

static inline int min(int a, int b)
{
	if(a > b) return b;
	return a;
}

struct placeinfo* alloc_place(struct SEP* sep, int ncores)
{
	sl_create(, sep->sep_place,,,,, sl__exclusive,
		*sep->sep_alloc,
		sl_glarg(struct SEP*,, sep),
		sl_glarg(unsigned long,, SAL_MAX | ncores),
		sl_sharg(struct placeinfo*, p, 0)
	);
	sl_sync();
	return sl_geta(p);
}

/**
 *	Allocate a new FIR structure.
 */
ppf_fir_t* ppf_fir_new(int nr_stations, int nr_channels, int nr_taps, int* blocks)
{
	unsigned int delaylen = nr_stations * nr_channels * nr_taps * NR_POLARIZATIONS;
	complex_f32* RESTRICT delay = ppf_malloc(complex_f32, delaylen);
	assert(delay);
	
	//memset(delay, 0, delaylen);
	for(unsigned int i = 0; i < delaylen; ++i)
	{
		delay[i].re = 0;
		delay[i].im = 0;
	}
	
	ppf_fir_t* fir = ppf_malloc(ppf_fir_t, 1);
	assert(fir);
	
	fir->nr_stations = nr_stations;
	fir->nr_channels = nr_channels;
	fir->nr_taps = nr_taps;
	fir->delay_index = 0;
	fir->delay = delay;
	fir->table_size = 0;
	
	int poweroftwo = nr_channels >> 1;
	while(poweroftwo)
	{
		fir->table_size++;
		poweroftwo >>= 1;
	}
	
	fir->station_block = blocks[0];
	fir->channel_block = blocks[1];
	fir->fft_block     = blocks[2];
	
#if SVP_HAS_SEP && defined(USE_SEP)
	fir->station_place = alloc_place( root_sep, nr_stations );
	assert(fir->station_place);
	fir->station_pid = fir->station_place->pid;

	/*fir->channel_place = alloc_place( root_sep, min(nr_channels, 64) );
	assert(fir->channel_place);
	fir->channel_pid = fir->channel_place->pid;*/
	fir->channel_pid = PLACE_LOCAL;
#else
	fir->station_pid   = PLACE_DEFAULT;
	fir->channel_pid   = PLACE_LOCAL;
#endif
	
	return fir;
}

void ppf_fir_free(ppf_fir_t* fir)
{
	free(fir->delay);
	free(fir);
}

complex_i4* ppf_input_new_i4(int nr_stations, int nr_channels)
{
	complex_i4* p = ppf_malloc(complex_i4,
		nr_stations * nr_channels * NR_POLARIZATIONS);
	assert(p);
	return p;
}

complex_i8* ppf_input_new_i8(int nr_stations, int nr_channels)
{
	complex_i8* p = ppf_malloc(complex_i8,
		nr_stations * nr_channels * NR_POLARIZATIONS);
	assert(p);
	return p;
}

complex_i16* ppf_input_new_i16(int nr_stations, int nr_channels)
{
	complex_i16* p = ppf_malloc(complex_i16,
		nr_stations * nr_channels * NR_POLARIZATIONS);
	assert(p);
	return p;
}

complex_f32* ppf_output_new(int nr_stations, int nr_channels)
{
	complex_f32* p = ppf_malloc(complex_f32,
		nr_stations * nr_channels * NR_POLARIZATIONS);
	assert(p);
	return p;
}

/**
 *	Compute taps in parallel (many threads required).
 */
#ifdef FIR_THREADS
sl_def(fir_compute, sl__static,
	sl_glparm(const complex_f32* RESTRICT, delay),
	sl_glparm(const float* RESTRICT, weights),
	sl_glparm(int, delay_index),
	sl_glparm(int, nr_taps),
	sl_shparm(float, sum0r),
	sl_shparm(float, sum0i),
	sl_shparm(float, sum1r),
	sl_shparm(float, sum1i),
	)
{
	sl_index(i);
	unsigned int delaydex = (i + sl_getp(delay_index)) & (sl_getp(nr_taps) - 1);
	float w        = sl_getp(weights)[i];
	complex_f32 t0 = sl_getp(delay)[delaydex * NR_POLARIZATIONS + 0];
	complex_f32 t1 = sl_getp(delay)[delaydex * NR_POLARIZATIONS + 1];
	
	float sum0r = sl_getp(sum0r);
	float sum0i = sl_getp(sum0i);
	float sum1r = sl_getp(sum1r);
	float sum1i = sl_getp(sum1i);
	
	sum0r += t0.re * w; // complex + complex * real
	sum0i += t0.im * w;
	sum1r += t1.re * w; // complex + complex * real
	sum1i += t1.im * w;
	
	sl_setp(sum0r, sum0r);
	sl_setp(sum0i, sum0i);
	sl_setp(sum1r, sum1r);
	sl_setp(sum1i, sum1i);
}
sl_enddef

/**
 *	Compute taps in sequence (fewer threads required).
 */
#else
static inline void fir_compute(const complex_f32* RESTRICT delay0,
	const float* RESTRICT weights, int delay_index, int nr_taps,
	complex_f32 sum0, complex_f32 sum1,
	complex_f32* RESTRICT output, unsigned int outdex0, unsigned int outdex1)
{
	complex_f32 tmp0, tmp1;
	const complex_f32* RESTRICT delay = delay0 + (delay_index + 1) * NR_POLARIZATIONS;
	float w = *weights++;
	
	sum0.re *= w;	// 4 fp ops
	sum0.im *= w;
	sum1.re *= w;
	sum1.im *= w;
	
#ifdef UNROLL_LOOPS
	int i = delay_index + 1;
	
	switch(i & 3)
	{
	// fallthrough is intentional
	case 1:
		w        = *weights++;
		tmp0     = delay[0];
		tmp1     = delay[1];
		sum0.re += tmp0.re * w;	// 8 fp ops
		sum0.im += tmp0.im * w;
		sum1.re += tmp1.re * w;
		sum1.im += tmp1.im * w;
		delay   += NR_POLARIZATIONS;
		i++;
	case 2:
		w        = *weights++;
		tmp0     = delay[0];
		tmp1     = delay[1];
		sum0.re += tmp0.re * w;
		sum0.im += tmp0.im * w;
		sum1.re += tmp1.re * w;
		sum1.im += tmp1.im * w;
		delay   += NR_POLARIZATIONS;
		i++;
	case 3:
		w        = *weights++;
		tmp0     = delay[0];
		tmp1     = delay[1];
		sum0.re += tmp0.re * w;
		sum0.im += tmp0.im * w;
		sum1.re += tmp1.re * w;
		sum1.im += tmp1.im * w;
		delay   += NR_POLARIZATIONS;
		i++;
	default:
		break;
	};
	
	for(; i < nr_taps; i += 4)
	{
		w        = *weights++;
		tmp0     = delay[0];
		tmp1     = delay[1];
		sum0.re += tmp0.re * w;
		sum0.im += tmp0.im * w;
		sum1.re += tmp1.re * w;
		sum1.im += tmp1.im * w;
		delay   += NR_POLARIZATIONS;
		
		w        = *weights++;
		tmp0     = delay[0];
		tmp1     = delay[1];
		sum0.re += tmp0.re * w;
		sum0.im += tmp0.im * w;
		sum1.re += tmp1.re * w;
		sum1.im += tmp1.im * w;
		delay   += NR_POLARIZATIONS;
		
		w        = *weights++;
		tmp0     = delay[0];
		tmp1     = delay[1];
		sum0.re += tmp0.re * w;
		sum0.im += tmp0.im * w;
		sum1.re += tmp1.re * w;
		sum1.im += tmp1.im * w;
		delay   += NR_POLARIZATIONS;
		
		w        = *weights++;
		tmp0     = delay[0];
		tmp1     = delay[1];
		sum0.re += tmp0.re * w;
		sum0.im += tmp0.im * w;
		sum1.re += tmp1.re * w;
		sum1.im += tmp1.im * w;
		delay   += NR_POLARIZATIONS;
	}
	
	delay = delay0;
	for(; i < (delay_index & ~3); i += 4)
	{
		w        = *weights++;
		tmp0     = delay[0];
		tmp1     = delay[1];
		sum0.re += tmp0.re * w;
		sum0.im += tmp0.im * w;
		sum1.re += tmp1.re * w;
		sum1.im += tmp1.im * w;
		delay   += NR_POLARIZATIONS;
		
		w        = *weights++;
		tmp0     = delay[0];
		tmp1     = delay[1];
		sum0.re += tmp0.re * w;
		sum0.im += tmp0.im * w;
		sum1.re += tmp1.re * w;
		sum1.im += tmp1.im * w;
		delay   += NR_POLARIZATIONS;
		
		w        = *weights++;
		tmp0     = delay[0];
		tmp1     = delay[1];;
		sum0.re += tmp0.re * w;
		sum0.im += tmp0.im * w;
		sum1.re += tmp1.re * w;
		sum1.im += tmp1.im * w;
		delay   += NR_POLARIZATIONS;
		
		w        = *weights++;
		tmp0     = delay[0];
		tmp1     = delay[1];
		sum0.re += tmp0.re * w;
		sum0.im += tmp0.im * w;
		sum1.re += tmp1.re * w;
		sum1.im += tmp1.im * w;
		delay   += NR_POLARIZATIONS;
	}
	
	switch(i & 3)
	{
	// fallthrough is intentional
	case 1:
		w        = *weights++;
		tmp0     = delay[0];
		tmp1     = delay[1];
		sum0.re += tmp0.re * w;
		sum0.im += tmp0.im * w;
		sum1.re += tmp1.re * w;
		sum1.im += tmp1.im * w;
		delay   += NR_POLARIZATIONS;
		//i++;
	case 2:
		w        = *weights++;
		tmp0     = delay[0];
		tmp1     = delay[1];
		sum0.re += tmp0.re * w;
		sum0.im += tmp0.im * w;
		sum1.re += tmp1.re * w;
		sum1.im += tmp1.im * w;
		delay   += NR_POLARIZATIONS;
		//i++;
	case 3:
		w        = *weights;//++;
		tmp0     = delay[0];
		tmp1     = delay[1];
		sum0.re += tmp0.re * w;
		sum0.im += tmp0.im * w;
		sum1.re += tmp1.re * w;
		sum1.im += tmp1.im * w;
		//delay   += NR_POLARIZATIONS;
		//i++;
	default:
		break;
	};

#else // UNROLL_LOOPS
	for(int i = delay_index + 1; i < nr_taps; ++i)
	{
		w        = *weights++;
		tmp0     = delay[0];
		tmp1     = delay[1];
		sum0.re += tmp0.re * w;
		sum0.im += tmp0.im * w;
		sum1.re += tmp1.re * w;
		sum1.im += tmp1.im * w;
		delay += NR_POLARIZATIONS;
	}
	
	delay = delay0;
	for(int i = 0; i < delay_index; ++i)
	{
		w        = *weights++;
		tmp0     = delay[0];
		tmp1     = delay[1];
		sum0.re += tmp0.re * w;
		sum0.im += tmp0.im * w;
		sum1.re += tmp1.re * w;
		sum1.im += tmp1.im * w;
		delay += NR_POLARIZATIONS;
	}
#endif
	
	output[outdex0] = sum0;
	output[outdex1] = sum1;
	
	//return sum;
}
#endif

static inline void fir_compute_channel(const ppf_fir_t* fir,
	const float* RESTRICT weights, complex_f32* RESTRICT output,
	const complex_f32* RESTRICT delay,
	unsigned int outdex0, unsigned int outdex1, int nr_taps, int delay_index,
	complex_f32 sum0, complex_f32 sum1)
{
#ifdef FIR_THREADS
	sl_create(, PLACE_LOCAL, 0, nr_taps, 1,,,
		fir_compute,
		sl_glarg(const complex_f32* RESTRICT,, delay),
		sl_glarg(const float* RESTRICT,, weights),
		sl_glarg(int,, delay_index),
		sl_glarg(int,, nr_taps),
		sl_sharg(float, sum0r, 0.0),
		sl_sharg(float, sum0i, 0.0),
		sl_sharg(float, sum1r, 0.0),
		sl_sharg(float, sum1i, 0.0),
	);
	/*sl_seta(sum0r, 0.0);
	sl_seta(sum0i, 0.0);
	sl_seta(sum1r, 0.0);
	sl_seta(sum1i, 0.0);*/
	sl_sync();
	output[outdex0].re = sl_geta(sum0r);
	output[outdex0].im = sl_geta(sum0i);
	output[outdex1].re = sl_geta(sum1r);
	output[outdex1].im = sl_geta(sum1i);
#else
	fir_compute(delay, weights, delay_index, nr_taps, sum0, sum1, output, outdex0, outdex1);
#endif
}



//************* NEW METHOD

#ifdef NEW_STYLE
/**
 *	Converts input samples to float and stores them in the delay lines.
 */
sl_def(fir_convert_i16, sl__static,
	sl_glparm(const complex_i16* RESTRICT, input), // start of the station's input
	sl_glparm(complex_f32* RESTRICT, delay), // start of the station's delay lines
	sl_glparm(unsigned int, nr_channels),
	sl_glparm(unsigned int, nr_taps),
	sl_glparm(unsigned int, delay_index)
	)
{
	// 3/1024 registers
	sl_index(channel); // 0 .. nr_channels.
	unsigned int nr_channels          = sl_getp(nr_channels);
	unsigned int delay_index          = sl_getp(delay_index);
	const complex_i16* RESTRICT input = sl_getp(input) + ppf_input_nth(nr_channels, 0, channel, 0);
	complex_f32* RESTRICT delay       = sl_getp(delay) + ppf_delay_nth(nr_channels, sl_getp(nr_taps),
		0, channel) + delay_index * NR_POLARIZATIONS;
	delay[0].re = (float)input[0].re;
	delay[0].im = (float)input[0].im;
	delay[1].re = (float)input[1].re;
	delay[1].im = (float)input[1].im;
}
sl_enddef

sl_def(fir_compute_channel_i16, sl__static,
	sl_glparm(const float* RESTRICT, weights),
	sl_glparm(const complex_f32* RESTRICT, delay),
	sl_glparm(complex_f32* RESTRICT, output),
	sl_glparm(unsigned int, nr_channels),
	sl_glparm(unsigned int, nr_taps),
	sl_glparm(unsigned int, delay_index),
	sl_glparm(unsigned int, output_stride)
	)
{
	// 9/1024 registers
	sl_index(channel); // 0 .. nr_channels.
	unsigned int nr_taps = sl_getp(nr_taps);
	unsigned int delay_index = sl_getp(delay_index);
	const float* RESTRICT weights      = sl_getp(weights) + channel * nr_taps;
	const complex_f32* RESTRICT delay0 = sl_getp(delay) + ppf_delay_nth(sl_getp(nr_channels), nr_taps,
		0, channel);
	const complex_f32* RESTRICT delay  = delay0 + delay_index * NR_POLARIZATIONS;
	complex_f32* RESTRICT output       = sl_getp(output) + ppf_output_nth(0, sl_getp(nr_channels), 0, channel, 0);
	complex_f32 sum0 = delay[0];
	complex_f32 sum1 = delay[1];
	float w = *weights++;
	
	sum0.re *= w;
	sum0.im *= w;
	sum1.re *= w;
	sum1.im *= w;
	delay += NR_POLARIZATIONS;
	
	
#ifdef UNROLL_LOOPS
	unsigned int i = delay_index + 1;
	
	switch(i & 3)
	{
	// fallthrough is intentional
	case 1:
		w        = *weights++;
		sum0.re += delay[0].re * w;	// 8 fp ops
		sum0.im += delay[0].im * w;
		sum1.re += delay[1].re * w;
		sum1.im += delay[1].im * w;
		delay   += NR_POLARIZATIONS;
		i++;
	case 2:
		w        = *weights++;
		sum0.re += delay[0].re * w;
		sum0.im += delay[0].im * w;
		sum1.re += delay[1].re * w;
		sum1.im += delay[1].im * w;
		delay   += NR_POLARIZATIONS;
		i++;
	case 3:
		w        = *weights++;
		sum0.re += delay[0].re * w;
		sum0.im += delay[0].im * w;
		sum1.re += delay[1].re * w;
		sum1.im += delay[1].im * w;
		delay   += NR_POLARIZATIONS;
		i++;
	default:
		break;
	};
	
	for(; i < nr_taps; i += 4)
	{
		w        = *weights++;
		sum0.re += delay[0].re * w;
		sum0.im += delay[0].im * w;
		sum1.re += delay[1].re * w;
		sum1.im += delay[1].im * w;
		delay   += NR_POLARIZATIONS;
		
		w        = *weights++;
		sum0.re += delay[0].re * w;
		sum0.im += delay[0].im * w;
		sum1.re += delay[1].re * w;
		sum1.im += delay[1].im * w;
		delay   += NR_POLARIZATIONS;
		
		w        = *weights++;
		sum0.re += delay[0].re * w;
		sum0.im += delay[0].im * w;
		sum1.re += delay[1].re * w;
		sum1.im += delay[1].im * w;
		delay   += NR_POLARIZATIONS;
		
		w        = *weights++;
		sum0.re += delay[0].re * w;
		sum0.im += delay[0].im * w;
		sum1.re += delay[1].re * w;
		sum1.im += delay[1].im * w;
		delay   += NR_POLARIZATIONS;
	}
	
	delay = delay0;
	for(; i < (delay_index & ~3); i += 4)
	{
		w        = *weights++;
		sum0.re += delay[0].re * w;
		sum0.im += delay[0].im * w;
		sum1.re += delay[1].re * w;
		sum1.im += delay[1].im * w;
		delay   += NR_POLARIZATIONS;
		
		w        = *weights++;
		sum0.re += delay[0].re * w;
		sum0.im += delay[0].im * w;
		sum1.re += delay[1].re * w;
		sum1.im += delay[1].im * w;
		delay   += NR_POLARIZATIONS;
		
		w        = *weights++;
		sum0.re += delay[0].re * w;
		sum0.im += delay[0].im * w;
		sum1.re += delay[1].re * w;
		sum1.im += delay[1].im * w;
		delay   += NR_POLARIZATIONS;
		
		w        = *weights++;
		sum0.re += delay[0].re * w;
		sum0.im += delay[0].im * w;
		sum1.re += delay[1].re * w;
		sum1.im += delay[1].im * w;
		delay   += NR_POLARIZATIONS;
	}
	
	switch(i & 3)
	{
	// fallthrough is intentional
	case 1:
		w        = *weights++;
		sum0.re += delay[0].re * w;
		sum0.im += delay[0].im * w;
		sum1.re += delay[1].re * w;
		sum1.im += delay[1].im * w;
		delay   += NR_POLARIZATIONS;
		//i++;
	case 2:
		w        = *weights++;
		sum0.re += delay[0].re * w;
		sum0.im += delay[0].im * w;
		sum1.re += delay[1].re * w;
		sum1.im += delay[1].im * w;
		delay   += NR_POLARIZATIONS;
		//i++;
	case 3:
		w        = *weights;//++;
		sum0.re += delay[0].re * w;
		sum0.im += delay[0].im * w;
		sum1.re += delay[1].re * w;
		sum1.im += delay[1].im * w;
		//delay   += NR_POLARIZATIONS;
		//i++;
	default:
		break;
	};

#else // UNROLL_LOOPS
	for(unsigned int i = delay_index + 1; i < nr_taps; ++i)
	{
		w        = *weights++;
		sum0.re += delay[0].re * w;
		sum0.im += delay[0].im * w;
		sum1.re += delay[1].re * w;
		sum1.im += delay[1].im * w;
		delay += NR_POLARIZATIONS;
	}
	
	delay = delay0;
	for(unsigned int i = 0; i < delay_index; ++i)
	{
		w        = *weights++;
		sum0.re += delay[0].re * w;
		sum0.im += delay[0].im * w;
		sum1.re += delay[1].re * w;
		sum1.im += delay[1].im * w;
		delay += NR_POLARIZATIONS;
	}
#endif
	
	output[0]                      = sum0;
	output[sl_getp(output_stride)] = sum1;
}
sl_enddef


sl_def(fir_compute_station_i16, sl__static,
	sl_glparm(const ppf_fir_t*, fir),
	sl_glparm(const complex_i16* RESTRICT, input),
	sl_glparm(const float* RESTRICT, weights),
	sl_glparm(complex_f32* RESTRICT, output),
	sl_glparm(int, delay_index)
	)
{
	sl_index(station);
	const ppf_fir_t* fir              = sl_getp(fir);
	const complex_i16* RESTRICT input = sl_getp(input)  + ppf_input_nth(fir->nr_channels,  station, 0, 0);
	complex_f32* RESTRICT delay       = fir->delay      + ppf_delay_nth(fir->nr_channels,  fir->nr_taps, station, 0);
	complex_f32* RESTRICT output      = sl_getp(output) + ppf_output_nth(fir->nr_stations, fir->nr_channels, station,
		0, 0);
	unsigned int output_stride        = ppf_output_nth(fir->nr_stations, fir->nr_channels, 0, 0, 1);
	
	sl_create(, PLACE_LOCAL, 0, fir->nr_channels, 1, fir->channel_block,,
		fir_convert_i16,
		sl_glarg(const complex_i16* RESTRICT,, input),
		sl_glarg(complex_f32* RESTRICT,, delay),
		sl_glarg(unsigned int,, fir->nr_channels),
		sl_glarg(unsigned int,, fir->nr_taps),
		sl_glarg(unsigned int,, sl_getp(delay_index))
	);
	sl_sync();
	
	sl_create(, PLACE_LOCAL, 0, fir->nr_channels, 1, fir->channel_block,,
		fir_compute_channel_i16,
		sl_glarg(const float* RESTRICT,, sl_getp(weights)),
		sl_glarg(const complex_f32* RESTRICT,, fir->delay),
		sl_glarg(complex_f32* RESTRICT,, output),
		sl_glarg(unsigned int,, fir->nr_channels),
		sl_glarg(unsigned int,, fir->nr_taps),
		sl_glarg(unsigned int,, sl_getp(delay_index)),
		sl_glarg(unsigned int,, output_stride)
	);
	sl_sync();
}
sl_enddef


void ppf_fir_compute_i16(ppf_fir_t* fir, const complex_i16* RESTRICT input,
	const float* RESTRICT weights, complex_f32* output)
{
	int delay_index = (fir->delay_index - 1) & (fir->nr_taps - 1);
	
	sl_create(, fir->station_pid, 0, fir->nr_stations, 1, fir->station_block,,
		fir_compute_station_i16,
		sl_glarg(const ppf_fir_t*,, fir),
		sl_glarg(const complex_i16* RESTRICT,, input),
		sl_glarg(const float* RESTRICT,, weights),
		sl_glarg(complex_f32* RESTRICT,, output),
		sl_glarg(int,, delay_index)
	);
	sl_sync();
	
	fir->delay_index = delay_index;
}


#else // NEW_STYLE



// FOR REFERENCE
#if 1==0
/**
 *	FIR channel computation thread.
 *	Spawns separate threads for all taps in a channel.
 */
sl_def(fir_compute_channel_i16, sl__static,
	sl_glparm(const ppf_fir_t*, fir),
	sl_glparm(const complex_i16* RESTRICT, input),
	sl_glparm(const float* RESTRICT, weights),
	sl_glparm(complex_f32* RESTRICT, output),
	sl_glparm(int, station),
	sl_glparm(int, delay_index)
	)
{
	sl_index(channel);
	const complex_i16* RESTRICT input = sl_getp(input);
	const ppf_fir_t* fir = sl_getp(fir);
	int nr_stations  = fir->nr_stations;
	int nr_taps      = fir->nr_taps;
	int nr_channels  = fir->nr_channels;
	int delay_index  = sl_getp(delay_index);
	int station      = sl_getp(station);
	// get the starting address of the delay line of this channel
	unsigned int delayoffset = ppf_delay_nth(nr_channels, nr_taps, station, channel);
	complex_f32* RESTRICT delay = fir->delay + delayoffset;
	
	// Get the input samples and convert to float.
	unsigned int index  = ppf_input_nth(nr_channels, station, channel, 0);
	complex_i16 sample0 = input[index];
	complex_i16 sample1 = input[index + 1];
	complex_f32 sum0    = {(float)sample0.re, (float)sample0.im};
	complex_f32 sum1    = {(float)sample1.re, (float)sample1.im};
	
	// Store sample values in the delay line.
	delay[delay_index * NR_POLARIZATIONS + 0] = sum0;
	delay[delay_index * NR_POLARIZATIONS + 1] = sum1;
	
	unsigned int outdex0 = ppf_output_nth(nr_stations, nr_channels, station, channel, 0);
	unsigned int outdex1 = ppf_output_nth(nr_stations, nr_channels, station, channel, 1);
	
	fir_compute_channel(fir, sl_getp(weights) + channel * nr_taps, sl_getp(output), delay,
		outdex0, outdex1, nr_taps, delay_index,
		sum0, sum1);
}
sl_enddef

/**
 *	FIR station computation thread.
 *	Spawns separate threads for all channels in a station.
 */
sl_def(fir_compute_station_i16, sl__static,
	sl_glparm(const ppf_fir_t*, fir),
	sl_glparm(const complex_i16* RESTRICT, input),
	sl_glparm(const float* RESTRICT, weights),
	sl_glparm(complex_f32* RESTRICT, output),
	sl_glparm(int, delay_index)
	)
{
	sl_index(station);
	int nr_channels = sl_getp(fir)->nr_channels;
	
	sl_create(, PLACE_LOCAL, 0, nr_channels, 1, fir->channel_block,,
		fir_compute_channel_i16,
		sl_glarg(const ppf_fir_t*,, sl_getp(fir)),
		sl_glarg(const complex_i16* RESTRICT,, sl_getp(input)),
		sl_glarg(const float* RESTRICT,, sl_getp(weights)),
		sl_glarg(complex_f32*,, sl_getp(output)),
		sl_glarg(int,, station),
		sl_glarg(int,, sl_getp(delay_index))
	);
	sl_sync();
}
sl_enddef

void ppf_fir_compute_i16(ppf_fir_t* fir, const complex_i16* RESTRICT input,
	const float* RESTRICT weights, complex_f32* output)
{
	int delay_index = (fir->delay_index - 1) & (fir->nr_taps - 1);
	//printf("delay_index=%d\n", delay_index);
	
	sl_create(, fir->station_pid, 0, fir->nr_stations, 1, fir->station_block,,
		fir_compute_station_i16,
		sl_glarg(const ppf_fir_t*,, fir),
		sl_glarg(const complex_i16* RESTRICT,, input),
		sl_glarg(const float* RESTRICT,, weights),
		sl_glarg(complex_f32* RESTRICT,, output),
		sl_glarg(int,, delay_index)
	);
	sl_sync();
	
	fir->delay_index = delay_index;
}

#else

#define DEF_COMPUTE_CHANNEL(name, sample_type)\
sl_def(name, sl__static,\
	sl_glparm(const ppf_fir_t*, fir),\
	sl_glparm(const sample_type* RESTRICT, input),\
	sl_glparm(const float* RESTRICT, weights),\
	sl_glparm(complex_f32* RESTRICT, output),\
	sl_glparm(int, station),\
	sl_glparm(int, delay_index)\
	)\
{\
	sl_index(channel);\
	const sample_type* RESTRICT input = sl_getp(input);\
	const ppf_fir_t* fir = sl_getp(fir);\
	int nr_stations  = fir->nr_stations;\
	int nr_taps      = fir->nr_taps;\
	int nr_channels  = fir->nr_channels;\
	int delay_index  = sl_getp(delay_index);\
	int station      = sl_getp(station);\
	unsigned int delayoffset = ppf_delay_nth(nr_channels, nr_taps, station, channel);\
	complex_f32* RESTRICT delay = fir->delay + delayoffset;\
	unsigned int index  = ppf_input_nth(nr_channels, station, channel, 0);\
	sample_type sample0 = input[index];\
	sample_type sample1 = input[index + 1];\
	complex_f32 sum0    = {(float)sample0.re, (float)sample0.im};\
	complex_f32 sum1    = {(float)sample1.re, (float)sample1.im};\
	delay[delay_index * NR_POLARIZATIONS + 0] = sum0;\
	delay[delay_index * NR_POLARIZATIONS + 1] = sum1;\
	unsigned int outdex0 = ppf_output_nth(nr_stations, nr_channels, station, channel, 0);\
	unsigned int outdex1 = ppf_output_nth(nr_stations, nr_channels, station, channel, 1);\
	fir_compute_channel(fir, sl_getp(weights) + channel * nr_taps, sl_getp(output), delay,\
		outdex0, outdex1, nr_taps, delay_index,\
		sum0, sum1);\
}\
sl_enddef

DEF_COMPUTE_CHANNEL(ppf_compute_channel_i4 , complex_i4);
DEF_COMPUTE_CHANNEL(ppf_compute_channel_i8 , complex_i8);
DEF_COMPUTE_CHANNEL(ppf_compute_channel_i16, complex_i16);

#define DEF_COMPUTE_STATION(name, sample_type, compute_channel)\
sl_def(name, sl__static,\
	sl_glparm(const ppf_fir_t*, fir),\
	sl_glparm(const sample_type* RESTRICT, input),\
	sl_glparm(const float* RESTRICT, weights),\
	sl_glparm(complex_f32* RESTRICT, output),\
	sl_glparm(int, delay_index)\
	)\
{\
	sl_index(station);\
	const ppf_fir_t* fir = sl_getp(fir);\
	int nr_channels = fir->nr_channels;\
	sl_create(, fir->channel_pid, 0, nr_channels, 1, fir->channel_block,,\
		compute_channel,\
		sl_glarg(const ppf_fir_t*,, fir),\
		sl_glarg(const sample_type* RESTRICT,, sl_getp(input)),\
		sl_glarg(const float* RESTRICT,, sl_getp(weights)),\
		sl_glarg(complex_f32*,, sl_getp(output)),\
		sl_glarg(int,, station),\
		sl_glarg(int,, sl_getp(delay_index))\
	);\
	sl_sync();\
}\
sl_enddef

DEF_COMPUTE_STATION(ppf_fir_compute_station_i4 , complex_i4 , ppf_compute_channel_i4);
DEF_COMPUTE_STATION(ppf_fir_compute_station_i8 , complex_i8 , ppf_compute_channel_i8);
DEF_COMPUTE_STATION(ppf_fir_compute_station_i16, complex_i16, ppf_compute_channel_i16);

#define DEF_COMPUTE(name, sample_type, compute_station)\
void name(ppf_fir_t* fir, const sample_type* RESTRICT input,\
	const float* RESTRICT weights, complex_f32* output)\
{\
	int delay_index = (fir->delay_index - 1) & (fir->nr_taps - 1);\
	sl_create(, fir->station_pid, 0, fir->nr_stations, 1, fir->station_block,,\
		compute_station,\
		sl_glarg(const ppf_fir_t*,, fir),\
		sl_glarg(const sample_type* RESTRICT,, input),\
		sl_glarg(const float* RESTRICT,, weights),\
		sl_glarg(complex_f32* RESTRICT,, output),\
		sl_glarg(int,, delay_index)\
	);\
	sl_sync();\
	fir->delay_index = delay_index;\
}

DEF_COMPUTE(ppf_fir_compute_i4 , complex_i4 , ppf_fir_compute_station_i4);
DEF_COMPUTE(ppf_fir_compute_i8 , complex_i8 , ppf_fir_compute_station_i8);
DEF_COMPUTE(ppf_fir_compute_i16, complex_i16, ppf_fir_compute_station_i16);

#endif


#endif //NEW_STYLE
