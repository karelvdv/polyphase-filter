/*
    A Polyphase Filter for GPUs and Multi-Core Processors
    Copyright (C) 2012 Karel van der Veldt (karel.vd.veldt@uva.nl)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <stdlib.h>
//#include <memory.h>
#include <assert.h>
#include <time.h>
#include <math.h>
#include <svp/slr.h>
#include <svp/perf.h>
#include "fir.h"
#include "fft.h"
#include "orig_weights.h"

#if DATATYPE == 16
#	define COMPLEX complex_i16
#	define INPUT_NEW ppf_input_new_i16
#	define FIR_COMPUTE ppf_fir_compute_i16
#elif DATATYPE == 8
#	define COMPLEX complex_i8
#	define INPUT_NEW ppf_input_new_i8
#	define FIR_COMPUTE ppf_fir_compute_i8
#elif DATATYPE == 4
#	define COMPLEX complex_i4
#	define INPUT_NEW ppf_input_new_i4
#	define FIR_COMPUTE ppf_fir_compute_i4
#else
#	error "Definition DATATYPE must be 4, 8 or 16!"
#endif



slr_decl(
	slr_var(int, p, "stations channels taps runs fir fft"),
	slr_var(int, m, "0=run 1=fir test 2=fft test"),
	slr_var(int, b, "st ch tap block size"),
);



static int gen_input(complex_i16* in, int nr_stations, int nr_channels, int doprint)
{
	for(int st = 1; st <= nr_stations; ++st)
	{
		if(doprint) printf("%d: ", st - 1);
		for(int ch = 1; ch <= nr_channels; ++ch)
		{
			if(doprint) printf("| ");
			for(int po = 0; po < NR_POLARIZATIONS; ++po)
			{
				unsigned int index = ppf_input_nth(nr_channels, st - 1, ch - 1, po);
				in[index].re = st * ch + po;
				in[index].im = ch;
				if(doprint) printf("%d %d, ", in[index].re, in[index].im);
			}
		}
		if(doprint) printf("\n");
	}
}



static int print_output(const complex_f32* out, int nr_stations, int nr_channels)
{
	static const char* ps[] = {"X", "Y"};
	
	for(int st = 0; st < nr_stations; ++st)
	{
		//printf("%d: ", st);
		for(int po = 0; po < NR_POLARIZATIONS; ++po)
		{
			printf("%d%s: ", st, ps[po]);
			for(int ch = 0; ch < nr_channels; ++ch)
			{
				//unsigned int index = ppf_output_nth(nr_stations, nr_channels, st, ch, po);
				printf("%.2g %.2g, ", out[0].re, out[0].im);
				out++;
			}
			printf("\n");
		}
	}
}



static void fir_test()
{
	int nr_stations = 2;
	int nr_channels = 4;
	int nr_taps     = 8;
	int blocks[]    = {1, 4, 4};
	const float weights[4 * 8] = {
		/*0.125, 0.125, 0.125, 0.125, 0.125, 0.125, 0.125, 0.125,
		0.125, 0.125, 0.125, 0.125, 0.125, 0.125, 0.125, 0.125,
		0.125, 0.125, 0.125, 0.125, 0.125, 0.125, 0.125, 0.125,
		0.125, 0.125, 0.125, 0.125, 0.125, 0.125, 0.125, 0.125,*/
		0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25,
		0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25,
		0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25,
		0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25, 0.25,
	};
	complex_i16* input  = ppf_input_new_i16(nr_stations, nr_channels);
	complex_f32* output = ppf_output_new(nr_stations, nr_channels);
	ppf_fir_t* fir = ppf_fir_new(nr_stations, nr_channels, nr_taps, blocks);
	
	gen_input(input, nr_stations, nr_channels, 1);
	
	for(int i = 0; i < 8; ++i)
	{
		ppf_fir_compute_i16(fir, input, weights, output);
	}
	
	print_output(output, nr_stations, nr_channels);
}



static void fft_test()
{
	int nr_stations = 2;
	int nr_channels = 8;
	int nr_taps     = 4;
	int blocks[]    = {1, 4, 4};
	complex_f32* output = ppf_output_new(nr_stations, nr_channels);
	ppf_fir_t* fir = ppf_fir_new(nr_stations, nr_channels, nr_taps, blocks);
	
	complex_f32* p = output;
	
	for(int st = 1; st <= nr_stations; ++st)
	{
		for(int po = 0; po < NR_POLARIZATIONS; ++po)
		{
			for(int ch = 1; ch <= nr_channels; ++ch)
			{
				//unsigned int idx = ppf_output_nth(nr_stations, nr_channels, st - 1, ch - 1, po);
				p[0].re = st * ch;
				p[0].im = ch + po;
				p++;
			}
		}
	}
	
	printf("Input:\n");
	print_output(output, nr_stations, nr_channels);
	
	for(int i = 0; i < 1; ++i)
	{
		ppf_fft_exec(fir, output);
	}
	
	printf("Output:\n");
	print_output(output, nr_stations, nr_channels);
}



static void run(int nr_stations, int nr_channels, int nr_taps, int nr_runs,
	int do_fir, int do_fft, int* blocks)
{
	ppf_fir_t* fir      = ppf_fir_new(nr_stations, nr_channels, nr_taps, blocks);
	COMPLEX* input  = INPUT_NEW(nr_stations, nr_channels);
	complex_f32* output = ppf_output_new(nr_stations, nr_channels);
	counter_t ct[2][MTPERF_NCOUNTERS];
	clock_t clocks = 0;
	
	//gen_input(input, nr_stations, nr_channels, 0);
	
	for(int i = 0; i < nr_runs; ++i)
	{
		mtperf_sample(ct[0]);
		
		if(do_fir) FIR_COMPUTE(fir, input, ppf_orig_weights, output);
		
		if(do_fft) ppf_fft_exec(fir, output);
		
		mtperf_sample(ct[1]);
		clocks += ct[1][MTPERF_CLOCKS] - ct[0][MTPERF_CLOCKS];
	}
	
	double secs = (double)clocks / (double)CLOCKS_PER_SEC;
	
	double io_gb = 0;
	double fir_ai = 0;
	double fir_gflop = 0;
	double fir_gb = 0;
	
	if(do_fir)
	{
		// what comes in + comes out of the fir
		// r 1 x complex int sample
		// w 1 x complex float sample
		double io_bytes = NR_POLARIZATIONS * (sizeof(COMPLEX) + sizeof(complex_f32));
		io_gb    = nr_stations * nr_channels * io_bytes * (nr_runs / (1024.0 * 1024.0 * 1024.0));
		
		// fir, what it actually processes
		// r 1 x sample
		// w 1 x f32*2 output
		// r nr_taps x f32 weights (once for both polarizations)
		// r nr_taps - 1 x f32*2 taps
		// w 1 x f32*2 tap
		double fir_bytes = NR_POLARIZATIONS * sizeof(complex_f32) * (nr_taps - 1 + 1) + sizeof(float) * nr_taps;
	#ifdef FIR_THREADS
		double fir_flop  = 8 * nr_taps;
	#else
		double fir_flop  = 4 + 8 * (nr_taps - 1);
	#endif
		fir_ai    = fir_flop / (fir_bytes + io_bytes);
		
		fir_gb    = nr_stations * nr_channels * fir_bytes * (nr_runs / (1024.0 * 1024.0 * 1024.0));
		fir_gflop = nr_stations * nr_channels * fir_flop  * (nr_runs / 1000000000.0);
	}
	
	// fft
	double fft_ai = 0;
	double fft_gflop = 0;
	double fft_gb = 0;
	
	if(do_fft)
	{
		// FFT operations = 5nlog2(n)
		// FFT inputs+outputs = 4n
		
		double fft_flop  = 5.0 * nr_channels * (log(nr_channels) / log(2));
		double fft_bytes = 4.0 * nr_channels * sizeof(complex_f32);
		fft_ai = fft_flop / fft_bytes;
		fft_gflop = fft_flop * nr_stations * NR_POLARIZATIONS * (nr_runs / 1000000000.0);
		// count input+output bytes
		fft_gb = 2 * fft_bytes * nr_stations * NR_POLARIZATIONS * ( nr_runs / (1024.0 * 1024.0 * 1024.0) );
	}
	
	printf("Stat Chan Taps Runs GB  Time(s) GB/s GFLOP/s AI STB CHB FTB\n");
	printf( "% 4d % 4d % 4d %d %g "
		"% 2.4g % 1.2g % 2.4g % 1.2g "
		"% 4d % 4d % 4d\n",
		nr_stations, nr_channels, nr_taps, nr_runs, io_gb + fir_gb + fft_gb,
		secs, (io_gb + fir_gb + fft_gb) / secs, (fir_gflop + fft_gflop) / secs, fir_ai + fft_ai,
		fir->station_block, fir->channel_block, fir->fft_block);
	
	//ppf_fir_free(fir);
	//ppf_input_free(input);
	//ppf_output_free(output);
}

//#define get_param(parm, def) (slr_len(parm) ? *slr_get(parm) : def)


/**
 *	Run with: slr mg-test-iXX m=MODE p=PARAMS b=BLOCKS
 *	MODE: 0 (measure mode), 1 (fir test), 2 (fft test)
 *	PARAMS: p=nr_stations,nr_channels,nr_taps,nr_runs,dofir,dofft
 *	BLOCKS: b=stationblock,channelblock,tapblock,fftblock
 *
 *	ex: slr -m rbm128 -n 64 mg-test-i16 m=0 p=16,256,16,10,1,0 b=1,4,1,4
 */

sl_def(t_main, void)
{
	int nr_stations = 16;
	int nr_channels = 256;
	int nr_taps     = 16;
	int nr_runs     = 10;
	int do_fir      = 1;
	int do_fft      = 0;
	int mode        = 0;
	int blocks[]    = {1, 4, 4};
	int* b = blocks;
	
	if(slr_len(p) >= 5)
	{
		nr_stations = slr_get(p)[0];
		nr_channels = slr_get(p)[1];
		nr_taps     = slr_get(p)[2];
		nr_runs     = slr_get(p)[3];
		do_fir      = slr_get(p)[4];
		do_fft      = slr_get(p)[5];
	}
	
	if( slr_len(m) ) mode = *slr_get(m);
	if( slr_len(b) ) b = slr_get(b);
	
	assert( nr_channels <= 1024 );
	assert( (nr_channels * nr_taps) <= (256 * 16 * 4) );
	
	fft_gen_table();
	
	//printf("SIZEOF: int=%d, ptr=%d\n", sizeof(int) ,sizeof(void*));
	
	switch(mode)
	{
	case 0:
		run(nr_stations, nr_channels, nr_taps, nr_runs, do_fir, do_fft, b);
		break;
	case 1:
		fir_test();
		break;
	case 2:
		fft_test();
		break;
	}
}
sl_enddef
