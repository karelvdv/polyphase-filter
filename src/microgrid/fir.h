/*
    A Polyphase Filter for GPUs and Multi-Core Processors
    Copyright (C) 2012 Karel van der Veldt (karel.vd.veldt@uva.nl)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __PPF_FIR_H
#define __PPF_FIR_H
#include <stdlib.h>
#include <svp/sep.h>

#define NR_POLARIZATIONS 2

#define RESTRICT restrict

#define ppf_malloc(type, count) (type*)malloc(sizeof(type) * (count))

/**
 *	4-bit sample.
 */
typedef struct __attribute__((packed))
{
	char re : 4;
	char im : 4;
} complex_i4;

/**
 *	8-bit sample.
 */
typedef struct __attribute__((packed))
{
	char re;
	char im;
} complex_i8;

/**
 *	16-bit sample.
 */
typedef struct __attribute__((packed))
{
	short re;
	short im;
} complex_i16;

/**
 *	32-bit sample.
 */
typedef struct
{
	float re;
	float im;
} complex_f32;

/**
 *	FIR data structure.
 */
typedef struct
{
	complex_f32* RESTRICT delay;	// Delay lines.
	int nr_stations;
	int nr_channels;
	int nr_taps;
	int delay_index;
	int table_size;					// 2**table_size = nr_channels
	
	struct placeinfo* station_place;
	struct placeinfo* channel_place;
	struct placeinfo* fft_place;
	sl_place_t station_pid;
	sl_place_t channel_pid;
	sl_place_t fft_pid;
	
	int station_block;
	int channel_block;
	int fft_block;
} ppf_fir_t;

/**
 *	Computes the index into the input array of the given sample.
 */
static inline unsigned int ppf_input_nth(int nr_channels,
	int station, int channel, int polarization)
{
	// input[s][c][p]
	return (station * nr_channels + channel) * NR_POLARIZATIONS + polarization;
}


/**
 *	Computes the index into the output array of the given sample.
 */
static inline unsigned int ppf_output_nth(int nr_stations, int nr_channels,
	int station, int channel, int polarization)
{
#if 1
	// output[p][s][c]
	return (polarization * nr_stations + station) * nr_channels + channel;
#else
	// output[s][p][c]
	return (station * NR_POLARIZATIONS + polarization) * nr_channels + channel;
#endif
}


/**
 *	Computes the index into the delay line array of the given sample.
 */
static inline unsigned int ppf_delay_nth(int nr_channels, int nr_taps, int station, int channel)
{
	// delay[s][c][t][p]
	return ((station * nr_channels + channel) * nr_taps + 0) * NR_POLARIZATIONS + 0;
}

/**
 *	Creates a new FIR data structure.
 */
ppf_fir_t* ppf_fir_new(int nr_stations, int nr_channels, int nr_taps, int* blocks);

/**
 *	Frees the FIR data structure.
 */
void ppf_fir_free(ppf_fir_t* fir);

/**
 *	Allocates a new input array for 16-bit samples.
 */
complex_i4* ppf_input_new_i4(int nr_stations, int nr_channels);
complex_i8* ppf_input_new_i8(int nr_stations, int nr_channels);
complex_i16* ppf_input_new_i16(int nr_stations, int nr_channels);

/**
 *	Allocates a new output array.
 */
complex_f32* ppf_output_new(int nr_stations, int nr_channels);

/**
 *	Frees the input array.
 */
static inline void ppf_input_free(void* in)
{
	free(in);
}

/**
 *	Frees the output array.
 */
static inline void ppf_output_free(complex_f32* out)
{
	free(out);
}

/**
 *	Compute the FIR of 16-bit samples.
 */
 void ppf_fir_compute_i4(ppf_fir_t* fir, const complex_i4* RESTRICT input,
	const float* RESTRICT weights, complex_f32* output);
	void ppf_fir_compute_i8(ppf_fir_t* fir, const complex_i8* RESTRICT input,
	const float* RESTRICT weights, complex_f32* output);
void ppf_fir_compute_i16(ppf_fir_t* fir, const complex_i16* RESTRICT input,
	const float* RESTRICT weights, complex_f32* output);

#endif

