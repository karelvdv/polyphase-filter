//
// fft_impl1.c: this file is part of the SL program suite.
//
// Copyright (C) 2009 Universiteit van Amsterdam.
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 3
// of the License, or (at your option) any later version.
//
// The complete GNU General Public Licence Notice can be found as the
// `COPYING' file in the root directory.
//
// Modified by Karel van der Veldt (karel.vd.veldt@uva.nl)

/**
	Two alternate implementations of the FFT kernel with different layouts
	for the sine/cosine lookup table are provided. Depending on the memory
	implementation they may display different run-time behavior.
*/

#include <math.h>
//#include <stdio.h>
#include <svp/delegate.h>
#include "fft.h"
#include "fir.h"

#define PI 3.1415926535897932384626433832795

#define TABLE_SIZE	10					// 2**TABLE_SIZE is the maximum FFT length (in other words, max. number of channels).
#define MAX_N		(1 << TABLE_SIZE)


sl_def(FFT_Swap, sl__static,
	sl_glparm(cpx_t* RESTRICT, X),
	sl_glparm(unsigned long, i),
	sl_glparm(unsigned long, j)
	)
{
	sl_index(k);
	
	if (sl_getp(i) < sl_getp(j))
	{
		cpx_t* RESTRICT Xi = sl_getp(X) + sl_getp(i);
		cpx_t* RESTRICT Xj = sl_getp(X) + sl_getp(j);
		cpx_t T1 = *Xi;
		cpx_t T2 = *Xj;
		*Xi = T2;
		*Xj = T1;
	}
}
sl_enddef

/**
 *	Performs bit reversal before computing the FFT.
 */
sl_def(FFT_Reverse, sl__static,
	sl_glparm(cpx_t* RESTRICT, X),
	sl_glparm(unsigned long, N),
	sl_shparm(unsigned long, j)
	)
{
	sl_index(i);
	unsigned long t = sl_getp(j);
	unsigned long k;
	
	sl_create(, PLACE_LOCAL,,,,,,
		FFT_Swap,
		sl_glarg(cpx_t* RESTRICT, gX, sl_getp(X)),
		sl_glarg(unsigned long, gi, i),
		sl_glarg(unsigned long, gt, t)
	);
	
	k = sl_getp(N);
	while (k <= t)
	{
		t = t - k;
		k = k / 2;
	}
	sl_sync();
	
	sl_setp(j, t + k);
}
sl_enddef


/**
 *	FFT implementation 1 (default).
 */
#if !defined(FFT_IMPL) || FFT_IMPL == 1

static cpx_t sc_table[TABLE_SIZE][MAX_N / 2];

sl_def(FFT_2, sl__static,
	sl_glparm(cpx_t* RESTRICT, X),
	sl_glparm(unsigned long, k)
	)
{
	sl_index(i);
	unsigned long km1 = sl_getp(k) - 1;
	unsigned long LE2 = 1 << km1;
	unsigned long w   = i & (LE2 - 1); // ((i >> km1) * LE2);
	unsigned long j   = 2 * (i - w) + w;
	unsigned long ip  = j + LE2;
	
	cpx_t U = sc_table[km1][i];
	
	cpx_t* RESTRICT X = sl_getp(X);
	cpx_t T = {
		U.re * X[ip].re - U.im * X[ip].im,
		U.im * X[ip].re + U.re * X[ip].im
	};
	
	cpx_t Xj = X[j];
	X[ip].re = Xj.re - T.re;
	X[ip].im = Xj.im - T.im;
	X[j] .re = Xj.re + T.re;
	X[j] .im = Xj.im + T.im;
}
sl_enddef

sl_def(FFT_1, sl__static,
	sl_glparm(cpx_t* RESTRICT, X),	// N inout data
	sl_glparm(unsigned long, N2),	// N/2
	sl_shparm(long, token)			// 0
	)
{
	sl_index(k);
	
	int t = sl_getp(token);
	sl_create(, PLACE_LOCAL, 0, sl_getp(N2), 1,,,
		FFT_2,
		sl_glarg(cpx_t* RESTRICT, gX, sl_getp(X)),
		sl_glarg(unsigned long, gk, k)
	);
	sl_sync();
	sl_setp(token, t);
}
sl_enddef

void fft_gen_table()
{
	for (unsigned int k = 1; k < TABLE_SIZE; ++k)
	{
		for(unsigned int i = 0; i < (MAX_N / 2); ++i)
		{
			unsigned int LE2 = 1 << (k - 1);
			unsigned int w = i % LE2;
			float d0 = (float)w * (float)(PI / LE2);
			sc_table[k - 1][i].re =  cosf(d0);
			sc_table[k - 1][i].im = -sinf(d0);
		}
	}
}


/**
 *	FFT implementation 2.
 */
#elif FFT_IMPL == 2

static cpx_t sc_table[MAX_N];


sl_def(FFT_2, sl__static,
	sl_glparm(unsigned long, LE2),
	sl_glparm(cpx_t* RESTRICT, X),
	sl_glparm(unsigned long, Z)
	)
{
	sl_index(i);
	
	const unsigned long w  = i & (sl_getp(LE2) - 1);
	const unsigned long j  = (i - w) * 2 + w;
	const unsigned long ip = j + sl_getp(LE2);
	cpx_t* restrict x = sl_getp(X);
	
	const cpx_t U = sc_table[w * sl_getp(Z)];
	
	const cpx_t T = {
		U.re * x[ip].re - U.im * x[ip].im,
		U.im * x[ip].re + U.re * x[ip].im
	};
	
	const cpx_t xj = { x[j].re, x[j].im };
	x[ip].re = xj.re - T.re;
	x[ip].im = xj.im - T.im;
	x[j].re  = xj.re + T.re;
	x[j].im  = xj.im + T.im;
}
sl_enddef

sl_def(FFT_1, sl__static,
	sl_glparm(cpx_t* RESTRICT, X),
	sl_glparm(unsigned long, N2),
	sl_shparm(long, token)
	)
{
    sl_index(k);
	
    unsigned long Z  = (MAX_N >> k);
    unsigned long LE = (1 << k);
	
    long t = sl_getp(token);
    sl_create(, PLACE_LOCAL, 0, sl_getp(N2),,,,
		FFT_2,
		sl_glarg(unsigned long, gLE2, LE / 2),
		sl_glarg(cpx_t* RESTRICT, gX, sl_getp(X)),
		sl_glarg(unsigned long, gZ, Z)
	);
    sl_sync();
    sl_setp(token, t);
}
sl_enddef

void fft_gen_table()
{
	float a = 2.0 * PI / (float)MAX_N;
	for(int i = 0; i < MAX_N; ++i)
	{
		float b = i * a;
		sc_table[i].re =  cosf(b);
		sc_table[i].im = -sinf(b);
	}
}

#else
#	error "FFT_IMPL must be 1 or 2."
#endif


sl_def(FFT_0, sl__static,
	sl_glparm(cpx_t* RESTRICT, inout),	// N inout data
	sl_glparm(unsigned long, N2),
	sl_glparm(int, nr_channels),
	sl_glparm(int, table_size),
	sl_glparm(int, block)
	)
{
	sl_index(i);
	cpx_t* RESTRICT inout = sl_getp(inout) + i * sl_getp(nr_channels);

	for(int j = 0; j < sl_getp(block); ++j)
	{
		sl_create(, PLACE_LOCAL, 0, (1 << sl_getp(table_size)) - 1, 1, 2,,
				FFT_Reverse,
				sl_glarg(cpx_t* RESTRICT,, inout),
				sl_glarg(unsigned long,, sl_getp(N2)),
				sl_sharg(unsigned long,, 0)
			);
		sl_sync();
		
		sl_create(, PLACE_LOCAL, 1, sl_getp(table_size) + 1, 1,,,
			FFT_1,
			sl_glarg(cpx_t* RESTRICT,, inout),
			sl_glarg(unsigned long,, sl_getp(N2)),
			sl_sharg(long,, 0)
		);
		sl_sync();
		
		inout += sl_getp(nr_channels);
	}
}
sl_enddef

void fft_execute(unsigned int nffts, unsigned int table_size, unsigned int block, cpx_t* RESTRICT inout)
{
	//int nffts = fir->nr_stations * NR_POLARIZATIONS;
	//int n2 = 1 << (fir->table_size - 1);
	int n2 = 1 << (table_size - 1);
	int nr_channels = 1 << table_size;
	
	sl_create(, PLACE_DEFAULT, 0, nffts, block, /*fir->fft_block*/,,
		FFT_0,
		sl_glarg(cpx_t* RESTRICT,, inout),
		sl_glarg(unsigned long,, n2),
		sl_glarg(int,, /*fir->*/nr_channels),
		sl_glarg(int,, /*fir->*/table_size),
		sl_glarg(int,, block)
	);
	sl_sync();
}

void ppf_fft_exec(const ppf_fir_t* fir, cpx_t* RESTRICT inout)
{
	fft_execute(fir->nr_stations * NR_POLARIZATIONS, fir->table_size, 1/*fir->fft_block*/, inout);
}

