/*
    A Polyphase Filter for GPUs and Multi-Core Processors
    Copyright (C) 2012 Karel van der Veldt (karel.vd.veldt@uva.nl)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/**
 *	Input data layout:
 *	short2 input[nr_stations][nr_channels][nr_polarizations]
 *	Delay line layout:
 *	float2 delay[nr_stations][nr_taps][nr_channels][nr_polarizations]
 *	Output layout:
 *	float2 output[nr_polarizations][nr_stations][nr_channels]
 *
 *	Grid dimensions:  nr_stations x 2
 *	Block dimensions: nr_channels x 1 (max 256 channels due to register limitations)
 *
 *	Each thread computes one polarization of one channel.
 *	The polarization to compute is determined by whether the thread id is even or odd.
 *	The computation of a whole station is split in two blocks, each computes one half of the total channels.
 */
#include <stdio.h>
#include <cuda.h>
#include "ppf.h"
#include "timer.h"


#define PAGELOCKED_MEMORY_FLAGS (cudaHostAllocPortable | cudaHostAllocMapped | cudaHostAllocWriteCombined)


/**
 *	The number of threads per block is limited by the number of registers,
 *	which is determined by the number of taps:
 *	registers_per_thread = nr_taps * 2 + 10 (delay line + extra vars)
 *
 *	This table holds the max number of threads per block based on the number of taps
 *	to achieve the highest occupancy (calculated with the occupancy calculator).
 *
 *	If the number of taps is not in the table, then it is not supported.
 */
typedef struct
{
	int nr_taps;
	int max_threads_per_block;
} tap_info;
static const tap_info allowed_taps[] = {
	//            occupancy on compute ability 2.0 (GTX480 Fermi)
	//{ 1, 512}, // 100%
	//{ 2, 512}, // 100%
	{ 4, 512}, // 100%
	{ 8, 512}, // 100%
	{16, 256}, // 50%
	{32, 128}, // 25%
	{64,  32}, // 15%
	{0, 0},
};

/**
 *	The weights are stored in the constant memory.
 */
static __device__ __constant__ float fir_weights[256 * 16 * 4]; // 64K (max)


/**
 *	Converts 16-bit sample.
 */
static inline __device__ float2 convert_i16(short2 sample)
{
	return make_float2( __int2float_rn(sample.x), __int2float_rn(sample.y) );
}

/**
 *	Converts 8-bit sample.
 */
static inline __device__ float2 convert_i8(char2 sample)
{
	return make_float2( __int2float_rn(sample.x), __int2float_rn(sample.y) );
}

/**
 *	Converts 4-bit sample.
 *	NOTE: The sample is a char because nvcc chokes on bitfields.
 */
static inline __device__ float2 convert_i4(char sample)
{
	// bit twiddling sign extension
	short r = (short)( (unsigned char)sample &  0x0f ) | ( 0xfff0 * (((unsigned char)sample >> 3) & 1) );
	short i = (short)( (unsigned char)sample >>    4 ) | ( 0xfff0 * (((unsigned char)sample >> 7) & 1) );
	return make_float2( __int2float_rn(r), __int2float_rn(i) );
}



/**
 *	Index into the delay line.
 */
static inline __device__ float2* delay_nth(float2* delay, ushort nr_stations, ushort nr_channels, ushort nr_taps,
	ushort station, ushort channel, ushort delay_index, ushort polarization)
{
	// delay[nr_stations][nr_taps][nr_channels][nr_polarizations]
	// delay[s][t][c][p] = s * nr_taps * nr_channels * nr_polarizations + t * nr_channels * nr_polarizations + c * nr_polarizations + p
	//                   = ((s * nr_taps + t) * nr_channels + c) * nr_polarizations + p
	return delay + ((station * nr_taps + delay_index) * nr_channels + channel) * PPF_NR_POLARIZATIONS + polarization;
}

/**
 *	Index into the input array.
 */
static inline __device__ uint input_nth(ushort nr_stations, ushort nr_channels,
	ushort station, ushort channel, ushort polarization, ushort nth_sample)
{
	// in[nr_samples][nr_stations][nr_channels][nr_polarizations]
	// in[n][s][c][p] = n * nr_samples * nr_stations * nr_channels + s * nr_channels * nr_polarizations + c * nr_polarizations + p
	//                = ((n * nr_stations + s) * nr_channels + c) * nr_polarizations + p
	return ((nth_sample * nr_stations + station) * nr_channels + channel) * PPF_NR_POLARIZATIONS + polarization;
	//(station * nr_channels + channel) * PPF_NR_POLARIZATIONS + polarization;
}

/**
 *	Index into the output array.
 */
static inline __device__ float2* output_nth(float2* out, ushort nr_stations, ushort nr_channels,
	ushort station, ushort channel, ushort polarization, ushort nth_sample)
{
#if 1
	// out[nr_samples][nr_polarizations][nr_stations][nr_channels]
	// out[n][p][s][c] = n * nr_polarizations * nr_stations * nr_channels + p * nr_stations * nr_channels + s * nr_channels + c
	//                 = ((n * nr_polarizations + p) * nr_stations + s) * nr_channels + c
	return out + ((nth_sample * PPF_NR_POLARIZATIONS + polarization) * nr_stations + station) * nr_channels + channel;
#else
	// out[nr_samples][nr_stations][nr_polarizations][nr_channels]
	// out[n][s][p][c] = n * nr_stations * nr_polarizations * nr_channels + s * nr_polarizations * nr_channels + p * nr_channels + c
	//                 = ((n * nr_stations + s) * nr_polarizations + p) * nr_channels + c
	return out + ((nth_sample * nr_stations + station) * PPF_NR_POLARIZATIONS + polarization) * nr_channels + channel;
#endif
}



/***************************************************************
 * REFERENCE IMPLEMENTATION
 ***************************************************************/
#ifdef REF_IMPL


// for reference
#if 0
static __global__ void ppf_fir_kernel_i16(const ushort nr_channels, const ushort nr_taps, const ushort delay_index, const short2* in, float2* delay, float2* out)
{
	const ushort nr_stations    = gridDim.x;
	const ushort station        = blockIdx.x;
	const ushort polarization   = threadIdx.x & 1;
	//const ushort nr_channels    = (blockDim.x * blockDim.y) >> 1;
	const ushort channel        = (threadIdx.x + blockIdx.y * blockDim.x) >> 1; // 0..127, 128...255
	const ushort delay_step     = nr_channels * PPF_NR_POLARIZATIONS;
	const float* weights = fir_weights + channel;
	float2* delay0 = delay_nth(delay, nr_stations, nr_channels, nr_taps, station, channel, delay_index, polarization);
	float2 sum;
	float2 tmp;
	float w;

	// Read the sample and convert to float.
	in  = in + input_nth(nr_stations,  nr_channels, station, channel, polarization, 0);
	sum = convert_i16(*in);

	// Store the sample in the delay line.
	*delay0 = sum;
	delay0 += nr_channels * PPF_NR_POLARIZATIONS;

	// Multiply the first sample with the weight.
	w = *weights;
	weights += nr_channels;
	sum.x *= w;
	sum.y *= w;

	// Multiply-add the other samples in the delay line.
	for(int i = delay_index + 1; i < nr_taps; ++i)
	{
		tmp      = *delay0;
		w        = *weights;
		sum.x   += tmp.x * w;
		sum.y   += tmp.y * w;
		weights += nr_channels;
		delay0  += delay_step;
	}

	delay0 = delay_nth(delay, nr_stations, nr_channels, nr_taps, station, channel, 0, polarization);
	for(int i = 0; i < delay_index; ++i)
	{
		tmp      = *delay0;
		w        = *weights;
		sum.x   += tmp.x * w;
		sum.y   += tmp.y * w;
		weights += nr_channels;
		delay0  += delay_step;
	}

	// Store the result in the output array of the correct polarization.
	out = output_nth(out, nr_stations, nr_channels, station, channel, polarization, 0);
	*out = sum;
}

static __global__ void ppf_fir_kernel_i16(const ushort nr_channels, const ushort nr_taps, const ushort delay_index,
	const short2* in, float2* delay, float2* out)
{
	const ushort nr_stations    = gridDim.x;
	const ushort station        = blockIdx.x;
	const ushort polarization   = threadIdx.x & 1;
	const ushort channel        = (threadIdx.x + blockIdx.y * blockDim.x) >> 1; // 0..127, 128...255
	float2 sum;

	// Read the sample and convert to float.
	in  = in + input_nth(nr_stations, nr_channels, station, channel, polarization, 0);
	sum = convert_i16(*in);

	ppf_fir_kernel(nr_stations, nr_channels, nr_taps, station, channel, polarization, delay_index, delay, out, sum);
}


#endif



static inline __device__ void ppf_fir_kernel(const ushort nr_stations, const ushort nr_channels, const ushort nr_taps,
	const ushort station, const ushort channel, const ushort polarization, const ushort delay_index,
	float2* delay, float2* out, float2 sum)
{
	const ushort delay_step     = nr_channels * PPF_NR_POLARIZATIONS;
	const float* weights = fir_weights + channel;
	float2* delay0 = delay_nth(delay, nr_stations, nr_channels, nr_taps, station, channel, delay_index, polarization);
	float2 tmp;
	float w;

	// Store the sample in the delay line.
	*delay0 = sum;
	delay0 += delay_step;

	// Multiply the first sample with the weight.
	w = *weights;
	weights += nr_channels;
	sum.x *= w;
	sum.y *= w;

	// Multiply-add the other samples in the delay line.
	for(int i = delay_index + 1; i < nr_taps; ++i)
	{
		tmp      = *delay0;
		w        = *weights;
		sum.x   += tmp.x * w;
		sum.y   += tmp.y * w;
		weights += nr_channels;
		delay0  += delay_step;
	}

	delay0 = delay_nth(delay, nr_stations, nr_channels, nr_taps, station, channel, 0, polarization);
	for(int i = 0; i < delay_index; ++i)
	{
		tmp      = *delay0;
		w        = *weights;
		sum.x   += tmp.x * w;
		sum.y   += tmp.y * w;
		weights += nr_channels;
		delay0  += delay_step;
	}

	// Store the result in the output array of the correct polarization.
	out = output_nth(out, nr_stations, nr_channels, station, channel, polarization, 0);
	*out = sum;
}

#define FIR_KERNEL(name, sample_type, convert)\
static __global__ void name(const ushort nr_channels, const ushort nr_taps, const ushort delay_index,\
	const sample_type* in, float2* delay, float2* out)\
{\
	const ushort nr_stations    = gridDim.x;\
	const ushort station        = blockIdx.x;\
	const ushort polarization   = threadIdx.x & 1;\
	const ushort channel        = (threadIdx.x + blockIdx.y * blockDim.x) >> 1;\
	float2 sum;\
	in  = in + input_nth(nr_stations, nr_channels, station, channel, polarization, 0);\
	sum = convert(*in);\
	ppf_fir_kernel(nr_stations, nr_channels, nr_taps, station, channel, polarization, delay_index, delay, out, sum);\
}



FIR_KERNEL(ppf_fir_kernel_i16, short2, convert_i16);
FIR_KERNEL(ppf_fir_kernel_i8, char2, convert_i8);
FIR_KERNEL(ppf_fir_kernel_i4, char, convert_i4);

#define ppf_fir_kernel_i16_64 ppf_fir_kernel_i16
#define ppf_fir_kernel_i16_32 ppf_fir_kernel_i16
#define ppf_fir_kernel_i16_16 ppf_fir_kernel_i16
#define ppf_fir_kernel_i16_8  ppf_fir_kernel_i16
#define ppf_fir_kernel_i16_4  ppf_fir_kernel_i16

#define ppf_fir_kernel_i8_64  ppf_fir_kernel_i8
#define ppf_fir_kernel_i8_32  ppf_fir_kernel_i8
#define ppf_fir_kernel_i8_16  ppf_fir_kernel_i8
#define ppf_fir_kernel_i8_8   ppf_fir_kernel_i8
#define ppf_fir_kernel_i8_4   ppf_fir_kernel_i8

#define ppf_fir_kernel_i4_64  ppf_fir_kernel_i4
#define ppf_fir_kernel_i4_32  ppf_fir_kernel_i4
#define ppf_fir_kernel_i4_16  ppf_fir_kernel_i4
#define ppf_fir_kernel_i4_8   ppf_fir_kernel_i4
#define ppf_fir_kernel_i4_4   ppf_fir_kernel_i4


/***************************************************************
 * OPTIMIZED IMPLEMENTATION
 ***************************************************************/
#else

#define FIR_TAP0()\
	sum.x *= w;\
	sum.y *= w;\
	weights += nr_channels;

#ifdef FORCEFMA
#	define FIR_TAPN(d)\
	w = *weights;\
	sum.x = __fmaf_rn(sum.x, d.x, w);\
	sum.y = __fmaf_rn(sum.y, d.y, w);\
	weights += nr_channels;
#else
#	define FIR_TAPN(d)\
	w = *weights;\
	sum.x += d.x * w;\
	sum.y += d.y * w;\
	weights += nr_channels;
#endif

/**
 *	Computes FIR for 4 taps.
 */
static inline __device__ float2 do_fir_4(ushort nr_channels, const float* weights,
	float2 d00, float2 d01, float2 d02, float2 d03)
{
	// 3 registers
	float w = *weights;
	float2 sum = d00;

	FIR_TAP0();
	FIR_TAPN(d01);
	FIR_TAPN(d02);
	FIR_TAPN(d03);

	return sum;
}

/**
 *	Computes FIR for 8 taps.
 */
static inline __device__ float2 do_fir_8(ushort nr_channels, const float* weights,
	float2 d00, float2 d01, float2 d02, float2 d03, float2 d04, float2 d05, float2 d06, float2 d07)
{
	// 3 registers
	float w = *weights;
	float2 sum = d00;

	FIR_TAP0();
	FIR_TAPN(d01);
	FIR_TAPN(d02);
	FIR_TAPN(d03);
	FIR_TAPN(d04);
	FIR_TAPN(d05);
	FIR_TAPN(d06);
	FIR_TAPN(d07);

	return sum;
}


/**
 *	Computes FIR for 16 taps.
 */
static inline __device__ float2 do_fir_16(ushort nr_channels, const float* weights,
	float2 d00, float2 d01, float2 d02, float2 d03, float2 d04, float2 d05, float2 d06, float2 d07,
	float2 d08, float2 d09, float2 d10, float2 d11, float2 d12, float2 d13, float2 d14, float2 d15)
{
	// 3 registers
	float w = *weights;
	float2 sum = d00;

	FIR_TAP0();
	FIR_TAPN(d01);
	FIR_TAPN(d02);
	FIR_TAPN(d03);
	FIR_TAPN(d04);
	FIR_TAPN(d05);
	FIR_TAPN(d06);
	FIR_TAPN(d07);
	FIR_TAPN(d08);
	FIR_TAPN(d09);
	FIR_TAPN(d10);
	FIR_TAPN(d11);
	FIR_TAPN(d12);
	FIR_TAPN(d13);
	FIR_TAPN(d14);
	FIR_TAPN(d15);

	return sum;
}

/**
 *	Computes FIR for 32 taps.
 */

static inline __device__ float2 do_fir_32(ushort nr_channels, const float* weights,
	float2 d00, float2 d01, float2 d02, float2 d03, float2 d04, float2 d05, float2 d06, float2 d07,
	float2 d08, float2 d09, float2 d10, float2 d11, float2 d12, float2 d13, float2 d14, float2 d15,
	float2 d16, float2 d17, float2 d18, float2 d19, float2 d20, float2 d21, float2 d22, float2 d23,
	float2 d24, float2 d25, float2 d26, float2 d27, float2 d28, float2 d29, float2 d30, float2 d31)
{
	// 3 registers
	float w = *weights;
	float2 sum = d00;

	FIR_TAP0();
	FIR_TAPN(d01);
	FIR_TAPN(d02);
	FIR_TAPN(d03);
	FIR_TAPN(d04);
	FIR_TAPN(d05);
	FIR_TAPN(d06);
	FIR_TAPN(d07);
	FIR_TAPN(d08);
	FIR_TAPN(d09);
	FIR_TAPN(d10);
	FIR_TAPN(d11);
	FIR_TAPN(d12);
	FIR_TAPN(d13);
	FIR_TAPN(d14);
	FIR_TAPN(d15);
	FIR_TAPN(d16);
	FIR_TAPN(d17);
	FIR_TAPN(d18);
	FIR_TAPN(d19);
	FIR_TAPN(d20);
	FIR_TAPN(d21);
	FIR_TAPN(d22);
	FIR_TAPN(d23);
	FIR_TAPN(d24);
	FIR_TAPN(d25);
	FIR_TAPN(d26);
	FIR_TAPN(d27);
	FIR_TAPN(d28);
	FIR_TAPN(d29);
	FIR_TAPN(d30);
	FIR_TAPN(d31);

	return sum;
}

/**
 *	Computes FIR for 64 taps.
 */

static inline __device__ float2 do_fir_64(ushort nr_channels, const float* weights,
	float2 d00, float2 d01, float2 d02, float2 d03, float2 d04, float2 d05, float2 d06, float2 d07,
	float2 d08, float2 d09, float2 d10, float2 d11, float2 d12, float2 d13, float2 d14, float2 d15,
	float2 d16, float2 d17, float2 d18, float2 d19, float2 d20, float2 d21, float2 d22, float2 d23,
	float2 d24, float2 d25, float2 d26, float2 d27, float2 d28, float2 d29, float2 d30, float2 d31,
	float2 d32, float2 d33, float2 d34, float2 d35, float2 d36, float2 d37, float2 d38, float2 d39,
	float2 d40, float2 d41, float2 d42, float2 d43, float2 d44, float2 d45, float2 d46, float2 d47,
	float2 d48, float2 d49, float2 d50, float2 d51, float2 d52, float2 d53, float2 d54, float2 d55,
	float2 d56, float2 d57, float2 d58, float2 d59, float2 d60, float2 d61, float2 d62, float2 d63)
{
	// 3 registers
	float w = *weights;
	float2 sum = d00;

	FIR_TAP0();
	FIR_TAPN(d01);
	FIR_TAPN(d02);
	FIR_TAPN(d03);
	FIR_TAPN(d04);
	FIR_TAPN(d05);
	FIR_TAPN(d06);
	FIR_TAPN(d07);
	FIR_TAPN(d08);
	FIR_TAPN(d09);
	FIR_TAPN(d10);
	FIR_TAPN(d11);
	FIR_TAPN(d12);
	FIR_TAPN(d13);
	FIR_TAPN(d14);
	FIR_TAPN(d15);
	FIR_TAPN(d16);
	FIR_TAPN(d17);
	FIR_TAPN(d18);
	FIR_TAPN(d19);
	FIR_TAPN(d20);
	FIR_TAPN(d21);
	FIR_TAPN(d22);
	FIR_TAPN(d23);
	FIR_TAPN(d24);
	FIR_TAPN(d25);
	FIR_TAPN(d26);
	FIR_TAPN(d27);
	FIR_TAPN(d28);
	FIR_TAPN(d29);
	FIR_TAPN(d30);
	FIR_TAPN(d31);
	FIR_TAPN(d32);
	FIR_TAPN(d33);
	FIR_TAPN(d34);
	FIR_TAPN(d35);
	FIR_TAPN(d36);
	FIR_TAPN(d37);
	FIR_TAPN(d38);
	FIR_TAPN(d39);
	FIR_TAPN(d40);
	FIR_TAPN(d41);
	FIR_TAPN(d42);
	FIR_TAPN(d43);
	FIR_TAPN(d44);
	FIR_TAPN(d45);
	FIR_TAPN(d46);
	FIR_TAPN(d47);
	FIR_TAPN(d48);
	FIR_TAPN(d49);
	FIR_TAPN(d50);
	FIR_TAPN(d51);
	FIR_TAPN(d52);
	FIR_TAPN(d53);
	FIR_TAPN(d54);
	FIR_TAPN(d55);
	FIR_TAPN(d56);
	FIR_TAPN(d57);
	FIR_TAPN(d58);
	FIR_TAPN(d59);
	FIR_TAPN(d60);
	FIR_TAPN(d61);
	FIR_TAPN(d62);
	FIR_TAPN(d63);

	return sum;
}

#define FIR_REGISTERS_4  d00, d01, d02, d03
#define FIR_REGISTERS_8  d00, d01, d02, d03, d04, d05, d06, d07
#define FIR_REGISTERS_16 d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15
#define FIR_REGISTERS_32 d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15,\
						 d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31
#define FIR_REGISTERS_64 d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15,\
						 d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31,\
						 d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47,\
						 d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63


#define FIR_ROUND_4(convert, d00, d01, d02, d03)\
	d00  = convert(*in);\
	*out = do_fir_4(nr_channels, weights, d00, d01, d02, d03);\
	in  += buffer_step;\
	out += buffer_step;

#define FIR_ROUND_8(convert, d00, d01, d02, d03, d04, d05, d06, d07)\
	d00  = convert(*in);\
	*out = do_fir_8(nr_channels, weights, d00, d01, d02, d03, d04, d05, d06, d07);\
	in  += buffer_step;\
	out += buffer_step;

#define FIR_ROUND_16(convert, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15) \
	d00  = convert(*in);\
	*out = do_fir_16(nr_channels, weights, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15);\
	in  += buffer_step;\
	out += buffer_step;

#define FIR_ROUND_32(convert,\
		d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15,\
		d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31)\
	d00  = convert(*in);\
	*out = do_fir_32(nr_channels, weights,\
		d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15,\
		d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31);\
	in  += buffer_step;\
	out += buffer_step;

#define FIR_ROUND_64(convert,\
		d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15,\
		d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31,\
		d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47,\
		d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63)\
	d00  = convert(*in);\
	*out = do_fir_64(nr_channels, weights,\
		d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15,\
		d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31,\
		d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47,\
		d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63);\
	in  += buffer_step;\
	out += buffer_step;

#define FIR_BATCH_4_4(convert)\
	FIR_ROUND_4(convert, d00, d01, d02, d03);\
	FIR_ROUND_4(convert, d03, d00, d01, d02);\
	FIR_ROUND_4(convert, d02, d03, d00, d01);\
	FIR_ROUND_4(convert, d01, d02, d03, d00);

#define FIR_BATCH_8_8(convert)\
	FIR_ROUND_8(convert, d00, d01, d02, d03, d04, d05, d06, d07);\
	FIR_ROUND_8(convert, d07, d00, d01, d02, d03, d04, d05, d06);\
	FIR_ROUND_8(convert, d06, d07, d00, d01, d02, d03, d04, d05);\
	FIR_ROUND_8(convert, d05, d06, d07, d00, d01, d02, d03, d04);\
	FIR_ROUND_8(convert, d04, d05, d06, d07, d00, d01, d02, d03);\
	FIR_ROUND_8(convert, d03, d04, d05, d06, d07, d00, d01, d02);\
	FIR_ROUND_8(convert, d02, d03, d04, d05, d06, d07, d00, d01);\
	FIR_ROUND_8(convert, d01, d02, d03, d04, d05, d06, d07, d00);

#define FIR_BATCH_16_16(convert)\
	FIR_ROUND_16(convert, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15);\
	FIR_ROUND_16(convert, d15, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14);\
	FIR_ROUND_16(convert, d14, d15, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13);\
	FIR_ROUND_16(convert, d13, d14, d15, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12);\
	FIR_ROUND_16(convert, d12, d13, d14, d15, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11);\
	FIR_ROUND_16(convert, d11, d12, d13, d14, d15, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10);\
	FIR_ROUND_16(convert, d10, d11, d12, d13, d14, d15, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09);\
	FIR_ROUND_16(convert, d09, d10, d11, d12, d13, d14, d15, d00, d01, d02, d03, d04, d05, d06, d07, d08);\
	FIR_ROUND_16(convert, d08, d09, d10, d11, d12, d13, d14, d15, d00, d01, d02, d03, d04, d05, d06, d07);\
	FIR_ROUND_16(convert, d07, d08, d09, d10, d11, d12, d13, d14, d15, d00, d01, d02, d03, d04, d05, d06);\
	FIR_ROUND_16(convert, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d00, d01, d02, d03, d04, d05);\
	FIR_ROUND_16(convert, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d00, d01, d02, d03, d04);\
	FIR_ROUND_16(convert, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d00, d01, d02, d03);\
	FIR_ROUND_16(convert, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d00, d01, d02);\
	FIR_ROUND_16(convert, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d00, d01);\
	FIR_ROUND_16(convert, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d00);

#define FIR_BATCH_32_32(convert)\
	FIR_ROUND_32(convert, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31);\
	FIR_ROUND_32(convert, d31, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30);\
	FIR_ROUND_32(convert, d30, d31, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29);\
	FIR_ROUND_32(convert, d29, d30, d31, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28);\
	FIR_ROUND_32(convert, d28, d29, d30, d31, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27);\
	FIR_ROUND_32(convert, d27, d28, d29, d30, d31, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26);\
	FIR_ROUND_32(convert, d26, d27, d28, d29, d30, d31, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25);\
	FIR_ROUND_32(convert, d25, d26, d27, d28, d29, d30, d31, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24);\
	FIR_ROUND_32(convert, d24, d25, d26, d27, d28, d29, d30, d31, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23);\
	FIR_ROUND_32(convert, d23, d24, d25, d26, d27, d28, d29, d30, d31, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22);\
	FIR_ROUND_32(convert, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21);\
	FIR_ROUND_32(convert, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20);\
	FIR_ROUND_32(convert, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19);\
	FIR_ROUND_32(convert, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18);\
	FIR_ROUND_32(convert, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17);\
	FIR_ROUND_32(convert, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16);\
	FIR_ROUND_32(convert, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15);\
	FIR_ROUND_32(convert, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14);\
	FIR_ROUND_32(convert, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13);\
	FIR_ROUND_32(convert, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12);\
	FIR_ROUND_32(convert, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11);\
	FIR_ROUND_32(convert, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10);\
	FIR_ROUND_32(convert, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09);\
	FIR_ROUND_32(convert, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d00, d01, d02, d03, d04, d05, d06, d07, d08);\
	FIR_ROUND_32(convert, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d00, d01, d02, d03, d04, d05, d06, d07);\
	FIR_ROUND_32(convert, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d00, d01, d02, d03, d04, d05, d06);\
	FIR_ROUND_32(convert, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d00, d01, d02, d03, d04, d05);\
	FIR_ROUND_32(convert, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d00, d01, d02, d03, d04);\
	FIR_ROUND_32(convert, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d00, d01, d02, d03);\
	FIR_ROUND_32(convert, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d00, d01, d02);\
	FIR_ROUND_32(convert, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d00, d01);\
	FIR_ROUND_32(convert, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d00);\

#define FIR_BATCH_64_64(convert)\
	FIR_ROUND_64(convert, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63);\
	FIR_ROUND_64(convert, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62);\
	FIR_ROUND_64(convert, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61);\
	FIR_ROUND_64(convert, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60);\
	FIR_ROUND_64(convert, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59);\
	FIR_ROUND_64(convert, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58);\
	FIR_ROUND_64(convert, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57);\
	FIR_ROUND_64(convert, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56);\
	FIR_ROUND_64(convert, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55);\
	FIR_ROUND_64(convert, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54);\
	FIR_ROUND_64(convert, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53);\
	FIR_ROUND_64(convert, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52);\
	FIR_ROUND_64(convert, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51);\
	FIR_ROUND_64(convert, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50);\
	FIR_ROUND_64(convert, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49);\
	FIR_ROUND_64(convert, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48);\
	FIR_ROUND_64(convert, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47);\
	FIR_ROUND_64(convert, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46);\
	FIR_ROUND_64(convert, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45);\
	FIR_ROUND_64(convert, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44);\
	FIR_ROUND_64(convert, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43);\
	FIR_ROUND_64(convert, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42);\
	FIR_ROUND_64(convert, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41);\
	FIR_ROUND_64(convert, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40);\
	FIR_ROUND_64(convert, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39);\
	FIR_ROUND_64(convert, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38);\
	FIR_ROUND_64(convert, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37);\
	FIR_ROUND_64(convert, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36);\
	FIR_ROUND_64(convert, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35);\
	FIR_ROUND_64(convert, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34);\
	FIR_ROUND_64(convert, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33);\
	FIR_ROUND_64(convert, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32);\
	FIR_ROUND_64(convert, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31);\
	FIR_ROUND_64(convert, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30);\
	FIR_ROUND_64(convert, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29);\
	FIR_ROUND_64(convert, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28);\
	FIR_ROUND_64(convert, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27);\
	FIR_ROUND_64(convert, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26);\
	FIR_ROUND_64(convert, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25);\
	FIR_ROUND_64(convert, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24);\
	FIR_ROUND_64(convert, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23);\
	FIR_ROUND_64(convert, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22);\
	FIR_ROUND_64(convert, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21);\
	FIR_ROUND_64(convert, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20);\
	FIR_ROUND_64(convert, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19);\
	FIR_ROUND_64(convert, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18);\
	FIR_ROUND_64(convert, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17);\
	FIR_ROUND_64(convert, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16);\
	FIR_ROUND_64(convert, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15);\
	FIR_ROUND_64(convert, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14);\
	FIR_ROUND_64(convert, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13);\
	FIR_ROUND_64(convert, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12);\
	FIR_ROUND_64(convert, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11);\
	FIR_ROUND_64(convert, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10);\
	FIR_ROUND_64(convert, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09);\
	FIR_ROUND_64(convert, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08);\
	FIR_ROUND_64(convert, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07);\
	FIR_ROUND_64(convert, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06);\
	FIR_ROUND_64(convert, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05);\
	FIR_ROUND_64(convert, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04);\
	FIR_ROUND_64(convert, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03);\
	FIR_ROUND_64(convert, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02);\
	FIR_ROUND_64(convert, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01);\
	FIR_ROUND_64(convert, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00);





#define FIR_DELAY_READ_4\
	delay += delay_step;\
	d01 = *delay; delay += delay_step;\
	d02 = *delay; delay += delay_step;\
	d03 = *delay;

#define FIR_DELAY_WRITE_4_4\
	*delay = d03; delay -= delay_step;\
	*delay = d02; delay -= delay_step;\
	*delay = d01;


#define FIR_DELAY_READ_8\
	delay += delay_step;\
	d01 = *delay; delay += delay_step;\
	d02 = *delay; delay += delay_step;\
	d03 = *delay; delay += delay_step;\
	d04 = *delay; delay += delay_step;\
	d05 = *delay; delay += delay_step;\
	d06 = *delay; delay += delay_step;\
	d07 = *delay;

#define FIR_DELAY_WRITE_8_8\
	*delay = d07; delay -= delay_step;\
	*delay = d06; delay -= delay_step;\
	*delay = d05; delay -= delay_step;\
	*delay = d04; delay -= delay_step;\
	*delay = d03; delay -= delay_step;\
	*delay = d02; delay -= delay_step;\
	*delay = d01;



#define FIR_DELAY_READ_16\
	delay += delay_step;\
	d01 = *delay; delay += delay_step;\
	d02 = *delay; delay += delay_step;\
	d03 = *delay; delay += delay_step;\
	d04 = *delay; delay += delay_step;\
	d05 = *delay; delay += delay_step;\
	d06 = *delay; delay += delay_step;\
	d07 = *delay; delay += delay_step;\
	d08 = *delay; delay += delay_step;\
	d09 = *delay; delay += delay_step;\
	d10 = *delay; delay += delay_step;\
	d11 = *delay; delay += delay_step;\
	d12 = *delay; delay += delay_step;\
	d13 = *delay; delay += delay_step;\
	d14 = *delay; delay += delay_step;\
	d15 = *delay;


#define FIR_DELAY_WRITE_16_16\
	*delay = d15; delay -= delay_step;\
	*delay = d14; delay -= delay_step;\
	*delay = d13; delay -= delay_step;\
	*delay = d12; delay -= delay_step;\
	*delay = d11; delay -= delay_step;\
	*delay = d10; delay -= delay_step;\
	*delay = d09; delay -= delay_step;\
	*delay = d08; delay -= delay_step;\
	*delay = d07; delay -= delay_step;\
	*delay = d06; delay -= delay_step;\
	*delay = d05; delay -= delay_step;\
	*delay = d04; delay -= delay_step;\
	*delay = d03; delay -= delay_step;\
	*delay = d02; delay -= delay_step;\
	*delay = d01;

#define FIR_DELAY_READ_32\
	delay += delay_step;\
	d01 = *delay; delay += delay_step;\
	d02 = *delay; delay += delay_step;\
	d03 = *delay; delay += delay_step;\
	d04 = *delay; delay += delay_step;\
	d05 = *delay; delay += delay_step;\
	d06 = *delay; delay += delay_step;\
	d07 = *delay; delay += delay_step;\
	d08 = *delay; delay += delay_step;\
	d09 = *delay; delay += delay_step;\
	d10 = *delay; delay += delay_step;\
	d11 = *delay; delay += delay_step;\
	d12 = *delay; delay += delay_step;\
	d13 = *delay; delay += delay_step;\
	d14 = *delay; delay += delay_step;\
	d15 = *delay; delay += delay_step;\
	d16 = *delay; delay += delay_step;\
	d17 = *delay; delay += delay_step;\
	d18 = *delay; delay += delay_step;\
	d19 = *delay; delay += delay_step;\
	d20 = *delay; delay += delay_step;\
	d21 = *delay; delay += delay_step;\
	d22 = *delay; delay += delay_step;\
	d23 = *delay; delay += delay_step;\
	d24 = *delay; delay += delay_step;\
	d25 = *delay; delay += delay_step;\
	d26 = *delay; delay += delay_step;\
	d27 = *delay; delay += delay_step;\
	d28 = *delay; delay += delay_step;\
	d29 = *delay; delay += delay_step;\
	d30 = *delay; delay += delay_step;\
	d31 = *delay;

#define FIR_DELAY_WRITE_32_32\
	*delay = d31; delay -= delay_step;\
	*delay = d30; delay -= delay_step;\
	*delay = d29; delay -= delay_step;\
	*delay = d28; delay -= delay_step;\
	*delay = d27; delay -= delay_step;\
	*delay = d26; delay -= delay_step;\
	*delay = d25; delay -= delay_step;\
	*delay = d24; delay -= delay_step;\
	*delay = d23; delay -= delay_step;\
	*delay = d22; delay -= delay_step;\
	*delay = d21; delay -= delay_step;\
	*delay = d20; delay -= delay_step;\
	*delay = d19; delay -= delay_step;\
	*delay = d18; delay -= delay_step;\
	*delay = d17; delay -= delay_step;\
	*delay = d16; delay -= delay_step;\
	*delay = d15; delay -= delay_step;\
	*delay = d14; delay -= delay_step;\
	*delay = d13; delay -= delay_step;\
	*delay = d12; delay -= delay_step;\
	*delay = d11; delay -= delay_step;\
	*delay = d10; delay -= delay_step;\
	*delay = d09; delay -= delay_step;\
	*delay = d08; delay -= delay_step;\
	*delay = d07; delay -= delay_step;\
	*delay = d06; delay -= delay_step;\
	*delay = d05; delay -= delay_step;\
	*delay = d04; delay -= delay_step;\
	*delay = d03; delay -= delay_step;\
	*delay = d02; delay -= delay_step;\
	*delay = d01;

#define FIR_DELAY_READ_64\
	delay += delay_step;\
	d01 = *delay; delay += delay_step;\
	d02 = *delay; delay += delay_step;\
	d03 = *delay; delay += delay_step;\
	d04 = *delay; delay += delay_step;\
	d05 = *delay; delay += delay_step;\
	d06 = *delay; delay += delay_step;\
	d07 = *delay; delay += delay_step;\
	d08 = *delay; delay += delay_step;\
	d09 = *delay; delay += delay_step;\
	d10 = *delay; delay += delay_step;\
	d11 = *delay; delay += delay_step;\
	d12 = *delay; delay += delay_step;\
	d13 = *delay; delay += delay_step;\
	d14 = *delay; delay += delay_step;\
	d15 = *delay; delay += delay_step;\
	d16 = *delay; delay += delay_step;\
	d17 = *delay; delay += delay_step;\
	d18 = *delay; delay += delay_step;\
	d19 = *delay; delay += delay_step;\
	d20 = *delay; delay += delay_step;\
	d21 = *delay; delay += delay_step;\
	d22 = *delay; delay += delay_step;\
	d23 = *delay; delay += delay_step;\
	d24 = *delay; delay += delay_step;\
	d25 = *delay; delay += delay_step;\
	d26 = *delay; delay += delay_step;\
	d27 = *delay; delay += delay_step;\
	d28 = *delay; delay += delay_step;\
	d29 = *delay; delay += delay_step;\
	d30 = *delay; delay += delay_step;\
	d31 = *delay; delay += delay_step;\
	d32 = *delay; delay += delay_step;\
	d33 = *delay; delay += delay_step;\
	d34 = *delay; delay += delay_step;\
	d35 = *delay; delay += delay_step;\
	d36 = *delay; delay += delay_step;\
	d37 = *delay; delay += delay_step;\
	d38 = *delay; delay += delay_step;\
	d39 = *delay; delay += delay_step;\
	d40 = *delay; delay += delay_step;\
	d41 = *delay; delay += delay_step;\
	d42 = *delay; delay += delay_step;\
	d43 = *delay; delay += delay_step;\
	d44 = *delay; delay += delay_step;\
	d45 = *delay; delay += delay_step;\
	d46 = *delay; delay += delay_step;\
	d47 = *delay; delay += delay_step;\
	d48 = *delay; delay += delay_step;\
	d49 = *delay; delay += delay_step;\
	d50 = *delay; delay += delay_step;\
	d51 = *delay; delay += delay_step;\
	d52 = *delay; delay += delay_step;\
	d53 = *delay; delay += delay_step;\
	d54 = *delay; delay += delay_step;\
	d55 = *delay; delay += delay_step;\
	d56 = *delay; delay += delay_step;\
	d57 = *delay; delay += delay_step;\
	d58 = *delay; delay += delay_step;\
	d59 = *delay; delay += delay_step;\
	d60 = *delay; delay += delay_step;\
	d61 = *delay; delay += delay_step;\
	d62 = *delay; delay += delay_step;\
	d63 = *delay;

#define FIR_DELAY_WRITE_64_64\
	*delay = d63; delay -= delay_step;\
	*delay = d62; delay -= delay_step;\
	*delay = d61; delay -= delay_step;\
	*delay = d60; delay -= delay_step;\
	*delay = d59; delay -= delay_step;\
	*delay = d58; delay -= delay_step;\
	*delay = d57; delay -= delay_step;\
	*delay = d56; delay -= delay_step;\
	*delay = d55; delay -= delay_step;\
	*delay = d54; delay -= delay_step;\
	*delay = d53; delay -= delay_step;\
	*delay = d52; delay -= delay_step;\
	*delay = d51; delay -= delay_step;\
	*delay = d50; delay -= delay_step;\
	*delay = d49; delay -= delay_step;\
	*delay = d48; delay -= delay_step;\
	*delay = d47; delay -= delay_step;\
	*delay = d46; delay -= delay_step;\
	*delay = d45; delay -= delay_step;\
	*delay = d44; delay -= delay_step;\
	*delay = d43; delay -= delay_step;\
	*delay = d42; delay -= delay_step;\
	*delay = d41; delay -= delay_step;\
	*delay = d40; delay -= delay_step;\
	*delay = d39; delay -= delay_step;\
	*delay = d38; delay -= delay_step;\
	*delay = d37; delay -= delay_step;\
	*delay = d36; delay -= delay_step;\
	*delay = d35; delay -= delay_step;\
	*delay = d34; delay -= delay_step;\
	*delay = d33; delay -= delay_step;\
	*delay = d32; delay -= delay_step;\
	*delay = d31; delay -= delay_step;\
	*delay = d30; delay -= delay_step;\
	*delay = d29; delay -= delay_step;\
	*delay = d28; delay -= delay_step;\
	*delay = d27; delay -= delay_step;\
	*delay = d26; delay -= delay_step;\
	*delay = d25; delay -= delay_step;\
	*delay = d24; delay -= delay_step;\
	*delay = d23; delay -= delay_step;\
	*delay = d22; delay -= delay_step;\
	*delay = d21; delay -= delay_step;\
	*delay = d20; delay -= delay_step;\
	*delay = d19; delay -= delay_step;\
	*delay = d18; delay -= delay_step;\
	*delay = d17; delay -= delay_step;\
	*delay = d16; delay -= delay_step;\
	*delay = d15; delay -= delay_step;\
	*delay = d14; delay -= delay_step;\
	*delay = d13; delay -= delay_step;\
	*delay = d12; delay -= delay_step;\
	*delay = d11; delay -= delay_step;\
	*delay = d10; delay -= delay_step;\
	*delay = d09; delay -= delay_step;\
	*delay = d08; delay -= delay_step;\
	*delay = d07; delay -= delay_step;\
	*delay = d06; delay -= delay_step;\
	*delay = d05; delay -= delay_step;\
	*delay = d04; delay -= delay_step;\
	*delay = d03; delay -= delay_step;\
	*delay = d02; delay -= delay_step;\
	*delay = d01;




// for reference
#if 0
static __global__ void ppf_fir_kernel_i16(const ushort nr_channels, const uint delay_step, const uint buffer_step,
	const short2* in, float2* out, float2* delay, ushort batches)
{
	// maximum amount of registers = 42
	// current usage: 36 + 3       = 39 registers
	// 2 registers
	const ushort nr_stations  = gridDim.x;
	const ushort station      = blockIdx.x;
	const ushort polarization = threadIdx.x & 1;
	const ushort channel      = (threadIdx.x + blockIdx.y * blockDim.x) >> 1;

	// starting address of the weights of this channel, 2 registers
	const float* weights = fir_weights + channel;

	// this is the whole delay line
	// 32 registers
	float2 d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15;

	in    = in + input_nth(nr_stations, nr_channels, station, channel, polarization, 0);
	out   = output_nth(out, nr_stations, nr_channels, station, channel, polarization, 0);
	delay = delay_nth(delay, nr_stations, nr_channels, 16, station, channel, 0, polarization);

	// get all the old values from the memory

	delay += delay_step;
	//d00 will contain the first sample
	d01 = *delay; delay += delay_step;
	d02 = *delay; delay += delay_step;
	d03 = *delay; delay += delay_step;
	d04 = *delay; delay += delay_step;
	d05 = *delay; delay += delay_step;
	d06 = *delay; delay += delay_step;
	d07 = *delay; delay += delay_step;
	d08 = *delay; delay += delay_step;
	d09 = *delay; delay += delay_step;
	d10 = *delay; delay += delay_step;
	d11 = *delay; delay += delay_step;
	d12 = *delay; delay += delay_step;
	d13 = *delay; delay += delay_step;
	d14 = *delay; delay += delay_step;
	d15 = *delay; //delay += delay_step;

	for(int i = 0; i < batches; ++i)
	{
		// newest ... oldest
		FIR_ROUND_16(convert_i16, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15);
		FIR_ROUND_16(convert_i16, d15, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14);
		FIR_ROUND_16(convert_i16, d14, d15, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13);
		FIR_ROUND_16(convert_i16, d13, d14, d15, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12);
		FIR_ROUND_16(convert_i16, d12, d13, d14, d15, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11);
		FIR_ROUND_16(convert_i16, d11, d12, d13, d14, d15, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10);
		FIR_ROUND_16(convert_i16, d10, d11, d12, d13, d14, d15, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09);
		FIR_ROUND_16(convert_i16, d09, d10, d11, d12, d13, d14, d15, d00, d01, d02, d03, d04, d05, d06, d07, d08);
		FIR_ROUND_16(convert_i16, d08, d09, d10, d11, d12, d13, d14, d15, d00, d01, d02, d03, d04, d05, d06, d07);
		FIR_ROUND_16(convert_i16, d07, d08, d09, d10, d11, d12, d13, d14, d15, d00, d01, d02, d03, d04, d05, d06);
		FIR_ROUND_16(convert_i16, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d00, d01, d02, d03, d04, d05);
		FIR_ROUND_16(convert_i16, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d00, d01, d02, d03, d04);
		FIR_ROUND_16(convert_i16, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d00, d01, d02, d03);
		FIR_ROUND_16(convert_i16, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d00, d01, d02);
		FIR_ROUND_16(convert_i16, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d00, d01);
		FIR_ROUND_16(convert_i16, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d00);
	}

	*delay = d15; delay -= delay_step;
	*delay = d14; delay -= delay_step;
	*delay = d13; delay -= delay_step;
	*delay = d12; delay -= delay_step;
	*delay = d11; delay -= delay_step;
	*delay = d10; delay -= delay_step;
	*delay = d09; delay -= delay_step;
	*delay = d08; delay -= delay_step;
	*delay = d07; delay -= delay_step;
	*delay = d06; delay -= delay_step;
	*delay = d05; delay -= delay_step;
	*delay = d04; delay -= delay_step;
	*delay = d03; delay -= delay_step;
	*delay = d02; delay -= delay_step;
	*delay = d01; //delay -= delay_step;
}
#endif //0







/**
 *	Process samples in batches.
 */
#define FIR_KERNEL(name, nr_taps, sample_type, convert, registers, delay_read, delay_write, batch)\
static __global__ void name(const ushort nr_channels, const uint delay_step, const uint buffer_step,\
	const sample_type* in, float2* out, float2* delay, ushort batches)\
{\
	const ushort nr_stations  = gridDim.x;\
	const ushort station      = blockIdx.x;\
	const ushort polarization = threadIdx.x & 1;\
	const ushort channel      = (threadIdx.x + blockIdx.y * blockDim.x) >> 1;\
	const float* weights = fir_weights + channel;\
	float2 registers;\
	in    = in + input_nth(nr_stations, nr_channels, station, channel, polarization, 0);\
	out   = output_nth(out, nr_stations, nr_channels, station, channel, polarization, 0);\
	delay = delay_nth(delay, nr_stations, nr_channels, nr_taps, station, channel, 0, polarization);\
	delay_read;\
	for(int i = 0; i < batches; ++i)\
	{\
		batch(convert);\
	}\
	delay_write;\
}


#ifndef PTXINSPECT
FIR_KERNEL(ppf_fir_kernel_i16_64, 64, short2, convert_i16, FIR_REGISTERS_64, FIR_DELAY_READ_64, FIR_DELAY_WRITE_64_64, FIR_BATCH_64_64);
FIR_KERNEL(ppf_fir_kernel_i16_32, 32, short2, convert_i16, FIR_REGISTERS_32, FIR_DELAY_READ_32, FIR_DELAY_WRITE_32_32, FIR_BATCH_32_32);
#endif
FIR_KERNEL(ppf_fir_kernel_i16_16, 16, short2, convert_i16, FIR_REGISTERS_16, FIR_DELAY_READ_16, FIR_DELAY_WRITE_16_16, FIR_BATCH_16_16);
#ifndef PTXINSPECT
FIR_KERNEL(ppf_fir_kernel_i16_8 ,  8, short2, convert_i16, FIR_REGISTERS_8, FIR_DELAY_READ_8, FIR_DELAY_WRITE_8_8, FIR_BATCH_8_8);
FIR_KERNEL(ppf_fir_kernel_i16_4 ,  4, short2, convert_i16, FIR_REGISTERS_4, FIR_DELAY_READ_4, FIR_DELAY_WRITE_4_4, FIR_BATCH_4_4);

FIR_KERNEL(ppf_fir_kernel_i8_64 , 64, char2, convert_i8, FIR_REGISTERS_64, FIR_DELAY_READ_64, FIR_DELAY_WRITE_64_64, FIR_BATCH_64_64);
FIR_KERNEL(ppf_fir_kernel_i8_32 , 32, char2, convert_i8, FIR_REGISTERS_32, FIR_DELAY_READ_32, FIR_DELAY_WRITE_32_32, FIR_BATCH_32_32);
FIR_KERNEL(ppf_fir_kernel_i8_16 , 16, char2, convert_i8, FIR_REGISTERS_16, FIR_DELAY_READ_16, FIR_DELAY_WRITE_16_16, FIR_BATCH_16_16);
FIR_KERNEL(ppf_fir_kernel_i8_8  ,  8, char2, convert_i8, FIR_REGISTERS_8, FIR_DELAY_READ_8, FIR_DELAY_WRITE_8_8, FIR_BATCH_8_8);
FIR_KERNEL(ppf_fir_kernel_i8_4  ,  4, char2, convert_i8, FIR_REGISTERS_4, FIR_DELAY_READ_4, FIR_DELAY_WRITE_4_4, FIR_BATCH_4_4);

FIR_KERNEL(ppf_fir_kernel_i4_64 , 64, char, convert_i4, FIR_REGISTERS_64, FIR_DELAY_READ_64, FIR_DELAY_WRITE_64_64, FIR_BATCH_64_64);
FIR_KERNEL(ppf_fir_kernel_i4_32 , 32, char, convert_i4, FIR_REGISTERS_32, FIR_DELAY_READ_32, FIR_DELAY_WRITE_32_32, FIR_BATCH_32_32);
FIR_KERNEL(ppf_fir_kernel_i4_16 , 16, char, convert_i4, FIR_REGISTERS_16, FIR_DELAY_READ_16, FIR_DELAY_WRITE_16_16, FIR_BATCH_16_16);
FIR_KERNEL(ppf_fir_kernel_i4_8  ,  8, char, convert_i4, FIR_REGISTERS_8, FIR_DELAY_READ_8, FIR_DELAY_WRITE_8_8, FIR_BATCH_8_8);
FIR_KERNEL(ppf_fir_kernel_i4_4  ,  4, char, convert_i4, FIR_REGISTERS_4, FIR_DELAY_READ_4, FIR_DELAY_WRITE_4_4, FIR_BATCH_4_4);
#endif

#endif //REF_IMPL


#ifndef PTXINSPECT

void ppf_prefer_l1()
{
	//cudaFuncSetCacheConfig(ppf_fir_kernel_i16, cudaFuncCachePreferL1);
}



/***********************************************************************
 * PAGELOCKED MEMORY
 ***********************************************************************/
#ifdef PAGELOCKED_MEMORY

/**
 *	Defines an input_new_XYZ function.
 */
#define INPUT_NEW(name, input_type, sample_type)\
input_type* name(int nr_stations, int nr_channels)\
{\
	uint len = sizeof(sample_type) * nr_stations * nr_channels * PPF_NR_POLARIZATIONS;\
	input_type* in = ppf_malloc(input_type, 1);\
	if( !in ) return NULL;\
	if( cudaHostAlloc(&in->d, len, PAGELOCKED_MEMORY_FLAGS) != cudaSuccess )\
	{\
		ppf_free(in);\
		return NULL;\
	}\
	return in;\
}

/**
 *	Defines an input_free_XYZ function.
 */
#define INPUT_FREE(name, input_type)\
void name(input_type* in)\
{\
	cudaFreeHost(in->d);\
	free(in);\
}


ppf_output* ppf_output_new(int nr_stations, int nr_channels)
{
	int len = sizeof(ppf_complex_f32) * nr_stations * nr_channels * PPF_NR_POLARIZATIONS;
	float2* outd;
	float2* outh;

	if( cudaMalloc(&outd, len) != cudaSuccess )
	{
		return 0;
	}
	
	if( cudaHostAlloc(&outh, len, cudaHostAllocMapped | cudaHostAllocPortable) != cudaSuccess )
	{
		cudaFree(outd);
		return 0;
	}

	ppf_output* out = ppf_malloc(ppf_output, 1);
	if( !out )
	{
		cudaFree(outd);
		cudaFreeHost(outh);
		return NULL;
	}
	
	out->h = outh;
	out->d = outd;
	
	return out;
}

void ppf_output_free(ppf_output* out)
{
	cudaFreeHost(out->h);
	cudaFree(out->d);
	free(out);
}


/**************************
 * NOT PAGELOCKED MEMORY
 **************************/
#else //PAGELOCKED_MEMORY

/**
 *	Defines a new input_new_XYZ function.
 */
#define INPUT_NEW(name, input_type, sample_type)\
input_type* name(int nr_stations, int nr_channels)\
{\
	uint len = sizeof(sample_type) * nr_stations * nr_channels * PPF_NR_POLARIZATIONS;\
	input_type* in = (input_type*)malloc( sizeof(input_type) + len );\
	if( !in ) return NULL;\
	if( cudaMalloc(&in->d, len) != cudaSuccess )\
	{\
		free(in);\
		return NULL;\
	}\
	return in;\
}

/**
 *	Defines a new input_free_XYZ function.
 */
#define INPUT_FREE(name, input_type)\
void name(input_type* in)\
{\
	cudaFree(in->d);\
	free(in);\
}

#if 0
cudaError_t ppf_input_transfer_i16(ppf_input_i16* in, int nr_stations, int nr_channels, int nr_samples)
{
	ppf_complex_i16* src = ppf_input_get_host(in);
	ppf_complex_i16* dst = ppf_input_get_device(in);

	return cudaMemcpy(dst, src,
		sizeof(ppf_complex_i16) * nr_stations * nr_channels * PPF_NR_POLARIZATIONS * nr_samples,
		cudaMemcpyHostToDevice);
}
#endif
cudaError_t ppf_input_transfer(void* dst, const void* src, int sample_size, int nr_stations, int nr_channels, int nr_samples)
{
	//ppf_complex_i16* src = ppf_input_get_host(in);
	//ppf_complex_i16* dst = ppf_input_get_device(in);
	return cudaMemcpy(dst, src,
		sample_size * nr_stations * nr_channels * PPF_NR_POLARIZATIONS * nr_samples,
		cudaMemcpyHostToDevice);
}

#define INPUT_TRANSFER(name, input_type, sample_type)\
cudaError_t name(input_type* in, int nr_stations, int nr_channels, int nr_samples)\
{\
	return ppf_input_transfer(ppf_input_get_device(in), ppf_input_get_host(in), sizeof(sample_type), nr_stations, nr_channels, nr_samples);\
}

INPUT_TRANSFER(ppf_input_transfer_i16, ppf_input_i16, ppf_complex_i16);
INPUT_TRANSFER(ppf_input_transfer_i8, ppf_input_i8, ppf_complex_i8);
INPUT_TRANSFER(ppf_input_transfer_i4, ppf_input_i4, ppf_complex_i4);


ppf_output* ppf_output_new(int nr_stations, int nr_channels)
{
	int len = sizeof(ppf_complex_f32) * nr_stations * nr_channels * PPF_NR_POLARIZATIONS;
	float2* outd;

	if( cudaMalloc(&outd, len) != cudaSuccess )
	{
		return 0;
	}

	ppf_output* out = (ppf_output*)malloc( sizeof(ppf_output) + len );
	if( !out )
	{
		cudaFree(outd);
		return NULL;
	}

	out->d = outd;
	
	return out;
}

void ppf_output_free(ppf_output* out)
{
	cudaFree(out->d);
	free(out);
}

#endif // PAGELOCKED_MEMORY


/**
 *	Define the input functions.
 */

INPUT_NEW(ppf_input_new_i16, ppf_input_i16, ppf_complex_i16);
INPUT_NEW(ppf_input_new_i8, ppf_input_i8, ppf_complex_i8);
INPUT_NEW(ppf_input_new_i4, ppf_input_i4, ppf_complex_i4);
INPUT_FREE(ppf_input_free_i16, ppf_input_i16);
INPUT_FREE(ppf_input_free_i8, ppf_input_i8);
INPUT_FREE(ppf_input_free_i4, ppf_input_i4);




cudaError_t ppf_output_transfer(ppf_output* out, int nr_stations, int nr_channels, int nr_samples)
{
	ppf_complex_f32* src = ppf_output_get_device(out);
	ppf_complex_f32* dst = ppf_output_get_host(out);

	return cudaMemcpy(dst, src,
		sizeof(ppf_complex_f32) * nr_stations * nr_channels * PPF_NR_POLARIZATIONS * nr_samples,
		cudaMemcpyDeviceToHost);
}



/**************************************************************
 *	FIR
 **************************************************************/

ppf_fir* ppf_fir_new(int nr_stations, int nr_channels, int nr_taps)
{
	const tap_info* tapinfo = allowed_taps;

	while(tapinfo->nr_taps && tapinfo->nr_taps != nr_taps) tapinfo++;

	// unsupported nr_taps
	if(tapinfo->nr_taps == 0)
	{
		return NULL;
	}

	float2* d_delay;

	if( cudaMalloc(&d_delay, sizeof(ppf_complex_f32) * nr_stations * nr_channels * nr_taps * PPF_NR_POLARIZATIONS) != cudaSuccess )
	{
		return NULL;
	}

	ppf_fir* fir = ppf_malloc(ppf_fir, 1);

	if( !fir )
	{
		cudaFree(d_delay);
		return NULL;
	}

	fir->nr_stations = nr_stations;
	fir->nr_channels = nr_channels;
	fir->nr_taps     = nr_taps;
	fir->d_delay     = d_delay;


	// calculate the size of the grid block
	int threads = tapinfo->max_threads_per_block;
	int blocks_per_station = 1;

	while( (nr_channels * PPF_NR_POLARIZATIONS) / blocks_per_station > threads )
	{
		blocks_per_station++;
	}
	
	fir->threads_per_block.x = (nr_channels * PPF_NR_POLARIZATIONS) / blocks_per_station;
	fir->threads_per_block.y = 1;
	fir->threads_per_block.z = 1;
	fir->blocks_per_grid.x   = nr_stations;
	fir->blocks_per_grid.y   = blocks_per_station;
	fir->blocks_per_grid.z   = 1;
#if 0
	fprintf(stderr, "threads_per_block = %d\n", fir->threads_per_block.x);
	fprintf(stderr, "blocks_per_grid   = %d x %d\n", fir->blocks_per_grid.x, fir->blocks_per_grid.y);
#endif

	return fir;
}

void ppf_fir_free(ppf_fir* fir)
{
	cudaFree(fir->d_delay);
	ppf_free(fir);
}

cudaError_t ppf_fir_set_weights(ppf_fir* fir, const float* weights, int nr_channels, int nr_taps)
{
	float* trans = ppf_malloc(float, nr_channels * nr_taps);

	// transpose the array first
	for(int i = 0; i < nr_channels; ++i)
	{
		for(int j = 0; j < nr_taps; ++j)
		{
			trans[j * nr_channels + i] = weights[i * nr_taps + j];
		}
	}

	cudaError_t err = cudaMemcpyToSymbol(fir_weights,
		trans, sizeof(float) * nr_channels * nr_taps, 0,
		cudaMemcpyHostToDevice);

	ppf_free(trans);

	return err;
}


/************************************************
 *	FIR EXECUTE REFERENCE IMPLEMENTATION
 ************************************************/
#ifdef REF_IMPL


#define FIR_EXECUTE(name, input_type, sample_type, kernel)\
static cudaError_t name(ppf_fir* fir, const input_type* in, ppf_output* out, int nr_samples)\
{\
	int nr_stations       = fir->nr_stations;\
	int nr_channels       = fir->nr_channels;\
	int nr_taps           = fir->nr_taps;\
	sample_type* ind      = (sample_type*)ppf_input_get_device(in);\
	ppf_complex_f32* outd = (ppf_complex_f32*)ppf_output_get_device(out);\
	int delay_index       = fir->delay_index;\
	cudaError_t err;\
	if( !ind || !outd ) return cudaGetLastError();\
	for(int i = 0; i < nr_samples; ++i)\
	{\
		delay_index = (delay_index - 1) & (nr_taps - 1);\
		kernel<<<fir->blocks_per_grid, fir->threads_per_block, 0>>>(nr_channels, nr_taps, delay_index, ind, fir->d_delay, outd);\
		if( (err = cudaThreadSynchronize()) != cudaSuccess ) return err;\
		ind  += nr_stations * nr_channels * PPF_NR_POLARIZATIONS;\
		outd += nr_stations * nr_channels * PPF_NR_POLARIZATIONS;\
	}\
	fir->delay_index = delay_index;\
	return err;\
}


/************************************************
 *	FIR EXECUTE OPTIMIZED IMPLEMENTATION
 ************************************************/
#else

#define FIR_EXECUTE(name, input_type, sample_type, kernel)\
static cudaError_t name(ppf_fir* fir, const input_type* in, ppf_output* out, int nr_samples)\
{\
	int nr_stations        = fir->nr_stations;\
	int nr_channels        = fir->nr_channels;\
	int nr_taps            = fir->nr_taps;\
	dim3 blocks_per_grid   = fir->blocks_per_grid;\
	dim3 threads_per_block = fir->threads_per_block;\
	sample_type* ind       = (sample_type*)ppf_input_get_device(in);\
	ppf_complex_f32* outd  = (ppf_complex_f32*)ppf_output_get_device(out);\
	uint delay_step        = PPF_NR_POLARIZATIONS * nr_channels;\
	uint buffer_step       = PPF_NR_POLARIZATIONS * nr_stations * nr_channels;\
	if( !ind || !outd ) return cudaGetLastError();\
	kernel<<<blocks_per_grid, threads_per_block, 0>>>(nr_channels, delay_step, buffer_step, ind, outd, fir->d_delay, nr_samples / nr_taps );\
	return cudaThreadSynchronize();\
}

#endif

// 
FIR_EXECUTE(ppf_fir_execute_i16_64, ppf_input_i16, short2, ppf_fir_kernel_i16_64);
FIR_EXECUTE(ppf_fir_execute_i16_32, ppf_input_i16, short2, ppf_fir_kernel_i16_32);
FIR_EXECUTE(ppf_fir_execute_i16_16, ppf_input_i16, short2, ppf_fir_kernel_i16_16);
FIR_EXECUTE(ppf_fir_execute_i16_8 , ppf_input_i16, short2, ppf_fir_kernel_i16_8);
FIR_EXECUTE(ppf_fir_execute_i16_4 , ppf_input_i16, short2, ppf_fir_kernel_i16_4);

FIR_EXECUTE(ppf_fir_execute_i8_64 , ppf_input_i8,  char2, ppf_fir_kernel_i8_64);
FIR_EXECUTE(ppf_fir_execute_i8_32 , ppf_input_i8,  char2, ppf_fir_kernel_i8_32);
FIR_EXECUTE(ppf_fir_execute_i8_16 , ppf_input_i8,  char2, ppf_fir_kernel_i8_16);
FIR_EXECUTE(ppf_fir_execute_i8_8  , ppf_input_i8,  char2, ppf_fir_kernel_i8_8);
FIR_EXECUTE(ppf_fir_execute_i8_4  , ppf_input_i8,  char2, ppf_fir_kernel_i8_4);

FIR_EXECUTE(ppf_fir_execute_i4_64 , ppf_input_i4,  char, ppf_fir_kernel_i4_64);
FIR_EXECUTE(ppf_fir_execute_i4_32 , ppf_input_i4,  char, ppf_fir_kernel_i4_32);
FIR_EXECUTE(ppf_fir_execute_i4_16 , ppf_input_i4,  char, ppf_fir_kernel_i4_16);
FIR_EXECUTE(ppf_fir_execute_i4_8  , ppf_input_i4,  char, ppf_fir_kernel_i4_8);
FIR_EXECUTE(ppf_fir_execute_i4_4  , ppf_input_i4,  char, ppf_fir_kernel_i4_4);

cudaError_t ppf_fir_execute_i16(ppf_fir* fir, const ppf_input_i16* in, ppf_output* out, int nr_samples)
{
	switch(fir->nr_taps)
	{
		case  4: return ppf_fir_execute_i16_4(fir, in, out, nr_samples);
		case  8: return ppf_fir_execute_i16_8(fir, in, out, nr_samples);
		case 16: return ppf_fir_execute_i16_16(fir, in, out, nr_samples);
		case 32: return ppf_fir_execute_i16_32(fir, in, out, nr_samples);
		case 64: return ppf_fir_execute_i16_64(fir, in, out, nr_samples);
		default: return cudaErrorUnknown;
	}
}

cudaError_t ppf_fir_execute_i8(ppf_fir* fir, const ppf_input_i8* in, ppf_output* out, int nr_samples)
{
	switch(fir->nr_taps)
	{
		case  4: return ppf_fir_execute_i8_4(fir, in, out, nr_samples);
		case  8: return ppf_fir_execute_i8_8(fir, in, out, nr_samples);
		case 16: return ppf_fir_execute_i8_16(fir, in, out, nr_samples);
		case 32: return ppf_fir_execute_i8_32(fir, in, out, nr_samples);
		case 64: return ppf_fir_execute_i8_64(fir, in, out, nr_samples);
		default: return cudaErrorUnknown;
	}
}

cudaError_t ppf_fir_execute_i4(ppf_fir* fir, const ppf_input_i4* in, ppf_output* out, int nr_samples)
{
	switch(fir->nr_taps)
	{
		case  4: return ppf_fir_execute_i4_4(fir, in, out, nr_samples);
		case  8: return ppf_fir_execute_i4_8(fir, in, out, nr_samples);
		case 16: return ppf_fir_execute_i4_16(fir, in, out, nr_samples);
		case 32: return ppf_fir_execute_i4_32(fir, in, out, nr_samples);
		case 64: return ppf_fir_execute_i4_64(fir, in, out, nr_samples);
		default: return cudaErrorUnknown;
	}
}


/****************************************************
 *	FFT
 ****************************************************/

ppf_fft* ppf_fft_new(int nr_stations, int nr_channels, int nr_samples)
{
	cufftHandle plan = 0;
	int nr_batches = PPF_NR_POLARIZATIONS * nr_stations * nr_samples;

	if( cufftPlanMany(&plan, 1, &nr_channels, NULL, 1, 0, NULL, 1, 0, CUFFT_C2C, nr_batches) != CUFFT_SUCCESS )
	{
		return NULL;
	}

	ppf_fft* fft = ppf_malloc(ppf_fft, 1);

	if(!fft)
	{
		cufftDestroy(plan);
		return NULL;
	}

	fft->plan = plan;
	return fft;
}

void ppf_fft_free(ppf_fft* fft)
{
	cufftDestroy(fft->plan);
	ppf_free(fft);
}

cufftResult ppf_fft_exec(ppf_fft* fft, ppf_output* out)
{
#if 0//def PAGELOCKED_MEMORY
	// from device to host memory
	return cufftExecC2C(fft->plan, out->d, (ppf_complex_f32*)ppf_get_device_ptr(out->h), CUFFT_FORWARD);
#else
	// in place on the device
	return cufftExecC2C(fft->plan, out->d, out->d, CUFFT_FORWARD);
#endif
}


#endif //PTXINSPECT
