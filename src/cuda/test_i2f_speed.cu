/*
    A Polyphase Filter for GPUs and Multi-Core Processors
    Copyright (C) 2012 Karel van der Veldt (karel.vd.veldt@uva.nl)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <stdlib.h>
#include <cuda.h>
#include "timer.h"

static __global__ void test_i16(const short* in, float* out)
{
	int i = blockIdx.x * blockDim.x + threadIdx.x;
	out[i] = __int2float_rn(in[i]);
}

static __global__ void test_f32(const float* in, float* out)
{
	int i = blockIdx.x * blockDim.x + threadIdx.x;
	out[i] = in[i];
}

static const int N = 1024 * 1024;
static const int threads_per_block = 1024;
static const int blocks_per_grid = N / threads_per_block;

void test_i2f_speed()
{
	short* a_h_i16 = (short*)malloc(sizeof(short) * N);
	float* a_h_f32 = (float*)malloc(sizeof(float) * N);
	short* a_d_i16;
	float* a_d_f32;
	float* b_d_f32;

	for(int i = 0; i < N; ++i)
	{
		a_h_i16[i] = i;
		a_h_f32[i] = i;
	}

	cudaMalloc(&a_d_i16, sizeof(short) * N);
	cudaMalloc(&a_d_f32, sizeof(float) * N);
	cudaMalloc(&b_d_f32, sizeof(float) * N);

	cudaMemcpy(a_d_i16, a_h_i16, sizeof(short) * N, cudaMemcpyHostToDevice);
	cudaMemcpy(a_d_f32, a_h_f32, sizeof(float) * N, cudaMemcpyHostToDevice);

	ticks t0;
	double cpu_mhz = get_cpu_mhz();

	t0 = get_ticks();
	test_i16<<<blocks_per_grid, threads_per_block>>>(a_d_i16, b_d_f32);
	cudaThreadSynchronize();
	double secs_i16 = ticks_to_secs(get_ticks() - t0, cpu_mhz);

	t0 = get_ticks();
	test_f32<<<blocks_per_grid, threads_per_block>>>(a_d_f32, b_d_f32);
	cudaThreadSynchronize();
	double secs_f32 = ticks_to_secs(get_ticks() - t0, cpu_mhz);

	printf(
		"Runs: %d\n"
		"16-bit int: %g secs\n"
		"32-bit  fp: %g secs\n"
		"__int2float overhead: %g%%\n",
		N, secs_i16, secs_f32, secs_i16 / secs_f32
		);

	free(a_h_i16);
	free(a_h_f32);
	cudaFree(a_d_i16);
	cudaFree(a_d_f32);
	cudaFree(b_d_f32);
}
