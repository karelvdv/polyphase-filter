/*
    A Polyphase Filter for GPUs and Multi-Core Processors
    Copyright (C) 2012 Karel van der Veldt (karel.vd.veldt@uva.nl)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __PPF_CUDA_H
#define __PPF_CUDA_H
#include <cuda.h>
#include <cufft.h>

#define PPF_NR_POLARIZATIONS 2

#define ppf_malloc(type, count) (type*)malloc(sizeof(type) * (count))
#define ppf_free(p) free(p)

typedef struct __attribute__((packed))
{
	char x : 4;
	char y : 4;
} ppf_complex_i4;

typedef char2  ppf_complex_i8;
typedef short2 ppf_complex_i16;
typedef float2 ppf_complex_f32;

#define ppf_c_real(cplx) ((cplx).x)
#define ppf_c_imag(cplx) ((cplx).y)

typedef struct
{
	ppf_complex_f32* d_delay;
	int nr_stations;
	int nr_channels;
	int nr_taps;
	int delay_index;
	dim3 threads_per_block;
	dim3 blocks_per_grid;
} ppf_fir;


typedef struct
{
	cufftHandle plan;
} ppf_fft;


// host memory accessable to device
#ifdef PAGELOCKED_MEMORY
#	define ppf_input_get_host(in) ((in)->d)
#	define ppf_input_get_device(in) ppf_get_device_ptr((in)->d)
#	define ppf_input_transfer_i16(in, nr_stations, nr_channels, nr_samples) cudaSuccess
#	define ppf_input_transfer_i8(in, nr_stations, nr_channels, nr_samples) cudaSuccess
#	define ppf_input_transfer_i4(in, nr_stations, nr_channels, nr_samples) cudaSuccess
#	define ppf_output_get_host(out) ((out)->h)
#	define ppf_output_get_device(out) ((out)->d)

typedef struct
{
	ppf_complex_i16* d;
} ppf_input_i16;

typedef struct
{
	ppf_complex_i8* d;
} ppf_input_i8;

typedef struct
{
	ppf_complex_i4* d;
} ppf_input_i4;

typedef struct
{
	ppf_complex_f32* d;
	ppf_complex_f32* h;
} ppf_output;

// seperate host and device buffers
#else
#	define ppf_input_get_host(in) ((in)->h)
#	define ppf_input_get_device(in) ((in)->d)
#	define ppf_output_get_host(out) ((out)->h)
#	define ppf_output_get_device(out) ((out)->d)

typedef struct
{
	ppf_complex_i16* d;
	ppf_complex_i16 h[];
} ppf_input_i16;

typedef struct
{
	ppf_complex_i8* d;
	ppf_complex_i8 h[];
} ppf_input_i8;

typedef struct
{
	ppf_complex_i4* d;
	ppf_complex_i4 h[];
} ppf_input_i4;

typedef struct
{
	ppf_complex_f32* d;
	ppf_complex_f32 h[];
} ppf_output;

cudaError_t ppf_input_transfer_i16(ppf_input_i16* in, int nr_stations, int nr_channels, int nr_samples);
cudaError_t ppf_input_transfer_i8(ppf_input_i8* in, int nr_stations, int nr_channels, int nr_samples);
cudaError_t ppf_input_transfer_i4(ppf_input_i4* in, int nr_stations, int nr_channels, int nr_samples);

#endif

cudaError_t ppf_output_transfer(ppf_output* out, int nr_stations, int nr_channels, int nr_samples);

void ppf_prefer_l1();

ppf_fir* ppf_fir_new(int nr_stations, int nr_channels, int nr_taps);
void ppf_fir_free(ppf_fir* fir);

cudaError_t ppf_fir_set_weights(ppf_fir* fir, const float* weights, int nr_channels, int nr_taps);




cudaError_t ppf_fir_execute_i16(ppf_fir* fir, const ppf_input_i16* in, ppf_output* out, int nr_samples);
cudaError_t ppf_fir_execute_i8(ppf_fir* fir, const ppf_input_i8* in, ppf_output* out, int nr_samples);
cudaError_t ppf_fir_execute_i4(ppf_fir* fir, const ppf_input_i4* in, ppf_output* out, int nr_samples);


ppf_input_i16* ppf_input_new_i16(int nr_stations, int nr_channels);
ppf_input_i8* ppf_input_new_i8(int nr_stations, int nr_channels);
ppf_input_i4* ppf_input_new_i4(int nr_stations, int nr_channels);
void ppf_input_free_i16(ppf_input_i16* in);
void ppf_input_free_i8(ppf_input_i8* in);
void ppf_input_free_i4(ppf_input_i4* in);

// get index to the start of the nth station
static inline unsigned int ppf_input_nth(int station, int nr_channels)
{
	return station * nr_channels * PPF_NR_POLARIZATIONS;
}

/**
 *	Computes the index into the output array of the given sample.
 */
static inline unsigned int ppf_output_nth(int nr_stations, int nr_channels,
	int station, int channel, int polarization)
{
#if 1
	// output[p][s][c]
	return (polarization * nr_stations + station) * nr_channels + channel;
#else
	// output[s][p][c]
	return (station * PPF_NR_POLARIZATIONS + polarization) * nr_channels + channel;
#endif
}

ppf_output* ppf_output_new(int nr_stations, int nr_channels);
void ppf_output_free(ppf_output* out);

ppf_fft* ppf_fft_new(int nr_stations, int nr_channels, int nr_samples);
void ppf_fft_free(ppf_fft* fft);
cufftResult ppf_fft_exec(ppf_fft* fft, ppf_output* out);

static inline void* ppf_get_device_ptr(void* host)
{
	void* dev = NULL;
	return cudaHostGetDevicePointer(&dev, host, 0) == cudaSuccess ? dev : NULL;
}


//extern const float ppf_orig_weights[256 * 16];

#endif
