/*
    A Polyphase Filter for GPUs and Multi-Core Processors
    Copyright (C) 2012 Karel van der Veldt (karel.vd.veldt@uva.nl)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <cuda.h>
#include "ppf.h"
#include "timer.h"
#include "orig_weights.h"

#if !defined(DATATYPE)
#	error "Definition DATATYPE must be set to 4, 8 or 16!"
#elif DATATYPE == 16
#	define COMPLEX    ppf_complex_i16
#	define INPUT      ppf_input_i16
#	define INPUT_NEW  ppf_input_new_i16
#	define INPUT_TRANSFER ppf_input_transfer_i16
#	define INPUT_FREE ppf_input_free_i16
#	define FIR_EXEC   ppf_fir_execute_i16
#elif DATATYPE == 8
#	define COMPLEX    ppf_complex_i8
#	define INPUT      ppf_input_i8
#	define INPUT_NEW  ppf_input_new_i8
#	define INPUT_TRANSFER ppf_input_transfer_i8
#	define INPUT_FREE ppf_input_free_i8
#	define FIR_EXEC   ppf_fir_execute_i8
#elif DATATYPE == 4
#	define COMPLEX    ppf_complex_i4
#	define INPUT      ppf_input_i4
#	define INPUT_NEW  ppf_input_new_i4
#	define INPUT_TRANSFER ppf_input_transfer_i4
#	define INPUT_FREE ppf_input_free_i4
#	define FIR_EXEC   ppf_fir_execute_i4
#else
#	error "Unimplemented DATATYPE."
#endif

cudaError_t pr_cuda_err(const char* str, cudaError_t err)
{
	if(err != cudaSuccess) fprintf(stderr, "%s: %s\n", str, cudaGetErrorString(err));
	return err;
}

void make_input(COMPLEX* input, int nr_channels)
{
	// make up some input data
	for(int i = 0; i < nr_channels; ++i, input += PPF_NR_POLARIZATIONS)
	{
		float a = (float)i;
		ppf_c_real(input[0]) =  100.0 * sinf( 2.0 * M_PI * (a / 256.0) ); // polarization 0
		ppf_c_imag(input[0]) =  100.0 * cosf( 2.0 * M_PI * (a / 256.0) );
		ppf_c_real(input[1]) =  ppf_c_real(input[0]); //sinf( 2.0 * M_PI * (a / 256.0) ); // polarization 1
		ppf_c_imag(input[1]) = -ppf_c_imag(input[0]); //-cosf( 2.0 * M_PI * (a / 256.0) );
	}
}

int roundup(int n, int m)
{
	int t = n % m;
	return t == 0 ? n : n + (m - t);
}

void perf_test(int nr_stations, int nr_channels, int nr_taps, int nr_runs, int nr_batches, int do_fir, int do_fft, int do_io)
{
#ifdef REF_IMPL
	int nr_samples = 1;
#else
	int nr_samples = nr_taps * nr_batches;
#endif
	nr_runs = roundup(nr_runs, nr_samples);
	INPUT* input       = INPUT_NEW(nr_stations * nr_samples, nr_channels);
	ppf_output* output = ppf_output_new(nr_stations * nr_samples, nr_channels);
	ppf_fir* fir       = ppf_fir_new(nr_stations, nr_channels, nr_taps);
	ppf_fft* fft       = ppf_fft_new(nr_stations, nr_channels, nr_samples);
	double cpu_mhz     = get_cpu_mhz();
	double secs        = 0.0;

#ifdef PREFER_L1
	ppf_prefer_l1();
#endif

	if( !input || !output || !fir )
	{
		printf("Out of memory %d %d %d\n", !input, !output, !fir);
		exit(-1);
	}

	ppf_fir_set_weights(fir, ppf_orig_weights, nr_channels, nr_taps);

	for(int i = 0; i < nr_stations * nr_samples; ++i)
	{
		make_input( ppf_input_get_host(input) + ppf_input_nth(i, nr_channels), nr_channels );
	}

	for(int i = 0; i < nr_runs; i += nr_samples)
	{
		ticks t0 = get_ticks();

		if(do_io)  pr_cuda_err("input" , INPUT_TRANSFER(input, nr_stations, nr_channels, nr_samples) );
		if(do_fir) pr_cuda_err("fir"   , FIR_EXEC(fir, input, output, nr_samples) );

		if(do_fft && ppf_fft_exec(fft, output) != CUFFT_SUCCESS) fprintf(stderr, "fft\n");

		if( do_io ) pr_cuda_err("output", ppf_output_transfer(output, nr_stations, nr_channels, nr_samples) );

		secs += ticks_to_secs( get_ticks() - t0, cpu_mhz );
	}

	double io_bytes = sizeof(COMPLEX) + sizeof(ppf_complex_f32);
	double io_gb    = 0;

	if(do_io)
	{
		//io_bytes = sizeof(COMPLEX) + sizeof(ppf_complex_f32);
		io_gb    = PPF_NR_POLARIZATIONS * nr_stations * nr_channels * io_bytes * (nr_runs / (1024.0 * 1024.0 * 1024.0));
	}

	double fir_bytes = 0;
	double fir_flop  = 0;
	double fir_ai    = 0;
	double fir_gb    = 0;
	double fir_gflop = 0;

	// r 1 x sample
	// w 1 x f32*2 output
	// r nr_taps x f32 weights
	// r nr_taps - 1 x f32*2 taps
	// w nr_taps - 1 x f32*2 taps
	if(do_fir)
	{
		fir_bytes = sizeof(float) * nr_taps + 2 * (sizeof(ppf_complex_f32) * (nr_taps - 1 + 1) / nr_samples);
		fir_flop  = 2 + 4 * (nr_taps - 1);
		fir_ai    = fir_flop / (io_bytes + fir_bytes);
		fir_gb    = PPF_NR_POLARIZATIONS * nr_stations * nr_channels * fir_bytes * (nr_runs / (1024.0 * 1024.0 * 1024.0));
		fir_gflop = PPF_NR_POLARIZATIONS * nr_stations * nr_channels * fir_flop  * (nr_runs / 1000000000.0);
	}

	// fft
	double fft_bytes = 0;
	double fft_flop  = 0;
	double fft_ai    = 0;
	double fft_gb    = 0;
	double fft_gflop = 0;

	if(do_fft)
	{
		fft_bytes = 4.0 * nr_channels * sizeof(ppf_complex_f32);
		fft_flop  = 5.0 * nr_channels * (log(nr_channels) / log(2));
		fft_ai    = fft_flop / fft_bytes;
		fft_gb    = PPF_NR_POLARIZATIONS * nr_stations * fft_bytes * (nr_runs / (1024.0 * 1024.0 * 1024.0));
		fft_gflop = PPF_NR_POLARIZATIONS * nr_stations * fft_flop * (nr_runs / 1000000000.0);
	}

	printf("Stat Chan Taps Runs GB Time(s) GB/s GFLOP/s AI Samples\n");
	printf("% 4d % 4d % 4d % 4d %g "
		"% 2.4g % 1.2g % 2.4g % 1.2g "
		"%d\n",
		nr_stations, nr_channels, nr_taps, nr_runs, io_gb + fir_gb + fft_gb,
		secs, (io_gb + fir_gb + fft_gb) / secs, (fir_gflop + fft_gflop) / secs, fir_ai + fft_ai, nr_samples);

	ppf_fir_free(fir);
	ppf_fft_free(fft);
	INPUT_FREE(input);
	ppf_output_free(output);
}


void fft_perf(int nr_stations, int nr_channels, int nr_runs, int do_io)
{
	ppf_input_i16* input = ppf_input_new_i16(nr_stations * 2, nr_channels);
	ppf_output* output   = ppf_output_new(nr_stations, nr_channels);
	ppf_fft* fft         = ppf_fft_new(nr_stations, nr_channels, 1);
	double cpu_mhz       = get_cpu_mhz();
	double secs          = 0.0;
	
	for(int i = 0; i < nr_runs; ++i)
	{
		ticks t0 = get_ticks();

		if(do_io) pr_cuda_err("input" , ppf_input_transfer_i16(input, nr_stations, nr_channels, 1) );

		if(ppf_fft_exec(fft, output) != CUFFT_SUCCESS) fprintf(stderr, "fft\n");

		if(do_io) pr_cuda_err("output", ppf_output_transfer(output, nr_stations, nr_channels, 1) );

		secs += ticks_to_secs( get_ticks() - t0, cpu_mhz );
	}
	
	double fft_flop  = 5.0 * nr_channels * (log(nr_channels) / log(2));
	double fft_gflop = PPF_NR_POLARIZATIONS * nr_stations * fft_flop * (nr_runs / 1000000000.0);
	
	printf("Stat Chan Runs Time T/Run GFLOP/s\n");
	printf("% 4d % 4d %4d %g %g %g\n", nr_stations, nr_channels, nr_runs, secs, secs / nr_runs, fft_gflop / secs);
}


void moving_avg_test()
{
	const float weights[] = {0.0625, 0.0625, 0.0625, 0.0625, 0.0625, 0.0625, 0.0625, 0.0625, // ch 0
							 0.0625, 0.0625, 0.0625, 0.0625, 0.0625, 0.0625, 0.0625, 0.0625,
							 0.0625, 0.0625, 0.0625, 0.0625, 0.0625, 0.0625, 0.0625, 0.0625, // ch 1
							 0.0625, 0.0625, 0.0625, 0.0625, 0.0625, 0.0625, 0.0625, 0.0625};
	const int nr_stations = 2;
	const int nr_channels = 2;
	const int nr_taps     = 16;
	const int nr_runs     = 32; // total number of samples
#ifdef REF_IMPL
	const int nr_batches = nr_runs; // number of batches
	const int nr_samples = 1;       // number of samples per batch
#else
	const int nr_batches = 1;
	const int nr_samples = nr_runs;
#endif

	pr_cuda_err( "cudaSetDeviceFlags", cudaSetDeviceFlags(cudaDeviceMapHost) );

	ppf_fir* fir         = ppf_fir_new(nr_stations, nr_channels, nr_taps);
	INPUT* input         = INPUT_NEW(nr_stations * nr_samples, nr_channels);
	ppf_output* output   = ppf_output_new(nr_stations * nr_samples, nr_channels);
	ppf_complex_f32* out = ppf_output_get_host(output);
	COMPLEX* in          = ppf_input_get_host(input);

	ppf_fir_set_weights(fir, weights, nr_channels, nr_taps);

	cudaMemset(fir->d_delay, 0, sizeof(ppf_complex_f32) * nr_stations * nr_channels * nr_taps * PPF_NR_POLARIZATIONS);

	memset( ppf_input_get_host(input), 0, sizeof(COMPLEX) * nr_stations * nr_channels * nr_samples * PPF_NR_POLARIZATIONS );

#ifdef REF_IMPL
	for(int j = 0; j < nr_stations * nr_channels * PPF_NR_POLARIZATIONS; ++j)
	{
		ppf_c_real(in[j]) = j;
		ppf_c_imag(in[j]) = j;
	}

	for(int i = 0; i < nr_samples * nr_batches; ++i)
	{
		pr_cuda_err("input", INPUT_TRANSFER(input, nr_stations, nr_channels, nr_samples) );
		pr_cuda_err("exec", FIR_EXEC(fir, input, output, nr_samples) );
		pr_cuda_err("output", ppf_output_transfer(output, nr_stations, nr_channels, nr_samples) );

		for(int j = 0; j < nr_stations * nr_channels * PPF_NR_POLARIZATIONS; ++j)
		{
			ppf_c_real(in[j])  = j;
			ppf_c_imag(in[j]) += 1;
		}

		for(int j = 0; j < nr_stations * nr_channels * PPF_NR_POLARIZATIONS; ++j)
		{
			printf( "(%.1f %.1f) ", ppf_c_real(out[j]), ppf_c_imag(out[j]) );
		}
		printf("\n");
	}
#else
	for(int b = 0; b < nr_batches; ++b)
	{
		for(int i = 0; i < nr_samples; ++i)
		{
			for(int j = 0; j < nr_stations * nr_channels * PPF_NR_POLARIZATIONS; ++j)
			{	
				ppf_c_real(in[j]) = j;
				ppf_c_imag(in[j]) = j + i;
			}
			in += nr_stations * nr_channels * PPF_NR_POLARIZATIONS;
		}

		pr_cuda_err("input", INPUT_TRANSFER(input, nr_stations, nr_channels, nr_samples) );
		pr_cuda_err("exec", FIR_EXEC(fir, input, output, nr_samples) );
		pr_cuda_err("output", ppf_output_transfer(output, nr_stations, nr_channels, nr_samples) );

		for(int i = 0; i < nr_samples; ++i)
		{
			for(int j = 0; j < nr_stations * nr_channels * PPF_NR_POLARIZATIONS; ++j)
			{
				printf( "(%.1f %.1f) ", ppf_c_real(out[j]), ppf_c_imag(out[j]) );
			}
			printf("\n");

			out += nr_stations * nr_channels * PPF_NR_POLARIZATIONS;
		}
	}
#endif

	ppf_fir_free(fir);
	INPUT_FREE(input);
	ppf_output_free(output);
}


/*void my_test(int mb)
{
	size_t sz = sizeof(float) * mb * 256;
	float* f = (float*)malloc(sz);
	printf("f=%p\n", f);
	free(f);
}*/



void print_devices()
{
	int count;
	struct cudaDeviceProp p;

	cudaGetDeviceCount(&count);

	for(int i = 0; i < count; ++i)
	{
		cudaGetDeviceProperties(&p, i);

		printf(
			"Device #%d: %s\n"
			"-- Global Memory: %lu bytes (%lu MB)\n"
			"-- Shared Memory Per Block: %lu bytes\n"
			"-- Registers Per Block: %d\n"
			"-- Warp Size: %d threads\n"
			"-- Max Pitch: %lu bytes (%lu MB)\n"
			"-- Max Threads Per Block: %d\n"
			"-- Max Thread Dimensions: %d x %d x %d\n"
			"-- Max Grid Size: %d x %d x %d\n"
			"-- Constant Memory: %lu bytes\n"
			"-- Compute Ability: %d.%d\n"
			"-- Clock Rate: %d KHz (%d MHz)\n"
			"-- Texture Alignment: %lu bytes\n"
			"-- Concurrent Copying: %s\n"
			"-- Multi Processors: %d\n"
			"-- Kernel Timeout: %s\n"
			"-- Integrated GPU: %s\n"
			"-- Can Map Host Memory Into Device: %s\n"
			"-- Compute Mode: %d\n",
			i, p.name,
			p.totalGlobalMem, p.totalGlobalMem / (1024 * 1024),
			p.sharedMemPerBlock,
			p.regsPerBlock,
			p.warpSize,
			p.memPitch, p.memPitch / (1024 * 1024),
			p.maxThreadsPerBlock,
			p.maxThreadsDim[0], p.maxThreadsDim[1], p.maxThreadsDim[2],
			p.maxGridSize[0], p.maxGridSize[1], p.maxGridSize[2],
			p.totalConstMem,
			p.major, p.minor,
			p.clockRate, p.clockRate / 1000,
			p.textureAlignment,
			p.deviceOverlap ? "yes" : "no",
			p.multiProcessorCount,
			p.kernelExecTimeoutEnabled ? "yes" : "no",
			p.integrated ? "yes" : "no",
			p.canMapHostMemory ? "yes" : "no",
			p.computeMode
		);
	}
}


void usage()
{
#ifdef REF_IMPL
#	define EXENAME "ppf-cuda-test-ref"
#else
#	define EXENAME "ppf-cuda-test"
#endif

	printf(
		"Polyphase filter CUDA "
#ifdef REF_IMPL
		"reference implementation\n"
#else
		"optimized implementation\n"
#endif
		"usage: " EXENAME "-i%d devices\n"
		"       " EXENAME "-i%d perf [stations channels taps runs batches device io fir fft]\n"
		"       " EXENAME "-i%d fir\n"
		"       " EXENAME "-i%d fftperf stations channels runs io\n"
		"modes: devices -- prints device information\n"
		"       perf    -- gather performance statistics\n"
		"       fir     -- fir moving average\n",
		DATATYPE, DATATYPE, DATATYPE, DATATYPE
		);
}

int main(int argc, char** argv)
{
	int nr_stations = 16;
	int nr_channels = 256;
	int nr_taps     = 16;
	int nr_runs     = 10000;
	int device      = 0;
	int nr_batches  = 1;
	int do_fir      = 1;
	int do_fft      = 0;
	int do_io       = 1;

	if( argc >= 2 )
	{
		if( !strcmp(argv[1], "devices") )
		{
			print_devices();
		}
		else if( !strcmp(argv[1], "fir") )
		{
			moving_avg_test();
		}
		else if( !strcmp(argv[1], "perf") && argc >= 11 )
		{
			nr_stations  = atoi(argv[2]);
			nr_channels  = atoi(argv[3]);
			nr_taps      = atoi(argv[4]);
			nr_runs      = atoi(argv[5]);
			nr_batches   = atoi(argv[6]);
			device       = atoi(argv[7]);
			do_io        = atoi(argv[8]);
			do_fir       = atoi(argv[9]);
			do_fft       = atoi(argv[10]);

			// In principle the PPF could run on multiple devices simultaneously, but not in this test program.
			if( cudaSetDevice(device) != cudaSuccess )
			{
				printf("Invalid device chosen (%d).\n", device);
				exit(-1);
			}

			pr_cuda_err( "cudaSetDeviceFlags", cudaSetDeviceFlags(cudaDeviceMapHost) );

			perf_test(nr_stations, nr_channels, nr_taps, nr_runs, nr_batches, do_fir, do_fft, do_io);
		}
		else if( !strcmp(argv[1], "fftperf") && argc >= 6 )
		{
			nr_stations = atoi(argv[2]);
			nr_channels = atoi(argv[3]);
			nr_runs     = atoi(argv[4]);
			do_io       = atoi(argv[5]);
			cudaSetDevice(0);
			cudaSetDeviceFlags(cudaDeviceMapHost);
			fft_perf(nr_stations, nr_channels, nr_runs, do_io);
		}
		else
		{
			usage();
		}
	}
	else
	{
		usage();
	}

	return 0;
}
