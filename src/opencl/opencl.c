/*
    A Polyphase Filter for GPUs and Multi-Core Processors
    Copyright (C) 2012 Karel van der Veldt (karel.vd.veldt@uva.nl)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <memory.h>
#include <stdarg.h>
#include <assert.h>
#include <stdlib.h>
#include "opencl.h"

/*
static void cl_error_callback(const char* errinfo, const void* private_info, size_t cb, void* user_data)
{
	fprintf(stderr, "OpenCL error: %s\n", errinfo);
}
*/

void opencl_assert(int test, const char* fmt, ...)
{
	if(test)
	{
		va_list args;
		va_start(args, fmt);
		vfprintf(stderr, fmt, args);
		va_end(args);
		exit(1);
	}
}


opencl_t* opencl_new(cl_uint platform, cl_uint device, cl_device_type device_type)
{
	cl_platform_id platforms[8];
	cl_device_id devs[8];
	cl_uint nplatforms = 0;
	cl_uint ndevices = 0;
	cl_int err = CL_SUCCESS;
	cl_platform_id p;
	cl_device_id d;
	
	err = clGetPlatformIDs(0, NULL, &nplatforms);
	opencl_assert(err != CL_SUCCESS, "clGetPlatformIDs = %d\n", err);
	err = clGetPlatformIDs(nplatforms, platforms, NULL);
	opencl_assert(err != CL_SUCCESS, "clGetPlatformIDs = %d\n", err);
	opencl_assert(platform >= nplatforms, "platform >= nplatforms\n");
	
	p = platforms[platform];
	
	const cl_context_properties props[3] = {
		CL_CONTEXT_PLATFORM,
		(cl_context_properties)p,
		0
	};
	
	err = clGetDeviceIDs(p, device_type, 0, NULL, &ndevices);
	opencl_assert(err != CL_SUCCESS, "clGetDeviceIDs = %d\n", err);
	err = clGetDeviceIDs(p, device_type, ndevices, devs, NULL);
	opencl_assert(err != CL_SUCCESS, "clGetDeviceIDs = %d\n", err);
	opencl_assert(device >= ndevices, "device >= ndevices\n");
	
	d = devs[device];
	
#if 0
	static char name[256];
	clGetDeviceInfo(d, CL_DEVICE_NAME, 256, name, NULL);
	fprintf(stderr, "Device name: %s\n", name);
#endif
	
	cl_context context = clCreateContext(props, 1, &d, NULL/*cl_error_callback*/, NULL, &err);
	opencl_assert(err != CL_SUCCESS, "clCreateContext = %d\n", err);
	
	cl_command_queue queue = clCreateCommandQueue(context, d, 0, &err);
	opencl_assert(err != CL_SUCCESS, "clCreateCommandQueue = %d", err);
	
	opencl_t* opencl = (opencl_t*)malloc(sizeof(opencl_t));
	opencl_assert(!opencl, "out of memory\n");
	
	opencl->context  = context;
	opencl->queue    = queue;
	opencl->platform = p;
	opencl->device   = d;
	
	return opencl;
}

void opencl_free(opencl_t* opencl)
{
	clReleaseCommandQueue(opencl->queue);
	clReleaseContext(opencl->context);
	free(opencl);
}

opencl_program_t* opencl_compile(opencl_t* cl, const void* code, size_t len,
	const char* build_options, const char** kernels, int nkernels, cl_uint binary)
{
	cl_int err;
	cl_int binerr = CL_SUCCESS;
	cl_program program;
	
	if(binary)
	{
		program = clCreateProgramWithBinary(cl->context, 1, &cl->device, &len, (const unsigned char**)&code, &binerr, &err);
		opencl_assert(err != CL_SUCCESS || binerr != CL_SUCCESS, "clCreateProgramWithBinary = (%d, %d)\n", err, binerr);
	}
	else
	{
		program = clCreateProgramWithSource(cl->context, 1, (const char**)&code, &len, &err);
		opencl_assert(err != CL_SUCCESS, "clCreateProgramWithSource = %d\n", err);
	}
	
	err = clBuildProgram(program, 0, NULL, build_options, NULL, NULL);
	opencl_assert(err != CL_SUCCESS, "clBuildProgram = %s (%d)\n", opencl_err_string(err), err);
	
	opencl_program_t* prog =
		(opencl_program_t*)malloc(sizeof(opencl_program_t) + sizeof(cl_kernel) * nkernels);
	opencl_assert(!prog, "out of memory\n");
	
	prog->nkernels = nkernels;
	prog->program  = program;
	
	for(int i = 0; i < nkernels; ++i)
	{
		prog->kernels[i] = clCreateKernel(program, kernels[i], &err);
		opencl_assert(err != CL_SUCCESS, "clCreateKernel %s = %d\n", kernels[i], err);
	}
	
	return prog;
}

void opencl_program_free(opencl_program_t* program)
{
	for(int i = 0; i < program->nkernels; ++i)
	{
		clReleaseKernel(program->kernels[i]);
	}
	
	clReleaseProgram(program->program);
	free(program);
}


cl_int opencl_call(opencl_t* cl, cl_kernel kernel,
	cl_uint work_dim, const size_t* global_work_size, const size_t* local_work_size,
	const char* argfmt, ...)
{
	va_list ap;
	cl_uint i = 0;
	char c;
	void* arg;
	size_t arg_size;
	cl_int err;
	
	cl_ushort s;
	cl_uint u;
	cl_mem* b;
	
	va_start(ap, argfmt);
	
	while( (c = *argfmt++) != 0 )
	{
		switch(c)
		{
		case 'I':
			arg_size = sizeof(cl_uint);
			u = va_arg(ap, cl_uint);
			arg = &u;
			break;
		case 'b':
			arg_size = sizeof(cl_mem);
			b = va_arg(ap, cl_mem*);
			arg = &b;
			break;
		case 'S':
			arg_size = sizeof(cl_ushort);
			s = va_arg(ap, cl_uint);
			arg = &s;
			break;
		default:
			return CL_INVALID_ARG_VALUE;
		}
		
		if( (err = clSetKernelArg(kernel, i++, arg_size, arg)) != CL_SUCCESS )
		{
			return err;
		}
	}
	
	va_end(ap);
	
	err = clEnqueueNDRangeKernel(cl->queue, kernel, work_dim,
		NULL, global_work_size, local_work_size, 0, NULL, NULL);
	return err == CL_SUCCESS ? clFinish(cl->queue) : err;
}


#define ERROR(err) case err: return #err

const char* opencl_err_string(cl_int err)
{
	switch(err)
	{
		ERROR(CL_SUCCESS);
		ERROR(CL_INVALID_COMMAND_QUEUE);
		ERROR(CL_INVALID_MEM_OBJECT);
		ERROR(CL_INVALID_VALUE);
		ERROR(CL_INVALID_EVENT_WAIT_LIST);
		ERROR(CL_MEM_OBJECT_ALLOCATION_FAILURE);
		ERROR(CL_OUT_OF_HOST_MEMORY);
		ERROR(CL_INVALID_PROGRAM_EXECUTABLE);
		ERROR(CL_INVALID_KERNEL);
		ERROR(CL_INVALID_CONTEXT);
		ERROR(CL_INVALID_KERNEL_ARGS);
		ERROR(CL_INVALID_WORK_DIMENSION);
		ERROR(CL_INVALID_GLOBAL_WORK_SIZE);
		ERROR(CL_INVALID_GLOBAL_OFFSET);
		ERROR(CL_INVALID_WORK_GROUP_SIZE);
		ERROR(CL_INVALID_WORK_ITEM_SIZE);
#ifdef CL_MISALIGNED_SUB_BUFFER_OFFSET
		ERROR(CL_MISALIGNED_SUB_BUFFER_OFFSET);
#endif
		ERROR(CL_INVALID_IMAGE_SIZE);
		default: return "UNKNOWN ERROR";
	}
}



cl_int opencl_memset(opencl_t* cl, cl_mem buf, cl_uint val, size_t num)
{
	static char code[] =
		"__kernel void memset(__global uint* mem, uint val) { mem[get_global_id(0)] = val; }";
	static const char* kernels[] = {"memset"};
	static opencl_program_t* prog = NULL;
	
	if(!prog)
	{
		prog = opencl_compile(cl, code, sizeof(code), "", (const char**)&kernels, 1, 0);
	}
	
	return opencl_call(cl, prog->kernels[0], 1, &num, NULL, "bI", buf, val);
}
