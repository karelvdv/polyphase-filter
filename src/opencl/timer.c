/*
    A Polyphase Filter for GPUs and Multi-Core Processors
    Copyright (C) 2012 Karel van der Veldt (karel.vd.veldt@uva.nl)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

double get_cpu_mhz()
{
#if defined __linux__
	static char buffer[256];
	FILE* in = fopen("/proc/cpuinfo", "r");
	char* colon;

	while ( !feof(in) )
	{
		for(int i = 0; i < 256; ++i)
		{
			buffer[i] = fgetc(in);
			if(buffer[i] == '\n') break;
		}
		
		if( strncmp("cpu MHz", buffer, 7) == 0 && (colon = strchr(buffer, ':')) != 0 )
		{
			fclose(in);
			return atof(colon + 2);
		}
	}
	
	fclose(in);
#endif
    return 0.0;
}
