__kernel void vectorAdd(__global const float* a, __global float* b) 
{
	int n = get_global_id(0); 
	b[n] = a[n] + 1;
}