/*
    A Polyphase Filter for GPUs and Multi-Core Processors
    Copyright (C) 2012 Karel van der Veldt (karel.vd.veldt@uva.nl)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <sys/stat.h>
#include <string.h>
#include <unistd.h>
#include <stdarg.h>
#include <stdlib.h>
#include <CL/cl.h>
#include "opencl.h"

static inline void myassert(int test, const char* fmt, ...)
{
	if(test)
	{
		va_list args;
		va_start(args, fmt);
		vfprintf(stderr, fmt, args);
		va_end(args);
		exit(1);
	}
}

void usage()
{
	printf(
		"usage: clcheck [-b bin] [-t target] file [options]\n"
		"  -b bin    Write the compiled binary to bin.\n"
		"  -t target Compile for a given platform, device and type.\n"
		"            type = a(ll), c(pu), g(pu)\n"
		"            ex: -t 0g1 (platform 0, gpu device 1)\n"
		"  options   Given without leading -.\n"
	);
}

int main(int argc, char** argv)
{
	static char build_options[1024];
	char* log = NULL;
	size_t loglen = 0;
	char* code = NULL;
	size_t codelen = 0;
	
	char* infile = NULL;
	FILE* f;
	struct stat fs;
	
	/*cl_context context;
	cl_platform_id* platforms = NULL;
	cl_uint nplatforms = 0;
	cl_device_id* devs = NULL;
	cl_uint ndevices = 0;*/
	cl_int errcode = CL_SUCCESS;
	cl_program program;
	cl_int err;
	
	unsigned char* binary = NULL;
	size_t binlen = 0;
	char* binpath = NULL;
	FILE* binfile = NULL;
	cl_uint tgt_platform = 0;
	cl_uint tgt_device = 0;
	cl_uint tgt_type = CL_DEVICE_TYPE_ALL;
	char c;
	
	if(argc < 2)
	{
		usage();
		return 1;
	}
	
	while ((c = getopt (argc, argv, "b:t:")) != -1 )
	{
		switch (c)
		{
		case 'b':
			binpath = optarg;
			break;
		case 't':
			tgt_platform = optarg[0] - '0';
			tgt_device   = optarg[2] - '0';
			switch(optarg[1])
			{
			case 'a': tgt_type = CL_DEVICE_TYPE_ALL; break;
			case 'c': tgt_type = CL_DEVICE_TYPE_CPU; break;
			case 'g': tgt_type = CL_DEVICE_TYPE_GPU; break;
			default:
				fprintf(stderr, "Invalid type \'%c\'\n", optarg[1]);
				return 1;
			}
			break;
		/*case 'f':
			infile = optarg;
			break;
		case 'o':
			strcat(build_options, "-");
			strcat(build_options, optarg);
			strcat(build_options, " ");
			break;*/
		case '?':
		default:
			fprintf (stderr, "Unknown option character `\\x%x'.\n", optopt);
			return 1;
		}
	}	
	
	myassert(optind >= argc, "no file given\n");
	infile = argv[optind];
	
	for(int i = optind + 1; i < argc; ++i)
	{
		strcat(build_options, "-");
		strcat(build_options, argv[i]);
		strcat(build_options, " ");
	}
	
	//fprintf(stderr, "build_options=%s\n", build_options);
	
	myassert(!infile, "must choose file with -f\n");
	
	f = fopen(infile, "rb");
	myassert(!f, "file not found \'%s\'\n", infile);	
	fstat(fileno(f), &fs);
	codelen = (size_t)fs.st_size;
	
	code = (char*)malloc(codelen);
	myassert(!code, "out of memory\n");
	myassert( fread(code, sizeof(char), codelen, f) != codelen, "read error\n" );
	
	
	opencl_t* cl = opencl_new(tgt_platform, tgt_device, tgt_type);
	
	program = clCreateProgramWithSource(cl->context, 1, (const char**)&code, &codelen, &err);
	myassert(err != CL_SUCCESS, "program error\n");
	
	if( (errcode = clBuildProgram(program, 0, NULL, build_options, NULL, NULL)) != CL_SUCCESS )
	{
		cl_int err = clGetProgramBuildInfo(program, cl->device, CL_PROGRAM_BUILD_LOG, 0, NULL, &loglen);
		myassert(err != CL_SUCCESS, "log error (A) %d.\n", err);
		
		log = (char*)malloc(loglen);
		myassert(!log, "out of memory.\n");
		
		err = clGetProgramBuildInfo(program, cl->device, CL_PROGRAM_BUILD_LOG, loglen, log, NULL);
		myassert(err != CL_SUCCESS, "log error (B) %d\n", err);
		
		fprintf(stderr, "%*s", (int)loglen, log);
		free(log);
		return 1;
	}
	
	if(binpath)
	{
		binfile = fopen(binpath, "wb");
		myassert(!binfile, "file not found \'%s\'\n", binpath);
		
		myassert(
			clGetProgramInfo(program, CL_PROGRAM_BINARY_SIZES, sizeof(size_t*), (size_t**)&binlen, NULL) != CL_SUCCESS,
			"binaries error.\n"
		);
		
		binary = (unsigned char*)malloc(binlen);
		myassert(!binary, "out of memory.\n");
		
		myassert(
			clGetProgramInfo(program, CL_PROGRAM_BINARIES, sizeof(char*), (unsigned char**)&binary, NULL) != CL_SUCCESS,
			"binaries error.\n"
		);
		
		myassert(fwrite(binary, 1, binlen, binfile) != binlen, "write error.\n");
		
		fclose(binfile);
	}
	
	//fprintf(stderr, "%s ok\n", argv[1]);
	
	return 0;
}