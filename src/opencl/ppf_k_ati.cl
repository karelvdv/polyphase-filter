/*
    A Polyphase Filter for GPUs and Multi-Core Processors
    Copyright (C) 2012 Karel van der Veldt (karel.vd.veldt@uva.nl)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
/*
	This implementation is meant for ATI cards.

	2 * nr_channels = threads_per_block * blocks_per_station
	threads_per_block <= max_threads_per_block
	Local work  = threads_per_block x 1
	Global work = (nr_stations * threads_per_block) x block_per_station
	
	Global work
	+-------> X (nr_stations)
	|S_0_0 S_1_0 S_n_0
	|S_0_1 ...
	|S_0_b ...
	V
	Y (blocks_per_station)
	
	Local work:
	+-----> X (threads_per_block)
	| C_0 C_1 C_2 C_3 ... C_n
	
	blocks_per_station * threads_per_block = nr_channels
	
	OpenCL     CUDA	
	work item  thread
	work group block
	__private  local
	__local    shared
	__constant constant
	__global   global
	
	weights
	+-----> channels
	|0123456...nr_channels, offset=channel, stride=nr_channels
	|
	V
	taps
 */

#define PPF_NR_POLARIZATIONS 2


/**
 *	Index into the delay line.
 */
inline uint delay_nth(uint nr_stations, uint nr_channels, uint nr_taps,
	uint station, uint channel, uint delay_index)
{
	// delay[nr_stations][nr_taps][nr_channels]
	// delay[s][t][c] = s * nr_taps * nr_channels + t * nr_channels + c
	//                   = ((s * nr_taps + t) * nr_channels + c)
	return ((station * nr_taps + delay_index) * nr_channels + channel);
}

/**
 *	Index into the input array.
 */
inline uint input_nth(uint nr_stations, uint nr_channels,
	uint station, uint channel, uint nth_sample)
{
	// in[nr_samples][nr_stations][nr_channels]
	// in[n][s][c] = n * nr_samples * nr_stations * nr_channels + s * nr_channels + c
	//                = ((n * nr_stations + s) * nr_channels + c)
	return ((nth_sample * nr_stations + station) * nr_channels + channel);
}

/**
 *	Index into the output array.
 */
inline uint output_nth(uint nr_stations, uint nr_channels,
	uint station, uint channel, uint polarization, uint nth_sample)
{
#if 1
	// out[nr_samples][nr_polarizations][nr_stations][nr_channels]
	// out[n][p][s][c] = n * nr_polarizations * nr_stations * nr_channels + p * nr_stations * nr_channels + s * nr_channels + c
	//                 = ((n * nr_polarizations + p) * nr_stations + s) * nr_channels + c
	return ((nth_sample * PPF_NR_POLARIZATIONS + polarization) * nr_stations + station) * nr_channels + channel;
#else
	// out[nr_samples][nr_stations][nr_polarizations][nr_channels]
	// out[n][s][p][c] = n * nr_stations * nr_polarizations * nr_channels + s * nr_polarizations * nr_channels + p * nr_channels + c
	//                 = ((n * nr_stations + s) * nr_polarizations + p) * nr_channels + c
	return ((nth_sample * nr_stations + station) * PPF_NR_POLARIZATIONS + polarization) * nr_channels + channel;
#endif
}

/**
 *	Convert 4-bit samples to floating point.
 */
inline float4 conv_i4_float4(char2 sample)
{
	// bit twiddling sign extension
	short4 a = {
		(short)( (uchar)sample.x &  0x0f ) | ( 0xfff0 * (((uchar)sample.x >> 3) & 1) ),
		(short)( (uchar)sample.x >>    4 ) | ( 0xfff0 * (((uchar)sample.x >> 7) & 1) ),
		(short)( (uchar)sample.y &  0x0f ) | ( 0xfff0 * (((uchar)sample.y >> 3) & 1) ),
		(short)( (uchar)sample.y >>    4 ) | ( 0xfff0 * (((uchar)sample.y >> 7) & 1) ),
	};
	return convert_float4(a);
}


/**************************************
 * REFERENCE
 **************************************/
#ifdef REF_IMPL


inline void ppf_fir_kernel(const ushort nr_stations, const ushort nr_channels, const ushort nr_taps,
	const ushort station, const ushort channel, const ushort delay_index,
	__global float4* delay, __global float2* out0, __global float2* out1, float4 sum, __constant float* weights)
{
	const ushort delay_step = nr_channels;
	__global float4* delay0 = delay +
		delay_nth(nr_stations, nr_channels, nr_taps, station, channel, delay_index);
	
	// Store the sample in the delay line.
	*delay0 = sum;
	
	// Multiply the first sample with the weight.
	sum     *= *weights;
	weights += nr_channels;
	delay0  += delay_step;
	
	// Multiply-add the other samples in the delay line.
	for(uint i = delay_index + 1; i < nr_taps; ++i)
	{
		sum     += *delay0 * *weights;
		weights += nr_channels;
		delay0  += delay_step;
	}
	
	delay0 = delay + delay_nth(nr_stations, nr_channels, nr_taps, station, channel, 0);
	for(uint i = 0; i < delay_index; ++i)
	{
		sum     += *delay0 * *weights;
		weights += nr_channels;
		delay0  += delay_step;
	}
	
	// Store the result in the output array of the correct polarization.
	*out0 = sum.xy;
	*out1 = sum.zw;
}

// FOR REFERENCE
#if 0
__kernel void fir_kernel_i16(ushort nr_channels, ushort nr_taps, ushort delay_index,
	__global short4* in, __global float4* delay, __global float2* out,
	__constant float* weights, uint sample)
{
	ushort nr_stations  = get_num_groups(0);
	ushort station      = get_group_id(0);
	ushort channel      = get_group_id(1) * get_local_size(0) + get_local_id(0); // 0..127, 128..255
	float4 sum;
	__global float2* out0 = out + output_nth(nr_stations, nr_channels, station, channel, 0, 0);
	__global float2* out1 = out + output_nth(nr_stations, nr_channels, station, channel, 1, 0);
	
	weights += channel;
	in      += input_nth(nr_stations, nr_channels, station, channel, sample);
	
	// Read the sample and convert to float.
	sum = convert_float4(*in);

	ppf_fir_kernel(nr_stations, nr_channels, nr_taps, station, channel,
		delay_index, delay, out0, out1, sum, weights);
}

#else
#define FIR_KERNEL(name, sample_type, convert)\
__kernel void name(ushort nr_channels, ushort nr_taps, ushort delay_index,\
	__global sample_type* in, __global float4* delay, __global float2* out,\
	__constant float* weights, uint sample)\
{\
	ushort nr_stations  = get_num_groups(0);\
	ushort station      = get_group_id(0);\
	ushort channel      = get_group_id(1) * get_local_size(0) + get_local_id(0);\
	float4 sum;\
	__global float2* out0 = out + output_nth(nr_stations, nr_channels, station, channel, 0, 0);\
	__global float2* out1 = out + output_nth(nr_stations, nr_channels, station, channel, 1, 0);\
	weights += channel;\
	in      += input_nth(nr_stations, nr_channels, station, channel, sample);\
	sum = convert(*in);\
	ppf_fir_kernel(nr_stations, nr_channels, nr_taps, station, channel,\
		delay_index, delay, out0, out1, sum, weights);\
}

FIR_KERNEL(fir_kernel_i16, short4, convert_float4)
FIR_KERNEL(fir_kernel_i8, char4, convert_float4)
FIR_KERNEL(fir_kernel_i4, char2, conv_i4_float4)
#endif





/*****************************
 * OPTIMIZED
 *****************************/
#else


#define FIR_TAP0()\
	sum *= *weights;\
	weights += nr_channels;

#define FIR_TAPN(d)\
	sum     += d * *weights;\
	weights += nr_channels;

/**
 *	Computes FIR for 4 taps.
 */
inline float4 do_fir_4(ushort nr_channels, __constant float* weights,
	float4 d00, float4 d01, float4 d02, float4 d03)
{
	float4 sum = d00;
	
	FIR_TAP0();
	FIR_TAPN(d01);
	FIR_TAPN(d02);
	FIR_TAPN(d03);

	return sum;
}

/**
 *	Computes FIR for 8 taps.
 */
inline float4 do_fir_8(ushort nr_channels, __constant float* weights,
	float4 d00, float4 d01, float4 d02, float4 d03, float4 d04, float4 d05, float4 d06, float4 d07)
{
	float4 sum = d00;
	
	FIR_TAP0();
	FIR_TAPN(d01);
	FIR_TAPN(d02);
	FIR_TAPN(d03);
	FIR_TAPN(d04);
	FIR_TAPN(d05);
	FIR_TAPN(d06);
	FIR_TAPN(d07);

	return sum;
}


/**
 *	Computes FIR for 16 taps.
 */
inline float4 do_fir_16(ushort nr_channels, __constant float* weights,
	float4 d00, float4 d01, float4 d02, float4 d03, float4 d04, float4 d05, float4 d06, float4 d07,
	float4 d08, float4 d09, float4 d10, float4 d11, float4 d12, float4 d13, float4 d14, float4 d15)
{
	float4 sum = d00;

	FIR_TAP0();
	FIR_TAPN(d01);
	FIR_TAPN(d02);
	FIR_TAPN(d03);
	FIR_TAPN(d04);
	FIR_TAPN(d05);
	FIR_TAPN(d06);
	FIR_TAPN(d07);
	FIR_TAPN(d08);
	FIR_TAPN(d09);
	FIR_TAPN(d10);
	FIR_TAPN(d11);
	FIR_TAPN(d12);
	FIR_TAPN(d13);
	FIR_TAPN(d14);
	FIR_TAPN(d15);

	return sum;
}

/**
 *	Computes FIR for 32 taps.
 */

inline float4 do_fir_32(ushort nr_channels, const __constant float* weights,
	float4 d00, float4 d01, float4 d02, float4 d03, float4 d04, float4 d05, float4 d06, float4 d07,
	float4 d08, float4 d09, float4 d10, float4 d11, float4 d12, float4 d13, float4 d14, float4 d15,
	float4 d16, float4 d17, float4 d18, float4 d19, float4 d20, float4 d21, float4 d22, float4 d23,
	float4 d24, float4 d25, float4 d26, float4 d27, float4 d28, float4 d29, float4 d30, float4 d31)
{
	float4 sum = d00;

	FIR_TAP0();
	FIR_TAPN(d01);
	FIR_TAPN(d02);
	FIR_TAPN(d03);
	FIR_TAPN(d04);
	FIR_TAPN(d05);
	FIR_TAPN(d06);
	FIR_TAPN(d07);
	FIR_TAPN(d08);
	FIR_TAPN(d09);
	FIR_TAPN(d10);
	FIR_TAPN(d11);
	FIR_TAPN(d12);
	FIR_TAPN(d13);
	FIR_TAPN(d14);
	FIR_TAPN(d15);
	FIR_TAPN(d16);
	FIR_TAPN(d17);
	FIR_TAPN(d18);
	FIR_TAPN(d19);
	FIR_TAPN(d20);
	FIR_TAPN(d21);
	FIR_TAPN(d22);
	FIR_TAPN(d23);
	FIR_TAPN(d24);
	FIR_TAPN(d25);
	FIR_TAPN(d26);
	FIR_TAPN(d27);
	FIR_TAPN(d28);
	FIR_TAPN(d29);
	FIR_TAPN(d30);
	FIR_TAPN(d31);

	return sum;
}

/**
 *	Computes FIR for 64 taps.
 */

inline float4 do_fir_64(ushort nr_channels, __constant float* weights,
	float4 d00, float4 d01, float4 d02, float4 d03, float4 d04, float4 d05, float4 d06, float4 d07,
	float4 d08, float4 d09, float4 d10, float4 d11, float4 d12, float4 d13, float4 d14, float4 d15,
	float4 d16, float4 d17, float4 d18, float4 d19, float4 d20, float4 d21, float4 d22, float4 d23,
	float4 d24, float4 d25, float4 d26, float4 d27, float4 d28, float4 d29, float4 d30, float4 d31,
	float4 d32, float4 d33, float4 d34, float4 d35, float4 d36, float4 d37, float4 d38, float4 d39,
	float4 d40, float4 d41, float4 d42, float4 d43, float4 d44, float4 d45, float4 d46, float4 d47,
	float4 d48, float4 d49, float4 d50, float4 d51, float4 d52, float4 d53, float4 d54, float4 d55,
	float4 d56, float4 d57, float4 d58, float4 d59, float4 d60, float4 d61, float4 d62, float4 d63)
{
	float4 sum = d00;

	FIR_TAP0();
	FIR_TAPN(d01);
	FIR_TAPN(d02);
	FIR_TAPN(d03);
	FIR_TAPN(d04);
	FIR_TAPN(d05);
	FIR_TAPN(d06);
	FIR_TAPN(d07);
	FIR_TAPN(d08);
	FIR_TAPN(d09);
	FIR_TAPN(d10);
	FIR_TAPN(d11);
	FIR_TAPN(d12);
	FIR_TAPN(d13);
	FIR_TAPN(d14);
	FIR_TAPN(d15);
	FIR_TAPN(d16);
	FIR_TAPN(d17);
	FIR_TAPN(d18);
	FIR_TAPN(d19);
	FIR_TAPN(d20);
	FIR_TAPN(d21);
	FIR_TAPN(d22);
	FIR_TAPN(d23);
	FIR_TAPN(d24);
	FIR_TAPN(d25);
	FIR_TAPN(d26);
	FIR_TAPN(d27);
	FIR_TAPN(d28);
	FIR_TAPN(d29);
	FIR_TAPN(d30);
	FIR_TAPN(d31);
	FIR_TAPN(d32);
	FIR_TAPN(d33);
	FIR_TAPN(d34);
	FIR_TAPN(d35);
	FIR_TAPN(d36);
	FIR_TAPN(d37);
	FIR_TAPN(d38);
	FIR_TAPN(d39);
	FIR_TAPN(d40);
	FIR_TAPN(d41);
	FIR_TAPN(d42);
	FIR_TAPN(d43);
	FIR_TAPN(d44);
	FIR_TAPN(d45);
	FIR_TAPN(d46);
	FIR_TAPN(d47);
	FIR_TAPN(d48);
	FIR_TAPN(d49);
	FIR_TAPN(d50);
	FIR_TAPN(d51);
	FIR_TAPN(d52);
	FIR_TAPN(d53);
	FIR_TAPN(d54);
	FIR_TAPN(d55);
	FIR_TAPN(d56);
	FIR_TAPN(d57);
	FIR_TAPN(d58);
	FIR_TAPN(d59);
	FIR_TAPN(d60);
	FIR_TAPN(d61);
	FIR_TAPN(d62);
	FIR_TAPN(d63);

	return sum;
}

#define FIR_REGISTERS_4  d00, d01, d02, d03
#define FIR_REGISTERS_8  d00, d01, d02, d03, d04, d05, d06, d07
#define FIR_REGISTERS_16 d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15
#define FIR_REGISTERS_32 d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15,\
						 d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31
#define FIR_REGISTERS_64 d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15,\
						 d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31,\
						 d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47,\
						 d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63


#define FIR_ROUND_4(convert, d00, d01, d02, d03)\
	d00   = convert(*in);\
	sum   = do_fir_4(nr_channels, weights, d00, d01, d02, d03);\
	*out0 = sum.xy;\
	*out1 = sum.zw;\
	in   += buffer_step_in;\
	out0 += buffer_step;\
	out1 += buffer_step;

#define FIR_ROUND_8(convert, d00, d01, d02, d03, d04, d05, d06, d07)\
	d00   = convert(*in);\
	sum   = do_fir_8(nr_channels, weights, d00, d01, d02, d03, d04, d05, d06, d07);\
	*out0 = sum.xy;\
	*out1 = sum.zw;\
	in   += buffer_step_in;\
	out0 += buffer_step;\
	out1 += buffer_step;

#define FIR_ROUND_16(convert, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15) \
	d00   = convert(*in);\
	sum   = do_fir_16(nr_channels, weights, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15);\
	*out0 = sum.xy;\
	*out1 = sum.zw;\
	in   += buffer_step_in;\
	out0 += buffer_step;\
	out1 += buffer_step;

#define FIR_ROUND_32(convert,\
		d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15,\
		d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31)\
	d00   = convert(*in);\
	sum   = do_fir_32(nr_channels, weights,\
		d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15,\
		d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31);\
	*out0 = sum.xy;\
	*out1 = sum.zw;\
	in   += buffer_step_in;\
	out0 += buffer_step;\
	out1 += buffer_step;

#define FIR_ROUND_64(convert,\
		d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15,\
		d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31,\
		d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47,\
		d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63)\
	d00   = convert(*in);\
	sum   = do_fir_64(nr_channels, weights,\
		d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15,\
		d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31,\
		d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47,\
		d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63);\
	*out0 = sum.xy;\
	*out1 = sum.zw;\
	in   += buffer_step_in;\
	out0 += buffer_step;\
	out1 += buffer_step;

#define FIR_BATCH_4_4(convert)\
	FIR_ROUND_4(convert, d00, d01, d02, d03);\
	FIR_ROUND_4(convert, d03, d00, d01, d02);\
	FIR_ROUND_4(convert, d02, d03, d00, d01);\
	FIR_ROUND_4(convert, d01, d02, d03, d00);

#define FIR_BATCH_8_8(convert)\
	FIR_ROUND_8(convert, d00, d01, d02, d03, d04, d05, d06, d07);\
	FIR_ROUND_8(convert, d07, d00, d01, d02, d03, d04, d05, d06);\
	FIR_ROUND_8(convert, d06, d07, d00, d01, d02, d03, d04, d05);\
	FIR_ROUND_8(convert, d05, d06, d07, d00, d01, d02, d03, d04);\
	FIR_ROUND_8(convert, d04, d05, d06, d07, d00, d01, d02, d03);\
	FIR_ROUND_8(convert, d03, d04, d05, d06, d07, d00, d01, d02);\
	FIR_ROUND_8(convert, d02, d03, d04, d05, d06, d07, d00, d01);\
	FIR_ROUND_8(convert, d01, d02, d03, d04, d05, d06, d07, d00);

#define FIR_BATCH_16_16(convert)\
	FIR_ROUND_16(convert, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15);\
	FIR_ROUND_16(convert, d15, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14);\
	FIR_ROUND_16(convert, d14, d15, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13);\
	FIR_ROUND_16(convert, d13, d14, d15, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12);\
	FIR_ROUND_16(convert, d12, d13, d14, d15, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11);\
	FIR_ROUND_16(convert, d11, d12, d13, d14, d15, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10);\
	FIR_ROUND_16(convert, d10, d11, d12, d13, d14, d15, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09);\
	FIR_ROUND_16(convert, d09, d10, d11, d12, d13, d14, d15, d00, d01, d02, d03, d04, d05, d06, d07, d08);\
	FIR_ROUND_16(convert, d08, d09, d10, d11, d12, d13, d14, d15, d00, d01, d02, d03, d04, d05, d06, d07);\
	FIR_ROUND_16(convert, d07, d08, d09, d10, d11, d12, d13, d14, d15, d00, d01, d02, d03, d04, d05, d06);\
	FIR_ROUND_16(convert, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d00, d01, d02, d03, d04, d05);\
	FIR_ROUND_16(convert, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d00, d01, d02, d03, d04);\
	FIR_ROUND_16(convert, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d00, d01, d02, d03);\
	FIR_ROUND_16(convert, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d00, d01, d02);\
	FIR_ROUND_16(convert, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d00, d01);\
	FIR_ROUND_16(convert, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d00);

#define FIR_BATCH_32_32(convert)\
	FIR_ROUND_32(convert, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31);\
	FIR_ROUND_32(convert, d31, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30);\
	FIR_ROUND_32(convert, d30, d31, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29);\
	FIR_ROUND_32(convert, d29, d30, d31, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28);\
	FIR_ROUND_32(convert, d28, d29, d30, d31, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27);\
	FIR_ROUND_32(convert, d27, d28, d29, d30, d31, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26);\
	FIR_ROUND_32(convert, d26, d27, d28, d29, d30, d31, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25);\
	FIR_ROUND_32(convert, d25, d26, d27, d28, d29, d30, d31, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24);\
	FIR_ROUND_32(convert, d24, d25, d26, d27, d28, d29, d30, d31, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23);\
	FIR_ROUND_32(convert, d23, d24, d25, d26, d27, d28, d29, d30, d31, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22);\
	FIR_ROUND_32(convert, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21);\
	FIR_ROUND_32(convert, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20);\
	FIR_ROUND_32(convert, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19);\
	FIR_ROUND_32(convert, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18);\
	FIR_ROUND_32(convert, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17);\
	FIR_ROUND_32(convert, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16);\
	FIR_ROUND_32(convert, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15);\
	FIR_ROUND_32(convert, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14);\
	FIR_ROUND_32(convert, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13);\
	FIR_ROUND_32(convert, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12);\
	FIR_ROUND_32(convert, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11);\
	FIR_ROUND_32(convert, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10);\
	FIR_ROUND_32(convert, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09);\
	FIR_ROUND_32(convert, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d00, d01, d02, d03, d04, d05, d06, d07, d08);\
	FIR_ROUND_32(convert, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d00, d01, d02, d03, d04, d05, d06, d07);\
	FIR_ROUND_32(convert, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d00, d01, d02, d03, d04, d05, d06);\
	FIR_ROUND_32(convert, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d00, d01, d02, d03, d04, d05);\
	FIR_ROUND_32(convert, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d00, d01, d02, d03, d04);\
	FIR_ROUND_32(convert, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d00, d01, d02, d03);\
	FIR_ROUND_32(convert, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d00, d01, d02);\
	FIR_ROUND_32(convert, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d00, d01);\
	FIR_ROUND_32(convert, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d00);\

#define FIR_BATCH_64_64(convert)\
	FIR_ROUND_64(convert, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63);\
	FIR_ROUND_64(convert, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62);\
	FIR_ROUND_64(convert, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61);\
	FIR_ROUND_64(convert, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60);\
	FIR_ROUND_64(convert, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59);\
	FIR_ROUND_64(convert, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58);\
	FIR_ROUND_64(convert, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57);\
	FIR_ROUND_64(convert, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56);\
	FIR_ROUND_64(convert, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55);\
	FIR_ROUND_64(convert, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54);\
	FIR_ROUND_64(convert, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53);\
	FIR_ROUND_64(convert, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52);\
	FIR_ROUND_64(convert, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51);\
	FIR_ROUND_64(convert, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50);\
	FIR_ROUND_64(convert, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49);\
	FIR_ROUND_64(convert, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48);\
	FIR_ROUND_64(convert, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47);\
	FIR_ROUND_64(convert, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46);\
	FIR_ROUND_64(convert, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45);\
	FIR_ROUND_64(convert, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44);\
	FIR_ROUND_64(convert, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43);\
	FIR_ROUND_64(convert, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42);\
	FIR_ROUND_64(convert, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41);\
	FIR_ROUND_64(convert, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40);\
	FIR_ROUND_64(convert, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39);\
	FIR_ROUND_64(convert, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38);\
	FIR_ROUND_64(convert, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37);\
	FIR_ROUND_64(convert, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36);\
	FIR_ROUND_64(convert, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35);\
	FIR_ROUND_64(convert, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34);\
	FIR_ROUND_64(convert, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33);\
	FIR_ROUND_64(convert, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32);\
	FIR_ROUND_64(convert, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31);\
	FIR_ROUND_64(convert, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30);\
	FIR_ROUND_64(convert, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29);\
	FIR_ROUND_64(convert, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28);\
	FIR_ROUND_64(convert, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27);\
	FIR_ROUND_64(convert, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26);\
	FIR_ROUND_64(convert, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25);\
	FIR_ROUND_64(convert, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24);\
	FIR_ROUND_64(convert, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23);\
	FIR_ROUND_64(convert, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22);\
	FIR_ROUND_64(convert, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21);\
	FIR_ROUND_64(convert, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20);\
	FIR_ROUND_64(convert, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19);\
	FIR_ROUND_64(convert, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18);\
	FIR_ROUND_64(convert, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17);\
	FIR_ROUND_64(convert, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16);\
	FIR_ROUND_64(convert, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15);\
	FIR_ROUND_64(convert, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14);\
	FIR_ROUND_64(convert, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13);\
	FIR_ROUND_64(convert, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12);\
	FIR_ROUND_64(convert, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11);\
	FIR_ROUND_64(convert, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10);\
	FIR_ROUND_64(convert, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09);\
	FIR_ROUND_64(convert, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07, d08);\
	FIR_ROUND_64(convert, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06, d07);\
	FIR_ROUND_64(convert, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05, d06);\
	FIR_ROUND_64(convert, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04, d05);\
	FIR_ROUND_64(convert, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03, d04);\
	FIR_ROUND_64(convert, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02, d03);\
	FIR_ROUND_64(convert, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01, d02);\
	FIR_ROUND_64(convert, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00, d01);\
	FIR_ROUND_64(convert, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d16, d17, d18, d19, d20, d21, d22, d23, d24, d25, d26, d27, d28, d29, d30, d31, d32, d33, d34, d35, d36, d37, d38, d39, d40, d41, d42, d43, d44, d45, d46, d47, d48, d49, d50, d51, d52, d53, d54, d55, d56, d57, d58, d59, d60, d61, d62, d63, d00);





#define FIR_DELAY_READ_4\
	delay += delay_step;\
	d01 = *delay; delay += delay_step;\
	d02 = *delay; delay += delay_step;\
	d03 = *delay;

#define FIR_DELAY_WRITE_4_4\
	*delay = d03; delay -= delay_step;\
	*delay = d02; delay -= delay_step;\
	*delay = d01;


#define FIR_DELAY_READ_8\
	delay += delay_step;\
	d01 = *delay; delay += delay_step;\
	d02 = *delay; delay += delay_step;\
	d03 = *delay; delay += delay_step;\
	d04 = *delay; delay += delay_step;\
	d05 = *delay; delay += delay_step;\
	d06 = *delay; delay += delay_step;\
	d07 = *delay;

#define FIR_DELAY_WRITE_8_8\
	*delay = d07; delay -= delay_step;\
	*delay = d06; delay -= delay_step;\
	*delay = d05; delay -= delay_step;\
	*delay = d04; delay -= delay_step;\
	*delay = d03; delay -= delay_step;\
	*delay = d02; delay -= delay_step;\
	*delay = d01;



#define FIR_DELAY_READ_16\
	delay += delay_step;\
	d01 = *delay; delay += delay_step;\
	d02 = *delay; delay += delay_step;\
	d03 = *delay; delay += delay_step;\
	d04 = *delay; delay += delay_step;\
	d05 = *delay; delay += delay_step;\
	d06 = *delay; delay += delay_step;\
	d07 = *delay; delay += delay_step;\
	d08 = *delay; delay += delay_step;\
	d09 = *delay; delay += delay_step;\
	d10 = *delay; delay += delay_step;\
	d11 = *delay; delay += delay_step;\
	d12 = *delay; delay += delay_step;\
	d13 = *delay; delay += delay_step;\
	d14 = *delay; delay += delay_step;\
	d15 = *delay;


#define FIR_DELAY_WRITE_16_16\
	*delay = d15; delay -= delay_step;\
	*delay = d14; delay -= delay_step;\
	*delay = d13; delay -= delay_step;\
	*delay = d12; delay -= delay_step;\
	*delay = d11; delay -= delay_step;\
	*delay = d10; delay -= delay_step;\
	*delay = d09; delay -= delay_step;\
	*delay = d08; delay -= delay_step;\
	*delay = d07; delay -= delay_step;\
	*delay = d06; delay -= delay_step;\
	*delay = d05; delay -= delay_step;\
	*delay = d04; delay -= delay_step;\
	*delay = d03; delay -= delay_step;\
	*delay = d02; delay -= delay_step;\
	*delay = d01;

#define FIR_DELAY_READ_32\
	delay += delay_step;\
	d01 = *delay; delay += delay_step;\
	d02 = *delay; delay += delay_step;\
	d03 = *delay; delay += delay_step;\
	d04 = *delay; delay += delay_step;\
	d05 = *delay; delay += delay_step;\
	d06 = *delay; delay += delay_step;\
	d07 = *delay; delay += delay_step;\
	d08 = *delay; delay += delay_step;\
	d09 = *delay; delay += delay_step;\
	d10 = *delay; delay += delay_step;\
	d11 = *delay; delay += delay_step;\
	d12 = *delay; delay += delay_step;\
	d13 = *delay; delay += delay_step;\
	d14 = *delay; delay += delay_step;\
	d15 = *delay; delay += delay_step;\
	d16 = *delay; delay += delay_step;\
	d17 = *delay; delay += delay_step;\
	d18 = *delay; delay += delay_step;\
	d19 = *delay; delay += delay_step;\
	d20 = *delay; delay += delay_step;\
	d21 = *delay; delay += delay_step;\
	d22 = *delay; delay += delay_step;\
	d23 = *delay; delay += delay_step;\
	d24 = *delay; delay += delay_step;\
	d25 = *delay; delay += delay_step;\
	d26 = *delay; delay += delay_step;\
	d27 = *delay; delay += delay_step;\
	d28 = *delay; delay += delay_step;\
	d29 = *delay; delay += delay_step;\
	d30 = *delay; delay += delay_step;\
	d31 = *delay;

#define FIR_DELAY_WRITE_32_32\
	*delay = d31; delay -= delay_step;\
	*delay = d30; delay -= delay_step;\
	*delay = d29; delay -= delay_step;\
	*delay = d28; delay -= delay_step;\
	*delay = d27; delay -= delay_step;\
	*delay = d26; delay -= delay_step;\
	*delay = d25; delay -= delay_step;\
	*delay = d24; delay -= delay_step;\
	*delay = d23; delay -= delay_step;\
	*delay = d22; delay -= delay_step;\
	*delay = d21; delay -= delay_step;\
	*delay = d20; delay -= delay_step;\
	*delay = d19; delay -= delay_step;\
	*delay = d18; delay -= delay_step;\
	*delay = d17; delay -= delay_step;\
	*delay = d16; delay -= delay_step;\
	*delay = d15; delay -= delay_step;\
	*delay = d14; delay -= delay_step;\
	*delay = d13; delay -= delay_step;\
	*delay = d12; delay -= delay_step;\
	*delay = d11; delay -= delay_step;\
	*delay = d10; delay -= delay_step;\
	*delay = d09; delay -= delay_step;\
	*delay = d08; delay -= delay_step;\
	*delay = d07; delay -= delay_step;\
	*delay = d06; delay -= delay_step;\
	*delay = d05; delay -= delay_step;\
	*delay = d04; delay -= delay_step;\
	*delay = d03; delay -= delay_step;\
	*delay = d02; delay -= delay_step;\
	*delay = d01;

#define FIR_DELAY_READ_64\
	delay += delay_step;\
	d01 = *delay; delay += delay_step;\
	d02 = *delay; delay += delay_step;\
	d03 = *delay; delay += delay_step;\
	d04 = *delay; delay += delay_step;\
	d05 = *delay; delay += delay_step;\
	d06 = *delay; delay += delay_step;\
	d07 = *delay; delay += delay_step;\
	d08 = *delay; delay += delay_step;\
	d09 = *delay; delay += delay_step;\
	d10 = *delay; delay += delay_step;\
	d11 = *delay; delay += delay_step;\
	d12 = *delay; delay += delay_step;\
	d13 = *delay; delay += delay_step;\
	d14 = *delay; delay += delay_step;\
	d15 = *delay; delay += delay_step;\
	d16 = *delay; delay += delay_step;\
	d17 = *delay; delay += delay_step;\
	d18 = *delay; delay += delay_step;\
	d19 = *delay; delay += delay_step;\
	d20 = *delay; delay += delay_step;\
	d21 = *delay; delay += delay_step;\
	d22 = *delay; delay += delay_step;\
	d23 = *delay; delay += delay_step;\
	d24 = *delay; delay += delay_step;\
	d25 = *delay; delay += delay_step;\
	d26 = *delay; delay += delay_step;\
	d27 = *delay; delay += delay_step;\
	d28 = *delay; delay += delay_step;\
	d29 = *delay; delay += delay_step;\
	d30 = *delay; delay += delay_step;\
	d31 = *delay; delay += delay_step;\
	d32 = *delay; delay += delay_step;\
	d33 = *delay; delay += delay_step;\
	d34 = *delay; delay += delay_step;\
	d35 = *delay; delay += delay_step;\
	d36 = *delay; delay += delay_step;\
	d37 = *delay; delay += delay_step;\
	d38 = *delay; delay += delay_step;\
	d39 = *delay; delay += delay_step;\
	d40 = *delay; delay += delay_step;\
	d41 = *delay; delay += delay_step;\
	d42 = *delay; delay += delay_step;\
	d43 = *delay; delay += delay_step;\
	d44 = *delay; delay += delay_step;\
	d45 = *delay; delay += delay_step;\
	d46 = *delay; delay += delay_step;\
	d47 = *delay; delay += delay_step;\
	d48 = *delay; delay += delay_step;\
	d49 = *delay; delay += delay_step;\
	d50 = *delay; delay += delay_step;\
	d51 = *delay; delay += delay_step;\
	d52 = *delay; delay += delay_step;\
	d53 = *delay; delay += delay_step;\
	d54 = *delay; delay += delay_step;\
	d55 = *delay; delay += delay_step;\
	d56 = *delay; delay += delay_step;\
	d57 = *delay; delay += delay_step;\
	d58 = *delay; delay += delay_step;\
	d59 = *delay; delay += delay_step;\
	d60 = *delay; delay += delay_step;\
	d61 = *delay; delay += delay_step;\
	d62 = *delay; delay += delay_step;\
	d63 = *delay;

#define FIR_DELAY_WRITE_64_64\
	*delay = d63; delay -= delay_step;\
	*delay = d62; delay -= delay_step;\
	*delay = d61; delay -= delay_step;\
	*delay = d60; delay -= delay_step;\
	*delay = d59; delay -= delay_step;\
	*delay = d58; delay -= delay_step;\
	*delay = d57; delay -= delay_step;\
	*delay = d56; delay -= delay_step;\
	*delay = d55; delay -= delay_step;\
	*delay = d54; delay -= delay_step;\
	*delay = d53; delay -= delay_step;\
	*delay = d52; delay -= delay_step;\
	*delay = d51; delay -= delay_step;\
	*delay = d50; delay -= delay_step;\
	*delay = d49; delay -= delay_step;\
	*delay = d48; delay -= delay_step;\
	*delay = d47; delay -= delay_step;\
	*delay = d46; delay -= delay_step;\
	*delay = d45; delay -= delay_step;\
	*delay = d44; delay -= delay_step;\
	*delay = d43; delay -= delay_step;\
	*delay = d42; delay -= delay_step;\
	*delay = d41; delay -= delay_step;\
	*delay = d40; delay -= delay_step;\
	*delay = d39; delay -= delay_step;\
	*delay = d38; delay -= delay_step;\
	*delay = d37; delay -= delay_step;\
	*delay = d36; delay -= delay_step;\
	*delay = d35; delay -= delay_step;\
	*delay = d34; delay -= delay_step;\
	*delay = d33; delay -= delay_step;\
	*delay = d32; delay -= delay_step;\
	*delay = d31; delay -= delay_step;\
	*delay = d30; delay -= delay_step;\
	*delay = d29; delay -= delay_step;\
	*delay = d28; delay -= delay_step;\
	*delay = d27; delay -= delay_step;\
	*delay = d26; delay -= delay_step;\
	*delay = d25; delay -= delay_step;\
	*delay = d24; delay -= delay_step;\
	*delay = d23; delay -= delay_step;\
	*delay = d22; delay -= delay_step;\
	*delay = d21; delay -= delay_step;\
	*delay = d20; delay -= delay_step;\
	*delay = d19; delay -= delay_step;\
	*delay = d18; delay -= delay_step;\
	*delay = d17; delay -= delay_step;\
	*delay = d16; delay -= delay_step;\
	*delay = d15; delay -= delay_step;\
	*delay = d14; delay -= delay_step;\
	*delay = d13; delay -= delay_step;\
	*delay = d12; delay -= delay_step;\
	*delay = d11; delay -= delay_step;\
	*delay = d10; delay -= delay_step;\
	*delay = d09; delay -= delay_step;\
	*delay = d08; delay -= delay_step;\
	*delay = d07; delay -= delay_step;\
	*delay = d06; delay -= delay_step;\
	*delay = d05; delay -= delay_step;\
	*delay = d04; delay -= delay_step;\
	*delay = d03; delay -= delay_step;\
	*delay = d02; delay -= delay_step;\
	*delay = d01;




// for reference
#if 0
__kernel void ppf_fir_kernel_i16_16(ushort nr_channels, uint delay_step, uint buffer_step,
	__global short4* in, __global float2* out, __global float4* delay, __constant float* weights, ushort batches)
{
	// maximum amount of registers = 42
	// current usage: 36 + 3       = 39 registers
	// 2 registers
	ushort nr_stations  = get_num_groups(0);
	ushort station      = get_group_id(0);
	ushort channel      = get_group_id(1) * get_local_size(0) + get_local_id(0); // 0..127, 128..255
	uint buffer_step_in = buffer_step >> 1;
	__global float2* out0 = out + output_nth(nr_stations, nr_channels, station, channel, 0, 0);
	__global float2* out1 = out + output_nth(nr_stations, nr_channels, station, channel, 1, 0);
	// this is the whole delay line, 32 registers
	float4 d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15;
	float4 sum;
	
	delay_step >>= 1;
	weights += channel;
	in       = in    + input_nth(nr_stations, nr_channels, station, channel, 0);
	delay    = delay + delay_nth(nr_stations, nr_channels, 16, station, channel, 0);

	// get all the old values from the memory

	delay += delay_step;
	//d00 will contain the first sample
	d01 = *delay; delay += delay_step;
	d02 = *delay; delay += delay_step;
	d03 = *delay; delay += delay_step;
	d04 = *delay; delay += delay_step;
	d05 = *delay; delay += delay_step;
	d06 = *delay; delay += delay_step;
	d07 = *delay; delay += delay_step;
	d08 = *delay; delay += delay_step;
	d09 = *delay; delay += delay_step;
	d10 = *delay; delay += delay_step;
	d11 = *delay; delay += delay_step;
	d12 = *delay; delay += delay_step;
	d13 = *delay; delay += delay_step;
	d14 = *delay; delay += delay_step;
	d15 = *delay; //delay += delay_step;

	for(int i = 0; i < batches; ++i)
	{
		// newest ... oldest
		FIR_ROUND_16(convert_float4, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15);
		FIR_ROUND_16(convert_float4, d15, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14);
		FIR_ROUND_16(convert_float4, d14, d15, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13);
		FIR_ROUND_16(convert_float4, d13, d14, d15, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12);
		FIR_ROUND_16(convert_float4, d12, d13, d14, d15, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11);
		FIR_ROUND_16(convert_float4, d11, d12, d13, d14, d15, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10);
		FIR_ROUND_16(convert_float4, d10, d11, d12, d13, d14, d15, d00, d01, d02, d03, d04, d05, d06, d07, d08, d09);
		FIR_ROUND_16(convert_float4, d09, d10, d11, d12, d13, d14, d15, d00, d01, d02, d03, d04, d05, d06, d07, d08);
		FIR_ROUND_16(convert_float4, d08, d09, d10, d11, d12, d13, d14, d15, d00, d01, d02, d03, d04, d05, d06, d07);
		FIR_ROUND_16(convert_float4, d07, d08, d09, d10, d11, d12, d13, d14, d15, d00, d01, d02, d03, d04, d05, d06);
		FIR_ROUND_16(convert_float4, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d00, d01, d02, d03, d04, d05);
		FIR_ROUND_16(convert_float4, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d00, d01, d02, d03, d04);
		FIR_ROUND_16(convert_float4, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d00, d01, d02, d03);
		FIR_ROUND_16(convert_float4, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d00, d01, d02);
		FIR_ROUND_16(convert_float4, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d00, d01);
		FIR_ROUND_16(convert_float4, d01, d02, d03, d04, d05, d06, d07, d08, d09, d10, d11, d12, d13, d14, d15, d00);
	}

	*delay = d15; delay -= delay_step;
	*delay = d14; delay -= delay_step;
	*delay = d13; delay -= delay_step;
	*delay = d12; delay -= delay_step;
	*delay = d11; delay -= delay_step;
	*delay = d10; delay -= delay_step;
	*delay = d09; delay -= delay_step;
	*delay = d08; delay -= delay_step;
	*delay = d07; delay -= delay_step;
	*delay = d06; delay -= delay_step;
	*delay = d05; delay -= delay_step;
	*delay = d04; delay -= delay_step;
	*delay = d03; delay -= delay_step;
	*delay = d02; delay -= delay_step;
	*delay = d01; //delay -= delay_step;
}

#else


/**
 *	Process samples in batches.
 */
#define FIR_KERNEL(name, nr_taps, sample_type, convert, registers, delay_read, delay_write, batch)\
__kernel void name(ushort nr_channels, uint delay_step, uint buffer_step,\
	__global sample_type* in, __global float2* out, __global float4* delay, __constant float* weights, ushort batches)\
{\
	ushort nr_stations  = get_num_groups(0);\
	ushort station      = get_group_id(0);\
	ushort channel      = get_group_id(1) * get_local_size(0) + get_local_id(0);\
	__global float2* out0 = out + output_nth(nr_stations, nr_channels, station, channel, 0, 0);\
	__global float2* out1 = out + output_nth(nr_stations, nr_channels, station, channel, 1, 0);\
	uint buffer_step_in = buffer_step >> 1;\
	float4 registers;\
	float4 sum;\
	delay_step >>= 1;\
	weights += channel;\
	in       = in    + input_nth(nr_stations, nr_channels, station, channel, 0);\
	delay    = delay + delay_nth(nr_stations, nr_channels, nr_taps, station, channel, 0);\
	delay_read;\
	for(int i = 0; i < batches; ++i)\
	{\
		batch(convert);\
	}\
	delay_write;\
}


#ifdef PTXINSPECT
FIR_KERNEL(fir_kernel_i16_16, 16, short4, convert_float4, FIR_REGISTERS_16, FIR_DELAY_READ_16, FIR_DELAY_WRITE_16_16, FIR_BATCH_16_16);
#else
#	ifdef NO64TAPS
__kernel void fir_kernel_i16_64() {}
__kernel void fir_kernel_i8_64() {}
__kernel void fir_kernel_i4_64() {}
#	else
FIR_KERNEL(fir_kernel_i16_64, 64, short4, convert_float4, FIR_REGISTERS_64, FIR_DELAY_READ_64, FIR_DELAY_WRITE_64_64, FIR_BATCH_64_64);
FIR_KERNEL(fir_kernel_i8_64 , 64, char4, convert_float4, FIR_REGISTERS_64, FIR_DELAY_READ_64, FIR_DELAY_WRITE_64_64, FIR_BATCH_64_64);
FIR_KERNEL(fir_kernel_i4_64 , 64, char2, conv_i4_float4, FIR_REGISTERS_64, FIR_DELAY_READ_64, FIR_DELAY_WRITE_64_64, FIR_BATCH_64_64);
#	endif
FIR_KERNEL(fir_kernel_i16_32, 32, short4, convert_float4, FIR_REGISTERS_32, FIR_DELAY_READ_32, FIR_DELAY_WRITE_32_32, FIR_BATCH_32_32);
FIR_KERNEL(fir_kernel_i16_16, 16, short4, convert_float4, FIR_REGISTERS_16, FIR_DELAY_READ_16, FIR_DELAY_WRITE_16_16, FIR_BATCH_16_16);
FIR_KERNEL(fir_kernel_i16_8 ,  8, short4, convert_float4, FIR_REGISTERS_8 , FIR_DELAY_READ_8 , FIR_DELAY_WRITE_8_8  , FIR_BATCH_8_8);
FIR_KERNEL(fir_kernel_i16_4 ,  4, short4, convert_float4, FIR_REGISTERS_4 , FIR_DELAY_READ_4 , FIR_DELAY_WRITE_4_4  , FIR_BATCH_4_4);
FIR_KERNEL(fir_kernel_i8_32 , 32, char4, convert_float4, FIR_REGISTERS_32, FIR_DELAY_READ_32, FIR_DELAY_WRITE_32_32, FIR_BATCH_32_32);
FIR_KERNEL(fir_kernel_i8_16 , 16, char4, convert_float4, FIR_REGISTERS_16, FIR_DELAY_READ_16, FIR_DELAY_WRITE_16_16, FIR_BATCH_16_16);
FIR_KERNEL(fir_kernel_i8_8  ,  8, char4, convert_float4, FIR_REGISTERS_8 , FIR_DELAY_READ_8 , FIR_DELAY_WRITE_8_8  , FIR_BATCH_8_8);
FIR_KERNEL(fir_kernel_i8_4  ,  4, char4, convert_float4, FIR_REGISTERS_4 , FIR_DELAY_READ_4 , FIR_DELAY_WRITE_4_4  , FIR_BATCH_4_4);
FIR_KERNEL(fir_kernel_i4_32 , 32, char2, conv_i4_float4, FIR_REGISTERS_32, FIR_DELAY_READ_32, FIR_DELAY_WRITE_32_32, FIR_BATCH_32_32);
FIR_KERNEL(fir_kernel_i4_16 , 16, char2, conv_i4_float4, FIR_REGISTERS_16, FIR_DELAY_READ_16, FIR_DELAY_WRITE_16_16, FIR_BATCH_16_16);
FIR_KERNEL(fir_kernel_i4_8  ,  8, char2, conv_i4_float4, FIR_REGISTERS_8 , FIR_DELAY_READ_8 , FIR_DELAY_WRITE_8_8  , FIR_BATCH_8_8);
FIR_KERNEL(fir_kernel_i4_4  ,  4, char2, conv_i4_float4, FIR_REGISTERS_4 , FIR_DELAY_READ_4 , FIR_DELAY_WRITE_4_4  , FIR_BATCH_4_4);
#endif //PTXINSPECT

#endif //0

#endif //REF_IMPL
