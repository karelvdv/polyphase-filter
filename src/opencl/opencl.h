/*
    A Polyphase Filter for GPUs and Multi-Core Processors
    Copyright (C) 2012 Karel van der Veldt (karel.vd.veldt@uva.nl)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __PPF_OPENCL_H
#define __PPF_OPENCL_H
#include <CL/cl.h>

typedef struct 
{
	cl_platform_id platform;
	cl_context context;
	cl_command_queue queue;
	cl_device_id device;
} opencl_t;

typedef struct
{
	int nkernels;
	cl_program program;
	cl_kernel kernels[];
} opencl_program_t;


void opencl_assert(int test, const char* fmt, ...);

/**
 *	Create an OpenCL context and queue.
 *	@param platform    The platform.
 *	@param device      The device on the platform.
 *	@param device_type A device type.
 *	@return	Pointer to the context.
 */
opencl_t* opencl_new(cl_uint platform, cl_uint device, cl_device_type device_type);

/**
 *	Frees the context.
 */
void opencl_free(opencl_t* opencl);

/**
 *	Compile a program and create the kernels.
 *	@param cl            The opencl context.
 *	@param code          The code.
 *	@param len           Length of the code in bytes.
 *	@param build_options Build options for the compiler.
 *	@param kernels       List of kernel names to create.
 *	@param nkernels      Length of the kernel name list.
 *	@return A compiled OpenCL program.
 */
opencl_program_t* opencl_compile(opencl_t* cl, const void* code, size_t len,
	const char* build_options, const char** kernels, int nkernels, cl_uint binary);

/**
 *	Frees the program and kernels.
 */
void opencl_program_free(opencl_program_t* program);

cl_int opencl_call(opencl_t* cl, cl_kernel kernel,
	cl_uint work_dim, const size_t* global_work_size, const size_t* local_work_size,
	const char* argfmt, ...);

const char* opencl_err_string(cl_int err);

cl_int opencl_memset(opencl_t* cl, cl_mem buf, cl_uint val, size_t num);

#endif
