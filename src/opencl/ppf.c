/*
    A Polyphase Filter for GPUs and Multi-Core Processors
    Copyright (C) 2012 Karel van der Veldt (karel.vd.veldt@uva.nl)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <assert.h>
#include <stdarg.h>
#include <stdlib.h>
#include "ppf.h"

/***********************
 *	NVIDIA
 ***********************/
#if defined(OPENCL_IS_NV)
#	ifdef REF_IMPL
#		include "ppf_k_cuda_ref.h"
#		include "ppf_k_ati_ref.h"
static const char* build_options[] = {
	"-DREF_IMPL",
	"-DREF_IMPL",
	"-DREF_IMPL",
	"-DREF_IMPL",
};
#	else //REF_IMPL
#		include "ppf_k_cuda.h"
#		include "ppf_k_ati.h"
static const char* build_options[] = {
	"",
	"",
	"",
	"",
};
#	endif //REF_IMPL
#	define BINARY_KERNEL 1
static const unsigned char* kernel_code[] = {
	_binary_ppf_k_cuda_start,
	_binary_ppf_k_ati_start,
	0,
	0,
};

static const cl_uint kernel_size[] = {
	sizeof(_binary_ppf_k_cuda_start),
	sizeof(_binary_ppf_k_ati_start),
	0,
	0,
};

static const cl_uint kernel_max_taps[] = {
	64,
	64,
	0,
	0,
};


typedef struct
{
	unsigned int nr_taps;
	unsigned int max_threads_per_block[4];
} tap_info;
static const tap_info allowed_taps[] = {
	// max work group item size
	// gtx480: 1024, cypress: 256, core i7: 1024
	// tap, {cuda, ati, cpu}, occupancy on compute ability 2.0
	{ 4, {512, 256, 0, 0}}, // 100%
	{ 8, {512, 256, 0, 0}}, // 100%
	{16, {256, 128, 0, 0}}, // 50%
	{32, {128,  64, 0, 0}}, // 25%
	{64, { 32,  16, 0, 0}}, // 15%
	{0,  {  0,   0, 0, 0}},
};



/*********************
 *	ATI
 *********************/
#elif defined (OPENCL_IS_ATI)
#	ifdef REF_IMPL
#		include "ppf_k_cuda_ref.h"
#		include "ppf_k_ati_ref.h"
#		include "ppf_k_ati_cpu_ref.h"
#		include "ppf_k_cuda_cpu_ref.h"
static const char* build_options[] = {
	"-DREF_IMPL -DNO64TAPS",
	"-DREF_IMPL -DNO64TAPS",
	"-DREF_IMPL",
	"-DREF_IMPL",
};
#	else
#		include "ppf_k_cuda.h"
#		include "ppf_k_ati.h"
#		include "ppf_k_ati_cpu.h"
#		include "ppf_k_cuda_cpu.h"
static const char* build_options[] = {
	"-DNO64TAPS",
	"-DNO64TAPS",
	"",
	"",
};
#	endif
#	define BINARY_KERNEL 1
static const unsigned char* kernel_code[] = {
	_binary_ppf_k_cuda_start,
	_binary_ppf_k_ati_start,
	_binary_ppf_k_cuda_cpu_start,
	_binary_ppf_k_ati_cpu_start,
};

static const cl_uint kernel_size[] = {
	sizeof(_binary_ppf_k_cuda_start),
	sizeof(_binary_ppf_k_ati_start),
	sizeof(_binary_ppf_k_cuda_cpu_start),
	sizeof(_binary_ppf_k_ati_cpu_start),
};

static const cl_uint kernel_max_taps[] = {
	32,
	32,
	64,
	64,
};

typedef struct
{
	unsigned int nr_taps;
	unsigned int max_threads_per_block[4];
} tap_info;
static const tap_info allowed_taps[] = {
	// max work group item size
	// gtx480: 1024, cypress: 256, core i7: 1024
	// tap, {cuda, ati, cpu, cpu}, occupancy on compute ability 2.0
	{ 4, {256, 256, 1024, 1024}}, // 100%
	{ 8, {256, 256, 1024, 1024}}, // 100%
	{16, {256, 256, 1024, 1024}}, // 50%
	{32, {256, 256, 1024, 1024}}, // 25%
	{64, {  0,   0, 1024, 1024}}, // 15%
	{0,  {  0,   0,   0,   0}},
};

#else
#	error OpenCL is neither NV or ATI!
#endif

static const cl_uint kernel_type[] = {
	CL_DEVICE_TYPE_GPU,
	CL_DEVICE_TYPE_GPU,
	CL_DEVICE_TYPE_CPU,
	CL_DEVICE_TYPE_CPU,
};

static const cl_uint kernel_threads_mult[] = {
	PPF_NR_POLARIZATIONS,
	1,
	PPF_NR_POLARIZATIONS,
	1,
};


/**
 *	REFERENCE IMPLEMENTATION KERNELS
 */
#ifdef REF_IMPL
static const char* kernels[] = {
	"fir_kernel_i16",
	"fir_kernel_i8",
	"fir_kernel_i4",
};

/**
 *	OPTIMIZED IMPLEMENTATION KERNELS
 */
#else
static const char* kernels[] = {
	"fir_kernel_i16_32",
	"fir_kernel_i16_16",
	"fir_kernel_i16_8",
	"fir_kernel_i16_4",
	"fir_kernel_i8_32",
	"fir_kernel_i8_16",
	"fir_kernel_i8_8",
	"fir_kernel_i8_4",
	"fir_kernel_i4_32",
	"fir_kernel_i4_16",
	"fir_kernel_i4_8",
	"fir_kernel_i4_4",
	"fir_kernel_i16_64",
	"fir_kernel_i8_64",
	"fir_kernel_i4_64",
};

#endif


/********************************
 *	FIR
 ********************************/

ppf_fir* ppf_fir_new(cl_uint nr_stations, cl_uint nr_channels, cl_uint nr_taps, opencl_t* cl, int kernel)
{
	cl_int err = CL_SUCCESS;
	const tap_info* tapinfo = allowed_taps;
	
	opencl_assert(nr_taps > kernel_max_taps[kernel], "too many taps %d.\n", nr_taps);
	
	while(tapinfo->nr_taps && tapinfo->nr_taps != nr_taps) tapinfo++;
	assert(tapinfo->nr_taps > 0);
	
	size_t len = sizeof(cl_float2) * nr_stations * nr_channels * nr_taps * PPF_NR_POLARIZATIONS;
	cl_mem delay = clCreateBuffer(cl->context, CL_MEM_READ_WRITE, len, NULL, &err);
	assert(err == CL_SUCCESS);
	
	assert( opencl_memset(cl, delay, 0, len / sizeof(cl_uint)) == CL_SUCCESS );
	
	len = sizeof(cl_float) * nr_channels * nr_taps;
	cl_mem weights = clCreateBuffer(cl->context, CL_MEM_READ_ONLY, len, NULL, &err);
	assert(err == CL_SUCCESS);
	
	ppf_fir* fir = (ppf_fir*)malloc(sizeof(ppf_fir));
	assert(fir);
	
	fir->nr_stations = nr_stations;
	fir->nr_channels = nr_channels;
	fir->nr_taps     = nr_taps;
	fir->delay_index = 0;
	fir->delay       = delay;
	fir->weights     = weights;
	fir->cl          = cl;
	fir->prog        = opencl_compile(cl,
		kernel_code[kernel], kernel_size[kernel],
		build_options[kernel], kernels, sizeof(kernels) / sizeof(char*), BINARY_KERNEL);
	
	// calculate the size of the grid block
	unsigned int threads = tapinfo->max_threads_per_block[kernel];
	unsigned int blocks_per_station = 1;
	
	unsigned int num_threads = nr_channels * kernel_threads_mult[kernel];
	
	while( num_threads / blocks_per_station > threads )
	{
		blocks_per_station++;
	}
	
	fir->local[0]  = num_threads / blocks_per_station;
	fir->local[1]  = 1;
	fir->global[0] = nr_stations * fir->local[0];
	fir->global[1] = blocks_per_station;
#if 0
	fprintf(stderr, "num_threads = %d * %d = %d\n", nr_channels, kernel_threads_mult[kernel], num_threads);
	fprintf(stderr, "local  = %lu x %lu\n", fir->local[0], fir->local[1]);
	fprintf(stderr, "global = %lu x %lu\n", fir->global[0], fir->global[1]);
#endif
	return fir;
}

void ppf_fir_free(ppf_fir* fir)
{
	clReleaseMemObject(fir->weights);
	clReleaseMemObject(fir->delay);
	free(fir);
}

void ppf_fir_set_weights(ppf_fir* fir, const float* weights)
{
	cl_uint nr_channels = fir->nr_channels;
	cl_uint nr_taps     = fir->nr_taps;
	size_t len          = sizeof(cl_float) * nr_channels * nr_taps;
	cl_float* trans = (cl_float*)malloc(len);
	assert(trans);
	
	// transpose the array first
	for(cl_uint i = 0; i < nr_channels; ++i)
	{
		for(cl_uint j = 0; j < nr_taps; ++j)
		{
			trans[j * nr_channels + i] = weights[i * nr_taps + j];
		}
	}
	
	assert( clEnqueueWriteBuffer(fir->cl->queue, fir->weights, CL_TRUE, 0, len, trans,
		0, NULL, NULL) == CL_SUCCESS );
	
	free(trans);
}


/**********************************
 *	INPUT
 **********************************/
#ifdef PAGELOCKED_MEMORY

#if 0
# define DEF_INPUT_NEW(name, type, sample_size)\
	type* name(cl_uint stations, cl_uint channels, cl_uint nr_samples, opencl_t* cl)\
	{\
		cl_int err = CL_SUCCESS;\
		size_t len = sizeof(sample_size) * stations * channels * PPF_NR_POLARIZATIONS;\
		type* in = (type*)malloc(sizeof(type));\
		assert(in);\
		in->len = len;\
		in->cl = cl;\
		in->d  = clCreateBuffer(cl->context, CL_MEM_READ_ONLY, len * nr_samples, NULL, &err);\
		assert(err == CL_SUCCESS);\
		in->hp = clCreateBuffer(cl->context, CL_MEM_READ_ONLY | CL_MEM_ALLOC_HOST_PTR, len * nr_samples, NULL, &err);\
		assert(err == CL_SUCCESS);\
		in->h = (sample_size*)clEnqueueMapBuffer(cl->queue, in->hp, CL_TRUE, CL_MAP_WRITE, 0, len * nr_samples, 0, NULL, NULL, &err);\
		assert(err == CL_SUCCESS);\
		return in;\
	}

#	define DEF_INPUT_FREE(name, type)\
	void name(type* in)\
	{\
		clEnqueueUnmapMemObject (in->cl->queue, in->hp, in->h, 0, NULL, NULL);\
		clReleaseMemObject(in->d);\
		clReleaseMemObject(in->hp);\
		free(in);\
	}
#else
#	define DEF_INPUT_NEW(name, type, sample_size)\
	type* name(cl_uint stations, cl_uint channels, cl_uint nr_samples, opencl_t* cl)\
	{\
		cl_int err = CL_SUCCESS;\
		size_t len = sizeof(sample_size) * stations * channels * PPF_NR_POLARIZATIONS;\
		type* in = (type*)malloc(sizeof(type) + len * nr_samples);\
		assert(in);\
		in->len = len;\
		in->cl = cl;\
		in->d = clCreateBuffer(cl->context, CL_MEM_READ_ONLY | CL_MEM_USE_HOST_PTR, len * nr_samples, in->h, &err);\
		assert(err == CL_SUCCESS);\
		return in;\
	}

#	define DEF_INPUT_FREE(name, type)\
	void name(type* in)\
	{\
		clReleaseMemObject(in->d);\
		free(in);\
	}
#endif


#else //PAGELOCKED_MEMORY
#	define DEF_INPUT_NEW(name, type, sample_size)\
	type* name(cl_uint stations, cl_uint channels, cl_uint nr_samples, opencl_t* cl)\
	{\
		cl_int err = CL_SUCCESS;\
		size_t len = sizeof(sample_size) * stations * channels * PPF_NR_POLARIZATIONS;\
		type* in = (type*)malloc(sizeof(type) + len * nr_samples);\
		assert(in);\
		in->len = len;\
		in->cl = cl;\
		in->d = clCreateBuffer(cl->context, CL_MEM_READ_ONLY, len * nr_samples, NULL, &err);\
		assert(err == CL_SUCCESS);\
		return in;\
	}

#	define DEF_INPUT_FREE(name, type)\
	void name(type* in)\
	{\
		clReleaseMemObject(in->d);\
		free(in);\
	}
#endif

#ifdef PAGELOCKED_MEMORY
#define DEF_INPUT_TO_DEVICE(name, in_type, sample_size)\
cl_int name(in_type* in, cl_uint nr_samples)\
{\
	return CL_SUCCESS;\
}
#else
#define DEF_INPUT_TO_DEVICE(name, in_type, sample_size)\
cl_int name(in_type* in, cl_uint nr_samples)\
{\
	sample_size* h = ppf_input_get_host(in);\
	cl_mem d = ppf_input_get_device(in);\
	return clEnqueueWriteBuffer(in->cl->queue, d, CL_TRUE, 0, in->len * nr_samples, h, 0, NULL, NULL);\
}
#endif

DEF_INPUT_NEW(ppf_input_new_i16, ppf_input_i16, ppf_complex_i16);
DEF_INPUT_NEW(ppf_input_new_i8 , ppf_input_i8 , ppf_complex_i8);
DEF_INPUT_NEW(ppf_input_new_i4 , ppf_input_i4 , ppf_complex_i4);

DEF_INPUT_FREE(ppf_input_free_i16, ppf_input_i16);
DEF_INPUT_FREE(ppf_input_free_i8 , ppf_input_i8);
DEF_INPUT_FREE(ppf_input_free_i4 , ppf_input_i4);

DEF_INPUT_TO_DEVICE(ppf_input_to_device_i16, ppf_input_i16, ppf_complex_i16);
DEF_INPUT_TO_DEVICE(ppf_input_to_device_i8 , ppf_input_i8 , ppf_complex_i8);
DEF_INPUT_TO_DEVICE(ppf_input_to_device_i4 , ppf_input_i4 , ppf_complex_i4);



/********************************
 * OUTPUT
 ********************************/

#ifdef PAGELOCKED_MEMORY
ppf_output* ppf_output_new(cl_uint stations, cl_uint channels, cl_uint nr_samples, opencl_t* cl)
{
	cl_int err = CL_SUCCESS;
	size_t len = sizeof(ppf_complex_f32) * stations * channels * PPF_NR_POLARIZATIONS;
	ppf_output* out = (ppf_output*)malloc(sizeof(ppf_output));
	assert(out);
	out->len = len;
	out->cl = cl;
	out->d = clCreateBuffer(cl->context, CL_MEM_READ_WRITE, len * nr_samples, NULL, &err);
	assert(err == CL_SUCCESS);
	out->hp = clCreateBuffer(cl->context, CL_MEM_READ_WRITE | CL_MEM_ALLOC_HOST_PTR, len * nr_samples, NULL, &err);
	assert(err == CL_SUCCESS);
	out->h = (ppf_complex_f32*)clEnqueueMapBuffer(cl->queue, out->hp, CL_TRUE, CL_MAP_READ, 0, len * nr_samples, 0, NULL, NULL, &err);
	assert(err == CL_SUCCESS);
	return out;
}

void ppf_output_free(ppf_output* out)
{
	clEnqueueUnmapMemObject (out->cl->queue, out->hp, out->h, 0, NULL, NULL);
	clReleaseMemObject(out->d);
	clReleaseMemObject(out->hp);
	free(out);
}

#else //PAGELOCKED_MEMORY
ppf_output* ppf_output_new(cl_uint stations, cl_uint channels, cl_uint nr_samples, opencl_t* cl)
{
	cl_int err = CL_SUCCESS;
	size_t len = sizeof(ppf_complex_f32) * stations * channels * PPF_NR_POLARIZATIONS;
	ppf_output* out = (ppf_output*)malloc(sizeof(ppf_output) + len * nr_samples);
	assert(out);
	out->len = len;
	out->cl = cl;
	out->d = clCreateBuffer(cl->context, CL_MEM_READ_WRITE, len * nr_samples, NULL, &err);
	assert(err == CL_SUCCESS);
	return out;
}

void ppf_output_free(ppf_output* out)
{
	clReleaseMemObject(ppf_output_get_device(out));
	free(out);
}
#endif

cl_int ppf_output_to_host(ppf_output* out, cl_uint nr_samples)
{
	ppf_complex_f32* h = ppf_output_get_host(out);
	cl_mem d = ppf_output_get_device(out);
	return clEnqueueReadBuffer(out->cl->queue, d, CL_TRUE, 0, out->len * nr_samples, h, 0, NULL, NULL);
}


/*********************************
 * EXECUTE
 *********************************/


/**
 *	REFERENCE
 */
#ifdef REF_IMPL

static inline cl_int ppf_fir_execute(ppf_fir* fir, cl_kernel k, cl_mem ind, cl_mem outd, cl_uint nr_samples)
{
	cl_mem delay   = fir->delay;
	cl_mem weights = fir->weights;
	cl_uint nr_taps = fir->nr_taps;
	//cl_uint nr_stations = fir->nr_stations;
	cl_uint nr_channels = fir->nr_channels;
	cl_uint delay_index = fir->delay_index;
	//cl_uint stride = nr_stations * nr_channels * PPF_NR_POLARIZATIONS;
	cl_int err;
	
	for(cl_uint i = 0; i < nr_samples; ++i)
	{
		delay_index = (delay_index - 1) & (nr_taps - 1);
		
		err = opencl_call(fir->cl, k, PPF_WORK_DIMS, fir->global, fir->local, "SSSbbbbI",
			nr_channels, nr_taps, delay_index, ind, delay, outd, weights, i);
		opencl_assert(err != CL_SUCCESS, "ppf_fir_execute error %d (%s)\n", err, opencl_err_string(err));
	}
	
	fir->delay_index = delay_index;
	return CL_SUCCESS;
}

#define FIR_EXECUTE(name, in_type, kernel_num)\
cl_int name(ppf_fir* fir, const in_type* in, ppf_output* out, cl_uint nr_samples)\
{\
	cl_kernel k    = fir->prog->kernels[kernel_num];\
	cl_mem ind     = ppf_input_get_device(in);\
	cl_mem outd    = ppf_output_get_device(out);\
	return ppf_fir_execute(fir, k, ind, outd, nr_samples);\
}

FIR_EXECUTE(ppf_fir_execute_i16, ppf_input_i16, 0)
FIR_EXECUTE(ppf_fir_execute_i8, ppf_input_i8, 1)
FIR_EXECUTE(ppf_fir_execute_i4, ppf_input_i4, 2)

/**
 *	OPTIMIZED
 */
#else


static inline cl_int ppf_fir_execute(ppf_fir* fir, cl_kernel k, cl_mem ind, cl_mem outd, cl_uint nr_samples)
{
	cl_mem delay   = fir->delay;
	cl_mem weights = fir->weights;
	cl_uint nr_taps = fir->nr_taps;
	cl_uint nr_stations = fir->nr_stations;
	cl_uint nr_channels = fir->nr_channels;
	cl_uint delay_step  = PPF_NR_POLARIZATIONS * nr_channels;
	cl_uint buffer_step = PPF_NR_POLARIZATIONS * nr_stations * nr_channels;
	cl_int err;
	
	err = opencl_call(fir->cl, k, PPF_WORK_DIMS, fir->global, fir->local, "SIIbbbbS",
			nr_channels, delay_step, buffer_step, ind, outd, delay, weights, nr_samples / nr_taps);
	opencl_assert(err != CL_SUCCESS, "ppf_fir_execute error %d (%s)\n", err, opencl_err_string(err));
	return err;
}

#define FIR_EXECUTE(name, in_type, kernel_num)\
static cl_int name(ppf_fir* fir, const in_type* in, ppf_output* out, cl_uint nr_samples)\
{\
	cl_kernel k    = fir->prog->kernels[kernel_num];\
	cl_mem ind     = ppf_input_get_device(in);\
	cl_mem outd    = ppf_output_get_device(out);\
	return ppf_fir_execute(fir, k, ind, outd, nr_samples);\
}


FIR_EXECUTE(ppf_fir_execute_i16_32, ppf_input_i16, 0)
FIR_EXECUTE(ppf_fir_execute_i16_16, ppf_input_i16, 1)
FIR_EXECUTE(ppf_fir_execute_i16_8 , ppf_input_i16, 2)
FIR_EXECUTE(ppf_fir_execute_i16_4 , ppf_input_i16, 3)

FIR_EXECUTE(ppf_fir_execute_i8_32, ppf_input_i8, 4)
FIR_EXECUTE(ppf_fir_execute_i8_16, ppf_input_i8, 5)
FIR_EXECUTE(ppf_fir_execute_i8_8 , ppf_input_i8, 6)
FIR_EXECUTE(ppf_fir_execute_i8_4 , ppf_input_i8, 7)

FIR_EXECUTE(ppf_fir_execute_i4_32, ppf_input_i4, 8)
FIR_EXECUTE(ppf_fir_execute_i4_16, ppf_input_i4, 9)
FIR_EXECUTE(ppf_fir_execute_i4_8 , ppf_input_i4, 10)
FIR_EXECUTE(ppf_fir_execute_i4_4 , ppf_input_i4, 11)

#ifndef NO64TAPS
FIR_EXECUTE(ppf_fir_execute_i16_64, ppf_input_i16, 12)
FIR_EXECUTE(ppf_fir_execute_i8_64, ppf_input_i8, 13)
FIR_EXECUTE(ppf_fir_execute_i4_64, ppf_input_i4, 14)
#endif


cl_int ppf_fir_execute_i16(ppf_fir* fir, const ppf_input_i16* in, ppf_output* out, cl_uint nr_samples)
{
	switch(fir->nr_taps)
	{
		case  4: return ppf_fir_execute_i16_4(fir, in, out, nr_samples);
		case  8: return ppf_fir_execute_i16_8(fir, in, out, nr_samples);
		case 16: return ppf_fir_execute_i16_16(fir, in, out, nr_samples);
		case 32: return ppf_fir_execute_i16_32(fir, in, out, nr_samples);
		case 64: return ppf_fir_execute_i16_64(fir, in, out, nr_samples);
		default: return CL_INVALID_VALUE;
	}
}

cl_int ppf_fir_execute_i8(ppf_fir* fir, const ppf_input_i8* in, ppf_output* out, cl_uint nr_samples)
{
	switch(fir->nr_taps)
	{
		case  4: return ppf_fir_execute_i8_4(fir, in, out, nr_samples);
		case  8: return ppf_fir_execute_i8_8(fir, in, out, nr_samples);
		case 16: return ppf_fir_execute_i8_16(fir, in, out, nr_samples);
		case 32: return ppf_fir_execute_i8_32(fir, in, out, nr_samples);
		case 64: return ppf_fir_execute_i8_64(fir, in, out, nr_samples);
		default: return CL_INVALID_VALUE;
	}
}

cl_int ppf_fir_execute_i4(ppf_fir* fir, const ppf_input_i4* in, ppf_output* out, cl_uint nr_samples)
{
	switch(fir->nr_taps)
	{
		case  4: return ppf_fir_execute_i4_4(fir, in, out, nr_samples);
		case  8: return ppf_fir_execute_i4_8(fir, in, out, nr_samples);
		case 16: return ppf_fir_execute_i4_16(fir, in, out, nr_samples);
		case 32: return ppf_fir_execute_i4_32(fir, in, out, nr_samples);
		case 64: return ppf_fir_execute_i4_64(fir, in, out, nr_samples);
		default: return CL_INVALID_VALUE;
	}
}


#endif


/**********************************************
 * FFT
 **********************************************/

/**
 *	Apple's FFT library
 */
#if FFT_LIB == 0
ppf_fft* ppf_fft_new(cl_uint nr_stations, cl_uint nr_channels, cl_uint nr_samples, opencl_t* cl)
{
	clFFT_Dim3 d = {nr_channels, 1, 1};
	cl_int err;
	clFFT_Plan plan = clFFT_CreatePlan(cl->context, d, clFFT_1D, clFFT_InterleavedComplexFormat, &err);
	opencl_assert(!plan || err != CL_SUCCESS, "FFT plan creation failed with error %s (%d)\n", opencl_err_string(err), err);
	
	ppf_fft* fft = (ppf_fft*)malloc(sizeof(ppf_fft));
	opencl_assert(!fft, "fft malloc failed\n");
	fft->plan = plan;
	fft->cl = cl;
	fft->stride = nr_samples * nr_stations * PPF_NR_POLARIZATIONS;
	return fft;
}

void ppf_fft_free(ppf_fft* fft)
{
	clFFT_DestroyPlan(fft->plan);
	free(fft);
}

cl_int ppf_fft_execute(ppf_fft* fft, ppf_output* out)
{
	cl_int err = 0;
	cl_mem buf = ppf_output_get_device(out);
	err  = clFFT_ExecuteInterleaved(fft->cl->queue, fft->plan,
		fft->stride, clFFT_Forward, buf, buf, 0, NULL, NULL);
	opencl_assert(err != CL_SUCCESS, "fft error %d = %s", err, opencl_err_string(err));
	err |= clFinish(fft->cl->queue);
	return err;
}

/**
 *	AMD's FFT library
 */
#elif FFT_LIB == 1

static void ppf_fft_teardown()
{
	clAmdFftTeardown();
}

ppf_fft* ppf_fft_new(cl_uint nr_stations, cl_uint nr_channels, cl_uint nr_samples, opencl_t* cl)
{
	static int initialized = 0;
	static clAmdFftSetupData setup;
	
	ppf_fft* fft;
	clAmdFftPlanHandle plan;
	cl_mem tmpbuf = NULL;
	cl_int err;
	size_t fftsize = nr_channels;
	
	// initialize first time.
	if(!initialized)
	{
		err  = clAmdFftInitSetupData(&setup);
		err |= clAmdFftSetup(&setup);
		opencl_assert(err != CL_SUCCESS, "AMD FFT init failed with error %d\n", err);
		atexit(ppf_fft_teardown);
		initialized = 1;
	}
	
	// create plan
	err = clAmdFftCreateDefaultPlan(&plan, cl->context, CLFFT_1D, &fftsize);
	opencl_assert(err != CL_SUCCESS, "clAmdFftCreateDefaultPlan failed with error %d\n", err);
	
	// settings
	clAmdFftSetPlanPrecision(plan, CLFFT_SINGLE_FAST);
	clAmdFftSetPlanBatchSize(plan, PPF_NR_POLARIZATIONS * nr_stations * nr_samples);
	clAmdFftSetLayout(plan, CLFFT_COMPLEX_INTERLEAVED, CLFFT_COMPLEX_INTERLEAVED);
	clAmdFftSetResultLocation(plan, CLFFT_INPLACE);
	
	// finish plan
	err = clAmdFftBakePlan(plan, 1, &cl->queue, NULL, NULL);
	opencl_assert(err != CL_SUCCESS, "clAmdFftBakePlan failed with error %d\n", err);
	
	// create temp buffer
	size_t tmpbufsz = 0;
	clAmdFftGetTmpBufSize(plan, &tmpbufsz);
	
	if(tmpbufsz)
	{
		tmpbuf = clCreateBuffer(cl->context, CL_MEM_READ_WRITE, tmpbufsz, NULL, &err);
		opencl_assert(err != CL_SUCCESS, "clCreateBuffer failed with error %d\n", err);
	}
	
	fft = (ppf_fft*)malloc(sizeof(ppf_fft));
	opencl_assert(!fft, "ppf_fft_new: malloc failed\n");
	fft->cl = cl;
	fft->plan = plan;
	fft->tmpbuf = tmpbuf;
	
	return fft;
}

void ppf_fft_free(ppf_fft* fft)
{
	if(fft->tmpbuf) clReleaseMemObject(fft->tmpbuf);
	clAmdFftDestroyPlan(&fft->plan);
	free(fft);
}

cl_int ppf_fft_execute(ppf_fft* fft, ppf_output* out)
{
	cl_mem buf = ppf_output_get_device(out);
	cl_int err = clAmdFftEnqueueTransform(fft->plan, CLFFT_FORWARD, 1, &fft->cl->queue,
		0, NULL, NULL, &buf, NULL, fft->tmpbuf);
	opencl_assert(err != CL_SUCCESS, "clAmdFftEnqueueTransform error %d\n", err);
	err |= clFinish(fft->cl->queue);
	return err;
}


/**
 *	Fixstar's FFT library
 */
#elif FFT_LIB == 2
ppf_fft* ppf_fft_new(cl_uint nr_stations, cl_uint nr_channels, cl_uint nr_samples, opencl_t* cl)
{
	cl_int err;
	cl_uint batches = PPF_NR_POLARIZATIONS * nr_stations * nr_samples;
	fsfft_plan plan = fsfft_new(cl->context, cl->queue, nr_channels, batches, &err);
	opencl_assert(!plan, "fft plan creation failed with error %d %s\n", err, opencl_err_string(err));
	
	ppf_fft* fft = (ppf_fft*)malloc(sizeof(ppf_fft));
	opencl_assert(!fft, "ppf_fft_new: malloc failed\n");
	fft->cl = cl;
	fft->plan = plan;
	return fft;
}

void ppf_fft_free(ppf_fft* fft)
{
	fsfft_free(fft->plan);
	free(fft);
}

cl_int ppf_fft_execute(ppf_fft* fft, ppf_output* out)
{
	cl_mem buf = ppf_output_get_device(out);
	cl_int err = CL_SUCCESS;
	err       |= fsfft_execute(fft->plan, buf, buf, FSFFT_FORWARD, 0, NULL);
	err       |= clFinish(fft->cl->queue);
	return err;
}


#endif
