/*
    A Polyphase Filter for GPUs and Multi-Core Processors
    Copyright (C) 2012 Karel van der Veldt (karel.vd.veldt@uva.nl)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __PPF_FIR_H
#define __PPF_FIR_H
#include <CL/cl.h>
#ifndef FFT_LIB
#	error FFT_LIB not defined!
#elif FFT_LIB == 0
#	include <clFFT.h>
#elif FFT_LIB == 1
#	include <clAmdFft.h>
#elif FFT_LIB == 2
#	include <fsfft.h>
#else
#	error FFT_LIB has invalid value (0 = Apple FFT, 1 = AMD FFT, 2 = Fixstar FFT)
#endif
#include "opencl.h"

#define PPF_NR_POLARIZATIONS 2

#define PPF_WORK_DIMS 2

typedef struct
{
	size_t global[PPF_WORK_DIMS];
	size_t local[PPF_WORK_DIMS];
	cl_mem delay;
	cl_mem weights;
	cl_uint nr_stations;
	cl_uint nr_channels;
	cl_uint nr_taps;
	cl_uint delay_index;
	opencl_t* cl;
	opencl_program_t* prog;
} ppf_fir;

//typedef cl_char    ppf_complex_i4;
typedef struct  __attribute__((packed))
{
	char x : 4;
	char y : 4;
} ppf_complex_i4;

typedef cl_char2   ppf_complex_i8;
typedef cl_short2 ppf_complex_i16;
typedef cl_float2 ppf_complex_f32;

#define ppf_cpx_r(cpx) ((cpx).x)
#define ppf_cpx_i(cpx) ((cpx).y)


#ifdef PAGELOCKED_MEMORY
#if 0
typedef struct
{
	opencl_t* cl;
	cl_mem d;
	cl_mem hp;
	size_t len;
	ppf_complex_i16* h;
} ppf_input_i16;

typedef struct
{
	opencl_t* cl;
	cl_mem d;
	cl_mem hp;
	size_t len;
	ppf_complex_i8* h;
} ppf_input_i8;

typedef struct
{
	opencl_t* cl;
	cl_mem d;
	cl_mem hp;
	size_t len;
	ppf_complex_i4* h;
} ppf_input_i4;
#else
typedef struct
{
	opencl_t* cl;
	cl_mem d;
	size_t len;
	ppf_complex_i16 h[];
} ppf_input_i16;

typedef struct
{
	opencl_t* cl;
	cl_mem d;
	size_t len;
	ppf_complex_i8 h[];
} ppf_input_i8;

typedef struct
{
	opencl_t* cl;
	cl_mem d;
	size_t len;
	ppf_complex_i4 h[];
} ppf_input_i4;
#endif

typedef struct
{
	opencl_t* cl;
	cl_mem d;
	cl_mem hp;
	size_t len;
	ppf_complex_f32* h;
} ppf_output;


#else // PAGELOCKED_MEMORY
typedef struct
{
	opencl_t* cl;
	cl_mem d;
	size_t len;
	ppf_complex_i16 h[];
} ppf_input_i16;

typedef struct
{
	opencl_t* cl;
	cl_mem d;
	size_t len;
	ppf_complex_i8 h[];
} ppf_input_i8;

typedef struct
{
	opencl_t* cl;
	cl_mem d;
	size_t len;
	ppf_complex_i4 h[];
} ppf_input_i4;

typedef struct
{
	opencl_t* cl;
	cl_mem d;
	size_t len;
	ppf_complex_f32 h[];
} ppf_output;
#endif

#define ppf_input_get_host(in) ((in)->h)
#define ppf_input_get_device(in) ((in)->d)
#define ppf_output_get_host(out) ((out)->h)
#define ppf_output_get_device(out) ((out)->d)

/**
 *	Apple's FFT library
 */
#if FFT_LIB == 0
typedef struct
{
	clFFT_Plan plan;
	opencl_t* cl;
	cl_uint stride;
} ppf_fft;

/**
 *	AMD's FFT library
 */
#elif FFT_LIB == 1
typedef struct
{
	opencl_t* cl;
	clAmdFftPlanHandle plan;
	cl_mem tmpbuf;
} ppf_fft;

/**
 *	Fixstart's FFT library
 */
#elif FFT_LIB == 2
typedef struct
{
	opencl_t* cl;
	fsfft_plan plan;
} ppf_fft;
#endif


/**
 *	Computes the index into the input array of the given sample.
 */
static inline unsigned int ppf_input_nth(int nr_channels,
	int station, int channel, int polarization)
{
	// input[s][c][p]
	return (station * nr_channels + channel) * PPF_NR_POLARIZATIONS + polarization;
}


/**
 *	Computes the index into the output array of the given sample.
 */
static inline unsigned int ppf_output_nth(int nr_stations, int nr_channels,
	int station, int channel, int polarization)
{
#if 1
	// output[p][s][c]
	return (polarization * nr_stations + station) * nr_channels + channel;
#else
	// output[s][p][c]
	return (station * PPF_NR_POLARIZATIONS + polarization) * nr_channels + channel;
#endif
}

ppf_fir* ppf_fir_new(cl_uint nr_stations, cl_uint nr_channels, cl_uint nr_taps, opencl_t* cl, int kernel);
void ppf_fir_free(ppf_fir* fir);
void ppf_fir_set_weights(ppf_fir* fir, const float* weights);

ppf_input_i16* ppf_input_new_i16(cl_uint stations, cl_uint channels, cl_uint nr_samples, opencl_t* cl);
ppf_input_i8* ppf_input_new_i8(cl_uint stations, cl_uint channels, cl_uint nr_samples, opencl_t* cl);
ppf_input_i4* ppf_input_new_i4(cl_uint stations, cl_uint channels, cl_uint nr_samples, opencl_t* cl);
void ppf_input_free_i16(ppf_input_i16* in);
void ppf_input_free_i8(ppf_input_i8* in);
void ppf_input_free_i4(ppf_input_i4* in);
cl_int ppf_input_to_device_i16(ppf_input_i16* in, cl_uint nr_samples);
cl_int ppf_input_to_device_i8(ppf_input_i8* in, cl_uint nr_samples);
cl_int ppf_input_to_device_i4(ppf_input_i4* in, cl_uint nr_samples);


ppf_output* ppf_output_new(cl_uint stations, cl_uint channels, cl_uint nr_samples, opencl_t* cl);
void ppf_output_free(ppf_output* out);
cl_int ppf_output_to_host(ppf_output* out, cl_uint nr_samples);

cl_int ppf_fir_execute_i16(ppf_fir* fir, const ppf_input_i16* in, ppf_output* out, cl_uint nr_samples);
cl_int ppf_fir_execute_i8(ppf_fir* fir, const ppf_input_i8* in, ppf_output* out, cl_uint nr_samples);
cl_int ppf_fir_execute_i4(ppf_fir* fir, const ppf_input_i4* in, ppf_output* out, cl_uint nr_samples);

ppf_fft* ppf_fft_new(cl_uint nr_stations, cl_uint nr_channels, cl_uint nr_samples, opencl_t* cl);
void ppf_fft_free(ppf_fft* fft);
cl_int ppf_fft_execute(ppf_fft* fft, ppf_output* out);

#endif
