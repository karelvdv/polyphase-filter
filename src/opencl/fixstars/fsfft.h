/**
 *	This FFT code is from Fixstars OpenCL samples (folder 6-1):
 *	http://www.fixstars.com/en/company/books/opencl/sample.html
 *
 *	I just put the code in a library and added FFT batching.
 *
 *	This FFT library can only do 1D single precision complex interleaved FFTs.
 */
#ifndef __FSFFT_H
#define __FSFFT_H
#include <CL/cl.h>

enum
{
	FSFFT_FORWARD = 0x00000000,
	FSFFT_INVERSE = 0x80000000,
};

// I'm just copying what the other FFT libraries do.
typedef struct fsfft_plan_s* fsfft_plan;

/**
 *	Creates an FFT plan.
 *	@param context The context.
 *	@param queue   The queue, which should be set to in-order queueing (the default).
 *	@param length  The length of the FFT, which must be a power of two.
 *	@param batches The number of FFTs to run.
 *	@param err     The OpenCL return value.
 *	@return        Returns NULL on error.
 */
fsfft_plan fsfft_new(cl_context context, cl_command_queue queue,
	cl_uint length, cl_uint batches, cl_int* err);

/**
 *	Frees an FFT plan from memory.
 *	@param plan The plan to free.
 *	@return     Returns CL_SUCCESS on success.
 */
cl_int fsfft_free(fsfft_plan plan);

/**
 *	Enqueues the FFT for execution (non-blocking!).
 *	@param plan                    The plan to execute.
 *	@param dst                     Where to put the output data (can be the same as src).
 *	@param src                     Where to get the input data (can be the same as dst).
 *	@param direction               Either FSFFT_FORWARD or FSFFT_INVERSE.
 *	@param num_events_in_wait_list Specifies the number of events that must finish before the FFT is executed.
 *	@param event_wait_list         Specifies which events must finish before the FFT is executed.
 *	@return                        Returns CL_SUCCESS on success.
 */
cl_int fsfft_execute(fsfft_plan plan, cl_mem dst, cl_mem src, cl_uint direction,
	cl_uint num_events_in_wait_list, const cl_event* event_wait_list);

#endif
