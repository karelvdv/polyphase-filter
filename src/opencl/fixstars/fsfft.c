#include <assert.h>
#include <memory.h>
#include <stdlib.h>
#include <stdio.h>
#include "fsfft.h"
#include "fsfft.cl.h"

typedef struct fsfft_plan_s
{
	cl_command_queue queue;
	cl_program program;
	cl_kernel brev;
	cl_kernel bfly;
	cl_kernel norm;
	cl_mem spin;
	cl_uint m; // log2(n)
	cl_uint n;
	cl_uint batches;
} fsfft_plan_t;

static inline void setWorkSize(size_t* gws, size_t* lws, cl_int x, cl_int y)
{
	switch(y)
	{
	case 1:
		gws[0] = x;
		gws[1] = 1;
		lws[0] = 256;
		lws[1] = 1;
		break;
	default:
		gws[0] = x;
		gws[1] = y;
		lws[0] = 16;
		lws[1] = 32;
		break;
	}
}

static cl_int fsfft_init(cl_context context, cl_program* program)
{
	cl_device_id devices[16];
	cl_uint ndevices;
	cl_int err = CL_SUCCESS;
	const char* src  = source;
	size_t sourcelen = sizeof(source);

	err |= clGetContextInfo(context, CL_CONTEXT_NUM_DEVICES, sizeof(cl_uint), &ndevices, NULL);
	err |= clGetContextInfo(context, CL_CONTEXT_DEVICES, sizeof(cl_device_id) * ndevices, devices, NULL);
	if(err != CL_SUCCESS) return err;
	
	*program = clCreateProgramWithSource(context, 1, &src, &sourcelen, &err);
	if(err != CL_SUCCESS) return err;
	
	err |= clBuildProgram(*program, ndevices, devices, NULL, NULL, NULL);
	if(err != CL_SUCCESS) return err;
	
	return CL_SUCCESS;
}

fsfft_plan fsfft_new(cl_context context, cl_command_queue queue,
	cl_uint length, cl_uint batches, cl_int* err)
{
	cl_mem spin = NULL;
	cl_uint n    = length;
	cl_uint m    = 0;
	cl_kernel sfac, brev, bfly, norm;
	cl_program program = NULL;
	fsfft_plan plan;
	size_t gws[1];
	
	while(length > 1)
	{
		length >>= 1;
		m++;
	}
	
	*err = fsfft_init(context, &program);
	if(*err != CL_SUCCESS) return NULL;
	
	sfac = clCreateKernel(program, "spinFact"  , err);
	brev = clCreateKernel(program, "bitReverse", err);
	bfly = clCreateKernel(program, "butterfly" , err);
	norm = clCreateKernel(program, "norm"      , err);
	if(!sfac || !brev || !bfly || !norm) return NULL;
	
    spin = clCreateBuffer(context, CL_MEM_READ_WRITE, (n / 2) * sizeof(cl_float2), NULL, err);
	if(*err != CL_SUCCESS) return NULL;
	
	/* Create spin factor */
	gws[0] = n / 2;
	*err |= clSetKernelArg(sfac, 0, sizeof(cl_mem), (void *)&spin);
	*err |= clSetKernelArg(sfac, 1, sizeof(cl_uint), (void *)&n);
	*err |= clEnqueueNDRangeKernel(queue, sfac, 1, NULL, gws, NULL, 0, NULL, NULL);
	*err |= clFinish(queue);
	if(*err != CL_SUCCESS) return NULL;
	
	clReleaseKernel(sfac);
	
	plan = (fsfft_plan)malloc(sizeof(fsfft_plan_t));
	if(!plan)
	{
		clReleaseMemObject(spin);
		*err = CL_OUT_OF_HOST_MEMORY;
		return NULL;
	}
	
	plan->queue   = queue;
	plan->program = program;
	plan->brev    = brev;
	plan->bfly    = bfly;
	plan->norm    = norm;
	plan->spin    = spin;
	plan->m       = m;
	plan->n       = n;
	plan->batches = batches;
	return plan;
}


cl_int fsfft_free(fsfft_plan plan)
{
	cl_int err = CL_SUCCESS;
	err |= clReleaseMemObject(plan->spin);
	err |= clReleaseKernel(plan->brev);
	err |= clReleaseKernel(plan->bfly);
	err |= clReleaseKernel(plan->norm);
	err |= clReleaseProgram(plan->program);
	free(plan);
	return err;
}

cl_int fsfft_execute(fsfft_plan plan, cl_mem dst, cl_mem src, cl_uint direction,
	cl_uint num_events_in_wait_list, const cl_event* event_wait_list)
{
	cl_kernel brev = plan->brev;
	cl_kernel bfly = plan->bfly;
	cl_kernel norm = plan->norm;
	cl_int err     = CL_SUCCESS;
	cl_uint m      = plan->m;
	cl_uint n      = plan->n;
	//cl_event ev_brev;
	//cl_event ev_bfly;
	//cl_event ev_norm;
	cl_uint iter;
	size_t gws[3];
	size_t lws[3] = {256, 1, 1};
	
	err |= clSetKernelArg(brev, 0, sizeof(cl_mem), (void *)&dst);
	err |= clSetKernelArg(brev, 1, sizeof(cl_mem), (void *)&src);
	err |= clSetKernelArg(brev, 2, sizeof(cl_uint), (void *)&m);
	err |= clSetKernelArg(brev, 3, sizeof(cl_uint), (void *)&n);
	
	err |= clSetKernelArg(bfly, 0, sizeof(cl_mem), (void *)&dst);
	err |= clSetKernelArg(bfly, 1, sizeof(cl_mem), (void *)&plan->spin);
	err |= clSetKernelArg(bfly, 2, sizeof(cl_uint), (void *)&m);
	err |= clSetKernelArg(bfly, 3, sizeof(cl_uint), (void *)&n);
	err |= clSetKernelArg(bfly, 5, sizeof(cl_uint), (void *)&direction);
	
	err |= clSetKernelArg(norm, 0, sizeof(cl_mem), (void *)&dst);
	err |= clSetKernelArg(norm, 1, sizeof(cl_uint), (void *)&n);
	
	if(err != CL_SUCCESS) return err;
	
	gws[2] = plan->batches;
	
	/* Reverse bit ordering */
	//setWorkSize(gws, lws, n, n);
	gws[0] = n;
	gws[1] = n;
	err |= clEnqueueNDRangeKernel(plan->queue, brev, 3, NULL, gws, lws, num_events_in_wait_list, event_wait_list, NULL/*&ev_brev*/);
	//err |= clWaitForEvents(1, &ev_brev);
	if(err != CL_SUCCESS) return err;
	
	/* Perform Butterfly Operations*/
	//setWorkSize(gws, lws, n / 2, n);
	gws[0] = n / 2;
	gws[1] = n;
	for(iter = 1; iter <= m; iter++)
	{
		err |= clSetKernelArg(bfly, 4, sizeof(cl_uint), (void *)&iter);
		err |= clEnqueueNDRangeKernel(plan->queue, bfly, 3, NULL, gws, lws, 0, NULL, NULL/*&ev_bfly*/);
		//err |= clWaitForEvents(1, &ev_bfly);
		if(err != CL_SUCCESS) return err;
	}
	
	if (direction == FSFFT_INVERSE)
	{
		//setWorkSize(gws, lws, n, n);
		gws[0] = n;
		gws[1] = n;
		err |= clEnqueueNDRangeKernel(plan->queue, norm, 3, NULL, gws, lws, 0, NULL, NULL/*&ev_norm*/);
		//err |= clWaitForEvents(1, &ev_norm);
		if(err != CL_SUCCESS) return err;
	}
	
	return clFlush(plan->queue);
}
