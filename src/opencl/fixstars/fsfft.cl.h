static const char source[] = "\
#define PI       3.14159265358979323846\n                                                             \
#define PI_2     1.57079632679489661923\n                                                             \
__kernel void spinFact(__global float2* w, int n)                                                     \
{                                                                                                     \
    uint i       = get_global_id(0);                                                                  \
    float2 angle = (float2)(2*i*PI/(float)n,(2*i*PI/(float)n)+PI_2);                                  \
    w[i]         = cos(angle);                                                                        \
}                                                                                                     \
__kernel void bitReverse(__global float2 *dst, __global float2 *src, uint m, uint n)                  \
{                                                                                                     \
    uint gid     = get_global_id(0);                                                                  \
    uint nid     = get_global_id(1);                                                                  \
    uint j       = gid;                                                                               \
    j            = (j & 0x55555555) <<  1 | (j & 0xAAAAAAAA) >>  1;                                   \
    j            = (j & 0x33333333) <<  2 | (j & 0xCCCCCCCC) >>  2;                                   \
    j            = (j & 0x0F0F0F0F) <<  4 | (j & 0xF0F0F0F0) >>  4;                                   \
    j            = (j & 0x00FF00FF) <<  8 | (j & 0xFF00FF00) >>  8;                                   \
    j            = (j & 0x0000FFFF) << 16 | (j & 0xFFFF0000) >> 16;                                   \
    j          >>= (32-m);                                                                            \
    dst         += get_global_id(2) * n;                                                              \
    src         += get_global_id(2) * n;                                                              \
    dst[nid*n+j] = src[nid*n+gid];                                                                    \
}                                                                                                     \
__kernel void norm(__global float2 *x, uint n)                                                        \
{                                                                                                     \
    uint gid     = get_global_id(0);                                                                  \
    uint nid     = get_global_id(1);                                                                  \
    uint j       = gid;                                                                               \
    j            = (j & 0x0000FFFF) << 16 | (j & 0xFFFF0000) >> 16;                                   \
    x           += get_global_id(2) * n;                                                              \
    x[nid*n+gid] = x[nid*n+gid] / (float2)((float)n, (float)n);                                       \
}                                                                                                     \
__kernel void butterfly(__global float2 *x, __global float2* w, uint m, uint n, uint iter, uint flag) \
{                                                                                                     \
    uint gid                = get_global_id(0);                                                       \
    uint nid                = get_global_id(1);                                                       \
    uint butterflySize      = 1 << (iter-1);                                                          \
    uint butterflyGrpDist   = 1 << iter;                                                              \
    uint butterflyGrpNum    = n >> iter;                                                              \
    uint butterflyGrpBase   = (gid >> (iter-1))*(butterflyGrpDist);                                   \
    uint butterflyGrpOffset = gid & (butterflySize-1);                                                \
    uint a                  = nid * n + butterflyGrpBase + butterflyGrpOffset;                        \
    uint b                  = a + butterflySize;                                                      \
    uint l                  = butterflyGrpNum * butterflyGrpOffset;                                   \
    x                      += get_global_id(2) * n;                                                   \
    float2 xa               = x[a];                                                                   \
    float2 xb               = x[b];                                                                   \
    float2 xbxx             = xb.xx;                                                                  \
    float2 xbyy             = xb.yy;                                                                  \
    float2 wab              = as_float2(as_uint2(w[l])   ^ (uint2)(0x0, flag));                       \
    float2 wayx             = as_float2(as_uint2(wab.yx) ^ (uint2)(0x80000000, 0x0));                 \
    float2 wbyx             = as_float2(as_uint2(wab.yx) ^ (uint2)(0x0, 0x80000000));                 \
    float2 resa             = xa + xbxx*wab + xbyy*wayx;                                              \
    float2 resb             = xa - xbxx*wab + xbyy*wbyx;                                              \
    x[a]                    = resa;                                                                   \
    x[b]                    = resb;                                                                   \
}";
