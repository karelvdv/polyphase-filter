/*
    A Polyphase Filter for GPUs and Multi-Core Processors
    Copyright (C) 2012 Karel van der Veldt (karel.vd.veldt@uva.nl)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <stdlib.h>
#include <CL/cl.h>
#include "orig_weights.h"
#include "opencl.h"
#include "ppf.h"
#include "timer.h"


#if !defined(DATATYPE)
#	error "Definition DATATYPE must be set to 4, 8 or 16!"
#elif DATATYPE == 16
#	define COMPLEX    ppf_complex_i16
#	define INPUT      ppf_input_i16
#	define INPUT_NEW  ppf_input_new_i16
#	define INPUT_TRANSFER ppf_input_to_device_i16
#	define INPUT_FREE ppf_input_free_i16
#	define FIR_EXEC   ppf_fir_execute_i16
#elif DATATYPE == 8
#	define COMPLEX    ppf_complex_i8
#	define INPUT      ppf_input_i8
#	define INPUT_NEW  ppf_input_new_i8
#	define INPUT_TRANSFER ppf_input_to_device_i8
#	define INPUT_FREE ppf_input_free_i8
#	define FIR_EXEC   ppf_fir_execute_i8
#elif DATATYPE == 4
#	define COMPLEX    ppf_complex_i4
#	define INPUT      ppf_input_i4
#	define INPUT_NEW  ppf_input_new_i4
#	define INPUT_TRANSFER ppf_input_to_device_i4
#	define INPUT_FREE ppf_input_free_i4
#	define FIR_EXEC   ppf_fir_execute_i4
#else
#	error "Unimplemented DATATYPE."
#endif





void test()
{
	static const char kernel_string[] =
		"__kernel void vectorAdd(__global const float* a, __global float* b)\
		 {\
			int n = get_global_id(0);\
			b[n] = a[n] + 1;\
		 }";
	static const char* kernels[] = {"vectorAdd"};
	static const size_t work = 2048;
	opencl_t* cl = opencl_new(0, 0, CL_DEVICE_TYPE_GPU);
	opencl_program_t* prog = opencl_compile(cl,
		kernel_string, sizeof(kernel_string), "", kernels, 1, 0);
	
	cl_kernel k = prog->kernels[0];
	cl_int err;
	size_t bytes = work * sizeof(cl_float);
	cl_float data[work];
	
	for(size_t i = 0; i < work; ++i) data[i] = i;
	
	cl_mem buf = clCreateBuffer(cl->context, CL_MEM_READ_WRITE, bytes, NULL, &err);
	clEnqueueWriteBuffer (cl->queue, buf, CL_TRUE, 0, bytes, data, 0, NULL, NULL);
	opencl_call(cl, k, 1, &work, NULL, "bb", buf, buf);
	clEnqueueReadBuffer (cl->queue, buf, CL_TRUE, 0, bytes, data, 0, NULL, NULL);
	
	for(size_t i = 0; i < work; ++i) printf("%lu: %f\n", i, data[i]);
}


static void gen_input(COMPLEX* in, int nr_stations, int nr_channels, int doprint, int nr_samples)
{
	for(int st = 1; st <= nr_stations; ++st)
	{
		if(doprint) printf("%d: ", st - 1);
		for(int ch = 1; ch <= nr_channels; ++ch)
		{
			if(doprint) printf("| ");
			for(int po = 0; po < PPF_NR_POLARIZATIONS; ++po)
			{
				unsigned int index = ppf_input_nth(nr_channels, st - 1, ch - 1, po);
				ppf_cpx_r(in[index]) = st * ch + po;
				ppf_cpx_i(in[index]) = ch;
				if(doprint) printf("%d %d, ", ppf_cpx_r(in[index]), ppf_cpx_i(in[index]));
			}
		}
		if(doprint) printf("\n");
	}
	
	for(int sa = 1; sa < nr_samples; ++sa)
	{
		unsigned int len = nr_stations * nr_channels * PPF_NR_POLARIZATIONS;
		memcpy(in + sa * len, in, len * sizeof(COMPLEX));
	}
}

static void print_output(const ppf_complex_f32* out, int nr_stations, int nr_channels)
{
	static const char* ps[] = {"X", "Y"};
	
	for(int st = 0; st < nr_stations; ++st)
	{
		//printf("%d: ", st);
		for(int po = 0; po < PPF_NR_POLARIZATIONS; ++po)
		{
			printf("%d%s: ", st, ps[po]);
			for(int ch = 0; ch < nr_channels; ++ch)
			{
				//unsigned int index = ppf_output_nth(nr_stations, nr_channels, st, ch, po);
				printf("%.2g %.2g, ", ppf_cpx_r(out[0]), ppf_cpx_i(out[0]));
				out++;
			}
			printf("\n");
		}
	}
}




void test2(int kernel)
{
	static const float weights[] = {
		0.25, 0.25, 0.25, 0.25,
		0.25, 0.25, 0.25, 0.25,
		0.25, 0.25, 0.25, 0.25,
		0.25, 0.25, 0.25, 0.25,
	};
	
	cl_uint nr_stations = 2;
	cl_uint nr_channels = 4;
	cl_uint nr_taps     = 4;
#ifdef REF_IMPL
	cl_uint nr_samples  = 1;
#else
	cl_uint nr_samples  = nr_taps;
#endif
	int type = kernel >= 2 ? CL_DEVICE_TYPE_CPU : CL_DEVICE_TYPE_GPU;
	opencl_t* cl    = opencl_new(0, 0, type);
	ppf_fir* fir    = ppf_fir_new(nr_stations, nr_channels, nr_taps, cl, kernel);
	INPUT* in       = INPUT_NEW(nr_stations, nr_channels, nr_samples, cl);
	ppf_output* out = ppf_output_new(nr_stations, nr_channels, nr_samples, cl);
	
	ppf_fir_set_weights(fir, weights);
	
	COMPLEX* input = ppf_input_get_host(in);
	gen_input(input, nr_stations, nr_channels, 1, nr_samples);
	
	INPUT_TRANSFER(in, nr_samples);
	
#ifdef REF_IMPL
	for(cl_uint i = 0; i < nr_taps; ++ i)
	{
		FIR_EXEC(fir, in, out, nr_samples);
		ppf_output_to_host(out, nr_samples);
		print_output(ppf_output_get_host(out), nr_stations, nr_channels);
	}
#else
	FIR_EXEC(fir, in, out, nr_samples);
	ppf_output_to_host(out, nr_samples);

	for(cl_uint i = 0; i < nr_taps; ++i)
	{
		unsigned int len = nr_stations * nr_channels * PPF_NR_POLARIZATIONS;
		print_output(ppf_output_get_host(out) + i * len, nr_stations, nr_channels);
	}
#endif
}


/*void make_input(COMPLEX* input, int nr_channels)
{
	// make up some input data
	for(int i = 0; i < nr_channels; ++i, input += PPF_NR_POLARIZATIONS)
	{
		float a = (float)i;
		input[0].x =  100.0 * sinf( 2.0 * M_PI * (a / 256.0) ); // polarization 0
		input[0].y =  100.0 * cosf( 2.0 * M_PI * (a / 256.0) );
		input[1].x =  input[0].x; //sinf( 2.0 * M_PI * (a / 256.0) ); // polarization 1
		input[1].y = -input[0].y; //-cosf( 2.0 * M_PI * (a / 256.0) );
	}
}*/


int roundup(int n, int m)
{
	int t = n % m;
	return t == 0 ? n : n + (m - t);
}

void perf_test(int nr_stations, int nr_channels, int nr_taps, int nr_runs,
	int nr_batches, int do_fir, int do_fft, int do_io, int kernel)
{
#ifdef REF_IMPL
	int nr_samples = 1;
#else
	int nr_samples = nr_taps * nr_batches;
#endif
	nr_runs = roundup(nr_runs, nr_samples);
	
	cl_int err = CL_SUCCESS;
	cl_int cl_type = kernel >= 2 ? CL_DEVICE_TYPE_CPU : CL_DEVICE_TYPE_GPU;
	
	opencl_t* cl       = opencl_new(0, 0, cl_type);
	INPUT* input       = INPUT_NEW(nr_stations, nr_channels, nr_samples, cl);
	ppf_output* output = ppf_output_new(nr_stations, nr_channels, nr_samples, cl);
	ppf_fir* fir       = ppf_fir_new(nr_stations, nr_channels, nr_taps, cl, kernel);
	ppf_fft* fft       = do_fft ? ppf_fft_new(nr_stations, nr_channels, nr_samples, cl) : NULL;
	double cpu_mhz     = get_cpu_mhz();
	double secs        = 0.0;
	
	ppf_fir_set_weights(fir, ppf_orig_weights);

	/*for(int i = 0; i < nr_stations * nr_samples; ++i)
	{
		make_input( ppf_input_nth( ppf_input_get_host(input), i, nr_channels), nr_channels );
	}*/

	for(int i = 0; i < nr_runs; i += nr_samples)
	{
		ticks t0 = get_ticks();

		if(do_io)  opencl_assert(INPUT_TRANSFER(input, nr_samples) != CL_SUCCESS, "input transfer failed\n");
		if(do_fir) opencl_assert(FIR_EXEC(fir, input, output, nr_samples) != CL_SUCCESS, "fir exe failed\n");

		if(do_fft)
		{	
			err = ppf_fft_execute(fft, output);
			opencl_assert(err != CL_SUCCESS, "fft failed %s\n", opencl_err_string(err));
		}

		if(do_io)  opencl_assert(ppf_output_to_host(output, nr_samples) != CL_SUCCESS, "output transfer failed\n");
		
		clFinish(cl->queue);

		secs += ticks_to_secs( get_ticks() - t0, cpu_mhz );
	}

	double io_bytes = sizeof(COMPLEX) + sizeof(ppf_complex_f32);
	double io_gb    = 0;

	if(do_io)
	{
		//io_bytes = sizeof(COMPLEX) + sizeof(ppf_complex_f32);
		io_gb    = PPF_NR_POLARIZATIONS * nr_stations * nr_channels * io_bytes * (nr_runs / (1024.0 * 1024.0 * 1024.0));
	}

	double fir_bytes = 0;
	double fir_flop  = 0;
	double fir_ai    = 0;
	double fir_gb    = 0;
	double fir_gflop = 0;

	// r 1 x sample
	// w 1 x f32*2 output
	// r nr_taps x f32 weights
	// r nr_taps - 1 x f32*2 taps
	// w nr_taps - 1 x f32*2 taps
	if(do_fir)
	{
		fir_bytes = sizeof(float) * nr_taps + 2 * (sizeof(ppf_complex_f32) * (nr_taps - 1 + 1) / nr_samples);
		fir_flop  = 2 + 4 * (nr_taps - 1);
		fir_ai    = fir_flop / (io_bytes + fir_bytes);
		fir_gb    = PPF_NR_POLARIZATIONS * nr_stations * nr_channels * fir_bytes * (nr_runs / (1024.0 * 1024.0 * 1024.0));
		fir_gflop = PPF_NR_POLARIZATIONS * nr_stations * nr_channels * fir_flop  * (nr_runs / 1000000000.0);
	}

	// fft
	double fft_bytes = 0;
	double fft_flop  = 0;
	double fft_ai    = 0;
	double fft_gb    = 0;
	double fft_gflop = 0;

	if(do_fft)
	{
		fft_bytes = 4.0 * nr_channels * sizeof(ppf_complex_f32);
		fft_flop  = 5.0 * nr_channels * (log(nr_channels) / log(2));
		fft_ai    = fft_flop / fft_bytes;
		fft_gb    = PPF_NR_POLARIZATIONS * nr_stations * fft_bytes * (nr_runs / (1024.0 * 1024.0 * 1024.0));
		fft_gflop = PPF_NR_POLARIZATIONS * nr_stations * fft_flop * (nr_runs / 1000000000.0);
	}

	printf("Stat Chan Taps Runs GB Time(s) GB/s GFLOP/s AI Samples\n");
	printf("% 4d % 4d % 4d % 4d %g "
		"% 2.4g % 1.2g % 2.4g % 1.2g "
		"%d\n",
		nr_stations, nr_channels, nr_taps, nr_runs, io_gb + fir_gb + fft_gb,
		secs, (io_gb + fir_gb + fft_gb) / secs, (fir_gflop + fft_gflop) / secs, fir_ai + fft_ai, nr_samples);

	ppf_fir_free(fir);
	//ppf_fft_free(fft);
	INPUT_FREE(input);
	ppf_output_free(output);
}


void fft_perf(int nr_stations, int nr_channels, int nr_runs, int device, int do_io)
{
	cl_int err = CL_SUCCESS;
	cl_int cl_type = device == 0 ? CL_DEVICE_TYPE_CPU : CL_DEVICE_TYPE_GPU;
	
	opencl_t* cl         = opencl_new(0, 0, cl_type);
	ppf_input_i16* input = ppf_input_new_i16(nr_stations, nr_channels, 2, cl);
	ppf_output* output   = ppf_output_new(nr_stations, nr_channels, 1, cl);
	ppf_fft* fft         = ppf_fft_new(nr_stations, nr_channels, 1, cl);
	double cpu_mhz       = get_cpu_mhz();
	double secs          = 0.0;
	
	for(int i = 0; i < nr_runs; ++i)
	{
		ticks t0 = get_ticks();
		
		if(do_io)  opencl_assert(ppf_input_to_device_i16(input, 2) != CL_SUCCESS, "input transfer failed\n");
		
		err = ppf_fft_execute(fft, output);
		opencl_assert(err != CL_SUCCESS, "fft failed %s\n", opencl_err_string(err));
		
		if(do_io)  opencl_assert(ppf_output_to_host(output, 1) != CL_SUCCESS, "output transfer failed\n");
		
		clFinish(cl->queue);
		
		secs += ticks_to_secs( get_ticks() - t0, cpu_mhz );
	}
	
	//double fft_bytes = 4.0 * nr_channels * sizeof(ppf_complex_f32);
	double fft_flop  = 5.0 * nr_channels * (log(nr_channels) / log(2));
	//double fft_ai    = fft_flop / fft_bytes;
	double fft_gflop = PPF_NR_POLARIZATIONS * nr_stations * fft_flop * (nr_runs / 1000000000.0);
	
	printf("Stat Chan Runs Time T/Run GFLOP/s\n");
	printf("% 4d % 4d %4d %g %g %g\n", nr_stations, nr_channels, nr_runs, secs, secs / nr_runs, fft_gflop / secs);
}




static void platforms()
{
	char param[256];
	cl_platform_id platforms[4];
	cl_uint nplatforms = 0;

	clGetPlatformIDs(4, platforms, &nplatforms);
	
	for(cl_uint i = 0; i < nplatforms; ++i)
	{
		printf("Platform %d:\n", i);
		clGetPlatformInfo(platforms[i], CL_PLATFORM_PROFILE, 256, param, NULL);
		printf("-- Profile: %s\n", param);
		clGetPlatformInfo(platforms[i], CL_PLATFORM_VERSION, 256, param, NULL);
		printf("-- Version: %s\n", param);
		clGetPlatformInfo(platforms[i], CL_PLATFORM_NAME, 256, param, NULL);
		printf("-- Name: %s\n", param);
		clGetPlatformInfo(platforms[i], CL_PLATFORM_VENDOR, 256, param, NULL);
		printf("-- Vendor: %s\n", param);
		clGetPlatformInfo(platforms[i], CL_PLATFORM_EXTENSIONS, 256, param, NULL);
		printf("-- Extensions: %s\n", param);
	}
}

static char* getdevstr(cl_device_id d, cl_device_info info)
{
	static char val[256];
	clGetDeviceInfo(d, info, 256, val, NULL);
	return val;
}

static cl_uint getdevuint(cl_device_id d, cl_device_info info)
{
	cl_uint val;
	clGetDeviceInfo(d, info, sizeof(cl_uint), &val, NULL);
	return val;
}

static cl_ulong getdevulong(cl_device_id d, cl_device_info info)
{
	cl_ulong val;
	clGetDeviceInfo(d, info, sizeof(cl_ulong), &val, NULL);
	return val;
}

static cl_bool getdevbool(cl_device_id d, cl_device_info info)
{
	cl_bool val;
	clGetDeviceInfo(d, info, sizeof(cl_bool), &val, NULL);
	return val;
}

static cl_device_type getdevtype(cl_device_id d, cl_device_info info)
{
	cl_device_type val;
	clGetDeviceInfo(d, info, sizeof(cl_device_type), &val, NULL);
	return val;
}

static size_t* getdevsize_tp(cl_device_id d, cl_device_info info)
{
	static size_t val[3];
	clGetDeviceInfo(d, info, sizeof(size_t) * 3, &val, NULL);
	return val;
}

static void devices()
{
	cl_platform_id platforms[4];
	cl_uint nplatforms = 0;
	cl_device_id devices[4];
	cl_uint ndevices = 0;
	size_t* sizes;
	
	clGetPlatformIDs(4, platforms, &nplatforms);
	
	for(cl_uint p = 0; p < nplatforms; ++p)
	{
		clGetDeviceIDs(platforms[p], CL_DEVICE_TYPE_ALL, 4, devices, &ndevices);
		
		for(cl_uint d = 0; d < ndevices; ++d)
		{
			cl_device_id dev = devices[d];
			printf("Platform %d / Device %d:\n", p, d);
			printf( "-- Name: %s\n", getdevstr(dev, CL_DEVICE_NAME) );
			printf( "-- Vendor: %s\n", getdevstr(dev, CL_DEVICE_VENDOR) );
			printf( "-- Device Version: %s\n", getdevstr(dev, CL_DEVICE_VERSION) );
			printf( "-- Driver Version: %s\n", getdevstr(dev, CL_DRIVER_VERSION) );
#ifdef CL_DEVICE_OPENCL_C_VERSION
			printf( "-- OpenCL C Version: %s\n", getdevstr(dev, CL_DEVICE_OPENCL_C_VERSION) );
#endif
			printf( "-- Type: %lu\n", getdevtype(dev, CL_DEVICE_TYPE) );
			printf( "-- Available: %d\n", getdevbool(dev, CL_DEVICE_AVAILABLE) );
			printf( "-- Address bits: %d\n", getdevuint(dev, CL_DEVICE_ADDRESS_BITS) );
			printf( "-- Extensions: %s\n", getdevstr(dev, CL_DEVICE_EXTENSIONS) );
			printf( "-- Global memory cache: %lu bytes\n", getdevulong(dev, CL_DEVICE_GLOBAL_MEM_CACHE_SIZE) );
			printf( "-- Global memory: %lu bytes\n", getdevulong(dev, CL_DEVICE_GLOBAL_MEM_SIZE) );
			printf( "-- Local memory: %lu bytes\n", getdevulong(dev, CL_DEVICE_LOCAL_MEM_SIZE) );
			printf( "-- Constant memory: %lu bytes\n", getdevulong(dev, CL_DEVICE_MAX_CONSTANT_BUFFER_SIZE) );
			printf( "-- Max clock frequency: %lu MHz\n", getdevulong(dev, CL_DEVICE_MAX_CLOCK_FREQUENCY) );
			printf( "-- Max work group size: %lu\n", getdevulong(dev, CL_DEVICE_MAX_WORK_GROUP_SIZE) );
			sizes = getdevsize_tp(dev, CL_DEVICE_MAX_WORK_ITEM_SIZES);
			printf( "-- Max work item sizes: %lu x %lu x %lu\n", sizes[0], sizes[1], sizes[2]);
		}
	}
}

static void usage()
{
	printf(
		"usage: ppf-cl-i16 perf stations channels taps runs batches kernel io fir fft\n"
		"usage: ppf-cl-i16 fftperf stations channels runs device io\n"
		"       perf -- Performance test\n"
		);
}

int main(int argc, char** argv)
{
	int nr_stations = 16;
	int nr_channels = 256;
	int nr_taps     = 16;
	int nr_runs     = 10000;
	int nr_batches  = 1;
	int do_fir      = 1;
	int do_io       = 1;
	int do_fft      = 0;
	int kernel      = 1;

	if(argc < 2)
	{
		usage();
	}
	else if( !strcmp(argv[1], "test") && argc >= 3 )
	{
		kernel = atoi(argv[2]);
		test2(kernel);
	}
	else if( !strcmp(argv[1], "test2") )
	{
		test();
	}
	else if( !strcmp(argv[1], "platforms") )
	{
		platforms();
	}
	else if( !strcmp(argv[1], "devices") )
	{
		devices();
	}
	else if( !strcmp(argv[1], "perf") && argc >= 11 )
	{
		nr_stations = atoi(argv[2]);
		nr_channels = atoi(argv[3]);
		nr_taps     = atoi(argv[4]);
		nr_runs     = atoi(argv[5]);
		nr_batches  = atoi(argv[6]);
		kernel      = atoi(argv[7]);
		do_io       = atoi(argv[8]);
		do_fir      = atoi(argv[9]);
		do_fft      = atoi(argv[10]);
		perf_test(nr_stations, nr_channels, nr_taps, nr_runs, nr_batches, do_fir, do_fft, do_io, kernel);
	}
	else if( !strcmp(argv[1], "fftperf") && argc >= 7)
	{
		nr_stations = atoi(argv[2]);
		nr_channels = atoi(argv[3]);
		nr_runs     = atoi(argv[4]);
		kernel      = atoi(argv[5]);
		do_io       = atoi(argv[6]);
		fft_perf(nr_stations, nr_channels, nr_runs, kernel, do_io);
	}
	else
	{
		usage();
	}
	
	return 0;
}