/*
    A Polyphase Filter for GPUs and Multi-Core Processors
    Copyright (C) 2012 Karel van der Veldt (karel.vd.veldt@uva.nl)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <memory.h>
#include <math.h>
#include <omp.h>
#include <fftw3.h>
#include "timer.h"
#include "ppf.h"
#include "orig_weights.h"

#if !defined(DATATYPE)
#	error "Definition DATATYPE must be set to 4, 8 or 16!"
#elif DATATYPE == 16
#	define COMPLEX    ppf_complex_i16
#	define INPUT_NEW  ppf_input_new_i16
#	define INPUT_FREE ppf_input_free_i16
#	define FIR_EXEC   ppf_fir_execute_i16
#	define PPF_EXEC   ppf_execute_i16
#elif DATATYPE == 8
#	define COMPLEX    ppf_complex_i8
#	define INPUT_NEW  ppf_input_new_i8
#	define INPUT_FREE ppf_input_free_i8
#	define FIR_EXEC   ppf_fir_execute_i8
#	define PPF_EXEC   ppf_execute_i8
#elif DATATYPE == 4
#	define COMPLEX    ppf_complex_i4
#	define INPUT_NEW  ppf_input_new_i4
#	define INPUT_FREE ppf_input_free_i4
#	define FIR_EXEC   ppf_fir_execute_i4
#	define PPF_EXEC   ppf_execute_i4
#else
#	error "Unimplemented DATATYPE."
#endif

void make_input(COMPLEX* input, int nr_channels)
{
	// make up some input data
	for(int i = 0; i < nr_channels; ++i, input += PPF_NR_POLARIZATIONS)
	{
		float a = (float)i;
#if DATATYPE == 4
		ppf_c_real(input[0]) =  7.0 * sinf( 2.0 * M_PI * (a / 256.0) );
		ppf_c_imag(input[0]) =  7.0 * cosf( 2.0 * M_PI * (a / 256.0) );
#else
		ppf_c_real(input[0]) =  100.0 * sinf( 2.0 * M_PI * (a / 256.0) ); // polarization 0
		ppf_c_imag(input[0]) =  100.0 * cosf( 2.0 * M_PI * (a / 256.0) );
#endif
		ppf_c_real(input[1]) =  ppf_c_real(input[0]); //sinf( 2.0 * M_PI * (a / 256.0) ); // polarization 1
		ppf_c_imag(input[1]) = -ppf_c_imag(input[0]); //-cosf( 2.0 * M_PI * (a / 256.0) );
	}
}

int roundup(int n, int m)
{
	int t = n % m;
	return t == 0 ? n : n + (m - t);
}

void perf_test(int nr_stations, int nr_channels, int nr_taps, int nr_threads, int nr_runs, int do_fft)
{
	nr_runs = roundup(nr_runs, nr_taps);
	COMPLEX* input          = INPUT_NEW(nr_stations, nr_channels);
	ppf_complex_f32* output = ppf_output_new(nr_stations, nr_channels);
	ppf_fir* fir            = ppf_fir_new(nr_stations, nr_channels, nr_taps);
	ppf_fft* fft            = ppf_fft_new(nr_stations, nr_channels, do_fft == 2 ? 0 : nr_threads);
	double cpu_mhz          = get_cpu_mhz();
	double secs             = 0.0;
	
	omp_set_num_threads(nr_threads);

	/*for(int i = 0; i < nr_stations; ++i)
	{
		make_input( input + ppf_input_nth(i, nr_channels), nr_channels );
	}*/

	for(int i = 0; i < nr_runs; ++i)
	{
		ticks t0 = get_ticks();
		
		switch(do_fft)
		{
		case 0:
			FIR_EXEC(fir, ppf_orig_weights, (const COMPLEX*)input, output);
			break;
		case 1:
			FIR_EXEC(fir, ppf_orig_weights, (const COMPLEX*)input, output);
			ppf_fft_execute(fft, output);
			break;
		case 2:
			PPF_EXEC(fir, fft, ppf_orig_weights, (const COMPLEX*)input, output);
			break;
		case 3:
			ppf_fft_execute(fft, output);
			break;
		}

		secs += ticks_to_secs( get_ticks() - t0, cpu_mhz );
	}

	double io_bytes = 0;
	double io_gb    = 0;
	
	double fir_bytes = 0;
	double fir_flop  = 0;
	double fir_ai    = 0;
	double fir_gb    = 0;
	double fir_gflop = 0;
	
	if(do_fft != 3)
	{
		// what comes in + comes out of the fir
		io_bytes = PPF_NR_POLARIZATIONS * (sizeof(COMPLEX) + sizeof(ppf_complex_f32));
		io_gb    = nr_stations * nr_channels * io_bytes * (nr_runs / (1024.0 * 1024.0 * 1024.0));
		
		// fir, what it actually processes
		// r 1 x sample
		// w 1 x f32*2 output
		// r nr_taps x f32 weights (once for both polarizations)
		// r nr_taps - 1 x f32*2 taps
		// w 1 x f32*2 tap
		fir_bytes = PPF_NR_POLARIZATIONS * sizeof(ppf_complex_f32) * (nr_taps - 1 + 1) + sizeof(float) * nr_taps;
		fir_flop  = 4 + 8 * (nr_taps - 1);
		fir_ai    = fir_flop / (fir_bytes + io_bytes);
		
		fir_gb    = nr_stations * nr_channels * fir_bytes * (nr_runs / (1024.0 * 1024.0 * 1024.0));
		fir_gflop = nr_stations * nr_channels * fir_flop  * (nr_runs / 1000000000.0);
	}

	// fft
	double fft_ai = 0;
	double fft_gflop = 0;
	double fft_gb = 0;

	if(do_fft)
	{
		// FFT operations = 5nlog2(n)
		// FFT inputs+outputs = 4n
		
		//double adds, muls, fmas;
		//fftwf_flops(fft->plan, &adds, &muls, &fmas);
		double fft_flop  = 5.0 * nr_channels * (log(nr_channels) / log(2)); //adds + muls + 2 * fmas;
		double fft_bytes = 4.0 * nr_channels * sizeof(ppf_complex_f32);
		fft_ai = fft_flop / fft_bytes;
		fft_gflop = fft_flop * fft->nr_plans * fft->nr_stations_per_plan * (nr_runs / 1000000000.0);
		// count input+output bytes
		fft_gb = 2 * fft_bytes * fft->nr_plans * fft->nr_stations_per_plan * ( nr_runs / (1024.0 * 1024.0 * 1024.0) );
	}

	printf("Stat Chan Taps Runs GB  Time(s) GB/s GFLOP/s AI Thrd\n");
	printf( "% 4d % 4d % 4d %d %g "
		"% 2.4g % 1.2g % 2.4g % 1.2g "
		"% 4d\n",
		nr_stations, nr_channels, nr_taps, nr_runs, io_gb + fir_gb + fft_gb,
		secs, (io_gb + fir_gb + fft_gb) / secs, (fir_gflop + fft_gflop) / secs, fir_ai + fft_ai,
		nr_threads);

	ppf_fir_free(fir);
	ppf_fft_free(fft);
	INPUT_FREE(input);
	ppf_output_free(output);
}

void perf_fft(int nr_stations, int nr_channels, int nr_runs)
{
	//omp_set_num_threads(4);
	ppf_complex_f32* output = ppf_output_new(nr_stations, nr_channels);
	ppf_fft* fft            = ppf_fft_new(nr_stations, nr_channels, 4);
	double cpu_mhz          = get_cpu_mhz();
	double secs             = 0;
	
	for(int i = 0; i < nr_runs; ++i)
	{
		ticks t0 = get_ticks();
		ppf_fft_execute(fft, output);
		secs += ticks_to_secs( get_ticks() - t0, cpu_mhz );
	}
	
	double fft_flop  = 5.0 * nr_channels * (log(nr_channels) / log(2));
	double fft_gflop = fft_flop * fft->nr_plans * fft->nr_stations_per_plan * (nr_runs / 1000000000.0);
	
	printf("Stat Chan Runs Time T/Run GFLOP/s\n");
	printf("% 4d % 4d %4d %g %g %g\n", nr_stations, nr_channels, nr_runs, secs, secs / nr_runs, fft_gflop / secs);
	
	ppf_output_free(output);
	ppf_fft_free(fft);
}

void print_delay_line(ppf_complex_f32* delay, int nr_taps, ppf_complex_f32* output)
{
	for(int i = 0; i < nr_taps; ++i)
	{
		printf( "% 3.0f ", ppf_c_real(delay[0]) );
		delay += PPF_NR_POLARIZATIONS;
	
	}

	printf( "| %.3f % 2.3f\n", ppf_c_real(output[0]), ppf_c_imag(output[1]) );
}

void moving_avg_test()
{
	const float weights[] = {0.0625, 0.0625, 0.0625, 0.0625, 0.0625, 0.0625, 0.0625, 0.0625,
							 0.0625, 0.0625, 0.0625, 0.0625, 0.0625, 0.0625, 0.0625, 0.0625};
	const int nr_stations = 1;
	const int nr_channels = 1;
	const int nr_taps     = 16;

	ppf_fir* fir            = ppf_fir_new(nr_stations, nr_channels, nr_taps);
	COMPLEX* input          = INPUT_NEW(nr_stations, nr_channels);
	ppf_complex_f32* output = ppf_output_new(nr_stations, nr_channels);

	omp_set_num_threads(1);

	print_delay_line( ppf_fir_nth_delay(fir, 0, 0), nr_taps, output );

	ppf_c_real(input[0]) = 1;
	ppf_c_imag(input[1]) = 1;

	for(int i = 0; i < 32; ++i)
	{
		FIR_EXEC(fir, weights, (const COMPLEX*)input, output);
		print_delay_line( ppf_fir_nth_delay(fir, 0, 0), nr_taps, output );

		ppf_c_real(input[0]) += 1;
		ppf_c_imag(input[1]) += 1;
	}

	ppf_fir_free(fir);
	INPUT_FREE(input);
	ppf_output_free(output);
}

void verify_fft()
{
	const int nr_stations = 8;
	const int nr_channels = 4;
	const int nr_threads  = 4;
	ppf_complex_f32* out = ppf_output_new(nr_stations, nr_channels);
	ppf_fft* fft = ppf_fft_new(nr_stations, nr_channels, nr_threads);

	omp_set_num_threads(nr_threads);

	for(int i = 0; i < nr_stations; ++i)
	{
		ppf_complex_f32* ch0 = out + ppf_output_nth(nr_stations, nr_channels, i, 0, 0);
		ppf_complex_f32* ch1 = out + ppf_output_nth(nr_stations, nr_channels, i, 0, 1);
		for(int j = 0; j < nr_channels; ++j)
		{
			ppf_c_real(ch0[0]) = i;
			ppf_c_imag(ch0[0]) = j;
			ppf_c_real(ch1[0]) = -i;
			ppf_c_imag(ch1[0]) = -j;
			ch0++;
			ch1++;
		}
	}

	printf("--- BEFORE FFT ---\n");
	for(int i = 0; i < nr_stations * PPF_NR_POLARIZATIONS; ++i)
	{
		printf("% 3d: ", i);
		ppf_complex_f32* ch0 = out + ppf_output_nth(nr_stations, nr_channels, i/2, 0, i&1);
		for(int j = 0; j < nr_channels; ++j)
		{
			printf( "(% 4.1f,% 4.1f), ", ppf_c_real(ch0[0]), ppf_c_imag(ch0[0]) );
			ch0++;
		}
		printf("\n");
	}


	ppf_fft_execute(fft, out);

	printf("--- AFTER FFT ---\n");
	for(int i = 0; i < nr_stations * PPF_NR_POLARIZATIONS; ++i)
	{
		printf("% 3d: ", i);
		ppf_complex_f32* ch0 = out + ppf_output_nth(nr_stations, nr_channels, i/2, 0, i&1);
		for(int j = 0; j < nr_channels; ++j)
		{
			printf( "(% 4.1f,% 4.1f), ", ppf_c_real(ch0[0]), ppf_c_imag(ch0[0]) );
			ch0++;
		}
		printf("\n");
	}

	fftwf_plan inv_fft = fftwf_plan_many_dft(1, &nr_channels, nr_stations * PPF_NR_POLARIZATIONS,
		NULL, NULL, 1, nr_channels, NULL, NULL, 1, nr_channels, FFTW_BACKWARD, FFTW_ESTIMATE);

	fftwf_execute_dft(inv_fft, (fftwf_complex*)out, (fftwf_complex*)out);

	printf("--- AFTER INV FFT ---\n");
	for(int i = 0; i < nr_stations * PPF_NR_POLARIZATIONS; ++i)
	{
		printf("% 3d: ", i);
		ppf_complex_f32* ch0 = out + ppf_output_nth(nr_stations, nr_channels, i/2, 0, i&1);
		for(int j = 0; j < nr_channels; ++j)
		{
			printf( "(% 4.1f,% 4.1f), ", ppf_c_real(ch0[0]), ppf_c_imag(ch0[0]) );
			ch0++;
		}
		printf("\n");
	}

	ppf_output_free(out);
	ppf_fft_free(fft);
	fftwf_free(inv_fft);
}

static int print_output(const ppf_complex_f32* out, int nr_stations, int nr_channels)
{
	static const char* ps[] = {"X", "Y"};
	
	for(int st = 0; st < nr_stations; ++st)
	{
		for(int po = 0; po < PPF_NR_POLARIZATIONS; ++po)
		{
			printf("%d%s: ", st, ps[po]);
			for(int ch = 0; ch < nr_channels; ++ch)
			{
				//unsigned int index = ppf_output_nth(nr_stations, nr_channels, st, ch, po);
				printf("%.2g %.2g, ", ppf_c_real(out[0]), ppf_c_imag(out[0]));
				out++;
			}
			printf("\n");
		}
	}
}

void fft_test2()
{
	int nr_stations = 2;
	int nr_channels = 8;
	int nr_taps     = 4;
	int nr_threads  = 1;
	
	ppf_complex_f32* out = ppf_output_new(nr_stations, nr_channels);
	ppf_fft* fft = ppf_fft_new(nr_stations, nr_channels, nr_threads);
	
	omp_set_num_threads(nr_threads);
	
	ppf_complex_f32* p = out;
	
	for(int st = 1; st <= nr_stations; ++st)
	{
		for(int po = 0; po < PPF_NR_POLARIZATIONS; ++po)
		{
			for(int ch = 1; ch <= nr_channels; ++ch)
			{
				//unsigned int idx = ppf_output_nth(nr_stations, nr_channels, st - 1, ch - 1, po);
				ppf_c_real(p[0]) = st * ch;
				ppf_c_imag(p[0]) = ch + po;
				p++;
			}
		}
	}
	
	printf("Input:\n");
	print_output(out, nr_stations, nr_channels);
	
	for(int i = 0; i < 1; ++i)
	{
		ppf_fft_execute(fft, out);
	}
	
	printf("Output:\n");
	print_output(out, nr_stations, nr_channels);
}


void usage()
{
#ifdef REF_IMPL
#	define EXENAME "ppf-cpu-test-ref"
#else
#	define EXENAME "ppf-cpu-test"
#endif

	printf(
		"Polyphase filter CPU "
#ifdef REF_IMPL
		"reference implementation\n"
#else
		"optimized implementation\n"
#endif
			"usage: " EXENAME "-i%d perf [stations channels taps runs threads dofft]\n"
			"       " EXENAME "-i%d fir\n"
			"       " EXENAME "-i%d fft\n"
			"       " EXENAME "-i%d fftperf stations channels runs\n"
			"modes: perf -- gather performance statistics (fft is off by default)\n"
			"               dofft: 0=no fft, 1=fir&fft separate, 2=combined, 3=fft only\n"
			"       fir  -- moving average test\n"
			"       fft  -- verify fft correctness\n"
			"       fftperf  -- fft perf test\n",
			DATATYPE, DATATYPE, DATATYPE, DATATYPE
			);
}

int main(int argc, char** argv)
{
	int nr_stations = 4;
	int nr_channels = 256;
	int nr_taps = 16;
	int nr_threads = 4;
	int nr_runs = 1000000;
	int do_fft = 0;

#if PPF_FFTW_WITH_THREADS
	fftwf_init_threads();
#endif

	if( argc >= 2 )
	{
		if( strcmp(argv[1], "perf") == 0 )
		{
			if(argc >= 3) nr_stations = atoi(argv[2]);
			if(argc >= 4) nr_channels = atoi(argv[3]);
			if(argc >= 5) nr_taps     = atoi(argv[4]);
			if(argc >= 6) nr_runs     = atoi(argv[5]);
			if(argc >= 7) nr_threads  = atoi(argv[6]);
			if(argc >= 8) do_fft      = atoi(argv[7]);

			perf_test(nr_stations, nr_channels, nr_taps, nr_threads, nr_runs, do_fft);
		}
		else if( strcmp(argv[1], "fir") == 0 )
		{
			moving_avg_test();
		}
		else if( strcmp(argv[1], "fft") == 0 )
		{
			verify_fft();
		}
		else if( strcmp(argv[1], "fft2") == 0 )
		{
			fft_test2();
		}
		else if( strcmp(argv[1], "fftperf") == 0 && argc >= 5 )
		{
			nr_stations = atoi(argv[2]);
			nr_channels = atoi(argv[3]);
			nr_runs     = atoi(argv[4]);
			perf_fft(nr_stations, nr_channels, nr_runs);
		}
		else
		{
			usage();
		}
	}
	else
	{
		usage();
	}

#if PPF_FFTW_WITH_THREADS
	fftwf_cleanup_threads();
#endif

	return 0;
}
