/*
    A Polyphase Filter for GPUs and Multi-Core Processors
    Copyright (C) 2012 Karel van der Veldt (karel.vd.veldt@uva.nl)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <memory.h>
#include <mmintrin.h>
#include <emmintrin.h>
#include <xmmintrin.h>
#include <omp.h>
#include "ppf.h"


ppf_fir* ppf_fir_new(int nr_stations, int nr_channels, int nr_taps)
{
	int delay_len = nr_channels * PPF_NR_POLARIZATIONS * nr_taps;
	ppf_fir* fir = ppf_malloc(ppf_fir, nr_stations);
	ppf_complex_f32* RESTRICT delay   = ppf_malloc(ppf_complex_f32, nr_stations * delay_len);

	memset(delay, 0, sizeof(ppf_complex_f32) * nr_stations * delay_len);

	fir->nr_stations = nr_stations;
	fir->nr_channels = nr_channels;
	fir->nr_taps     = nr_taps;
	fir->delay_index = 0;
	fir->delay       = delay;

	return fir;
}

void ppf_fir_free(ppf_fir* fir)
{
	ppf_free(fir->delay);
	ppf_free(fir);
}

#define PPF_INPUT_NEW(type) ppf_complex_##type * ppf_input_new_##type(int nr_stations, int nr_channels)\
{\
	return ppf_malloc(ppf_complex_##type, nr_stations * nr_channels * PPF_NR_POLARIZATIONS);\
}

#define PPF_INPUT_FREE(type) void ppf_input_free_##type(ppf_complex_##type * input)\
{\
	ppf_free(input);\
}

PPF_INPUT_NEW(i4);
PPF_INPUT_NEW(i8);
PPF_INPUT_NEW(i16);
PPF_INPUT_NEW(f32);

PPF_INPUT_FREE(i4);
PPF_INPUT_FREE(i8);
PPF_INPUT_FREE(i16);
PPF_INPUT_FREE(f32);


ppf_complex_f32* ppf_output_new(int nr_stations, int nr_channels)
{
	return ppf_malloc(ppf_complex_f32, nr_stations * nr_channels * PPF_NR_POLARIZATIONS);
}

void ppf_output_free(ppf_complex_f32* output)
{
	ppf_free(output);
}


ppf_fft* ppf_fft_new(int nr_stations, int nr_channels, int nr_threads)
{
	int nr_stations_per_plan = nr_threads ? nr_stations / nr_threads : 1;
	//int nr_threads = nr_stations / nr_stations_per_plan;
	ppf_fft* fft = ppf_malloc(ppf_fft, 1);
	fft->nr_stations_per_plan = nr_stations_per_plan;
	fft->nr_channels = nr_channels;
	fft->nr_plans    = (nr_threads ? nr_threads : nr_stations) * PPF_NR_POLARIZATIONS;

	if( nr_threads )
	{
#if PPF_FFTW_WITH_THREADS
		fftwf_plan_with_nthreads(nr_threads);
		fft->plan = fftwf_plan_many_dft(1, &nr_channels, nr_stations * PPF_NR_POLARIZATIONS, NULL, NULL, 1, nr_channels,
				NULL, NULL, 1, nr_channels, FFTW_FORWARD, FFTW_ESTIMATE);
#else
		fft->plan = fftwf_plan_many_dft(1, &nr_channels, nr_stations_per_plan, NULL, NULL, 1, nr_channels,
			NULL, NULL, 1, nr_channels, FFTW_FORWARD, FFTW_ESTIMATE);
#endif
	}
	else
	{
		fft->plan = fftwf_plan_dft_1d(nr_channels, NULL, NULL, FFTW_FORWARD, FFTW_ESTIMATE);
	}

	return fft;
}

void ppf_fft_free(ppf_fft* fft)
{
	fftwf_destroy_plan(fft->plan);
	ppf_free(fft);
}

void ppf_fft_execute(const ppf_fft* fft, ppf_complex_f32* inout)
{
#if PPF_FFTW_WITH_THREADS
	fftwf_execute_dft(fft->plan, inout, inout);
#else
#	pragma omp parallel for if(fft->nr_plans >= 2)
	for(int i = 0; i < fft->nr_plans; ++i)
	{
		fftwf_complex* io = (fftwf_complex*)(inout + i * fft->nr_stations_per_plan * fft->nr_channels);
		fftwf_execute_dft(fft->plan, io, io);
	}
#endif
}

// complex arithmetic:
// (a + bi) + (c + di) = (a + c) + (b + d)i
// (a + bi) * (c + di) = (ac - bd) + (bc + ad)i
// (a + bi) * c = (a + bi) * (c + 0i) = (ac + bci)


/**********************************************************
 *	REFERENCE IMPLEMENTATION
 **********************************************************/
#ifdef REF_IMPL
static inline void ppf_fir_inner_loop(int nr_taps, int delay_index, const float* RESTRICT weights,
	ppf_complex_f32* RESTRICT delay, ppf_complex_f32* RESTRICT output0, ppf_complex_f32* RESTRICT output1, ppf_complex_f32* RESTRICT sample)
{
	/*
	 *	The delay_index field points to the 0th delay and counts down (and wraps around) after each run.
	 *	This way no data needs to be copied when tap data becomes older.
	 *
	 *	Computation: multiply-add samples starting from pointer to the right,
	 *	then wrap to beginning and multiply-add the samples before the pointer.
	 *  init  , 1st   , 2nd   , 3rd   , 4th sample
	 *	0 0 0 , 0 0 1 , 0 2 1 , 3 2 1 , 3 2 4 , ...
	 *	^     ,     ^ ,   ^   , ^     ,     ^
	 *
	 *	In the delay line samples from both polarizations of a channel are stored side by side:
	 *	sample0pol0r sample0pol0i sample0pol1r sample0pol1i sample1pol0r sample1pol0i sample1pol1r sample1pol1i ...
	 */
	
	ppf_complex_f32* RESTRICT delay0 = delay + delay_index * PPF_NR_POLARIZATIONS;
	
	float sum0_real = ppf_c_real(sample[0]); // polarization 0
	float sum0_imag = ppf_c_imag(sample[0]);
	float sum1_real = ppf_c_real(sample[1]); // polarization 1
	float sum1_imag = ppf_c_imag(sample[1]);

	// store the new input sample in the delay line
	ppf_c_real(delay0[0]) = sum0_real; // polarization 0
	ppf_c_imag(delay0[0]) = sum0_imag;
	ppf_c_real(delay0[1]) = sum1_real; // polarization 1
	ppf_c_imag(delay0[1]) = sum1_imag;
	delay0 += PPF_NR_POLARIZATIONS;

	// delay # 0
	float w = *weights++;
	sum0_real *= w;
	sum0_imag *= w;
	sum1_real *= w;
	sum1_imag *= w; // 4 fp ops

	// delay # 1 .. delay_index-1
	for(int i = delay_index + 1; i < nr_taps; ++i, delay0 += PPF_NR_POLARIZATIONS)
	{
		w = *weights++;
		sum0_real += w * ppf_c_real(delay0[0]);
		sum0_imag += w * ppf_c_imag(delay0[0]);
		sum1_real += w * ppf_c_real(delay0[1]);
		sum1_imag += w * ppf_c_imag(delay0[1]); // 8 fp ops
	}

	// delay # delay_index .. nr_taps-1
	delay0 = delay;
	for(int i = 0; i < delay_index; ++i, delay0 += PPF_NR_POLARIZATIONS)
	{
		w = *weights++;
		sum0_real += w * ppf_c_real(delay0[0]);
		sum0_imag += w * ppf_c_imag(delay0[0]);
		sum1_real += w * ppf_c_real(delay0[1]);
		sum1_imag += w * ppf_c_imag(delay0[1]); // 8 fp ops
	}
	// 8 fp ops * nr_taps-1

	ppf_c_real(output0[0]) = sum0_real; // polarization 0
	ppf_c_imag(output0[0]) = sum0_imag;
	ppf_c_real(output1[0]) = sum1_real; // polarization 1
	ppf_c_imag(output1[0]) = sum1_imag;

}


static inline void load_i4(const ppf_complex_i4* RESTRICT input, ppf_complex_f32* RESTRICT sample)
{
	ppf_c_real(sample[0]) = (float)ppf_c_real(input[0]);
	ppf_c_imag(sample[0]) = (float)ppf_c_imag(input[0]);
	ppf_c_real(sample[1]) = (float)ppf_c_real(input[1]);
	ppf_c_imag(sample[1]) = (float)ppf_c_imag(input[1]);
}

static inline void load_i8(const ppf_complex_i8* RESTRICT input, ppf_complex_f32* RESTRICT sample)
{
	ppf_c_real(sample[0]) = (float)ppf_c_real(input[0]);
	ppf_c_imag(sample[0]) = (float)ppf_c_imag(input[0]);
	ppf_c_real(sample[1]) = (float)ppf_c_real(input[1]);
	ppf_c_imag(sample[1]) = (float)ppf_c_imag(input[1]);
}

static inline void load_i16(const ppf_complex_i16* RESTRICT input, ppf_complex_f32* RESTRICT sample)
{
	ppf_c_real(sample[0]) = (float)ppf_c_real(input[0]);
	ppf_c_imag(sample[0]) = (float)ppf_c_imag(input[0]);
	ppf_c_real(sample[1]) = (float)ppf_c_real(input[1]);
	ppf_c_imag(sample[1]) = (float)ppf_c_imag(input[1]);
}

#define ppf_fir_inner_outer_loop(TYPE, CONVERT)\
		const TYPE* RESTRICT input0       = input + ppf_input_nth(station, nr_channels);\
		ppf_complex_f32* RESTRICT delay   = ppf_fir_nth_delay(fir, station, 0);\
		ppf_complex_f32* RESTRICT output0 = output + ppf_output_nth(nr_stations, nr_channels, station, 0, 0);\
		ppf_complex_f32* RESTRICT output1 = output + ppf_output_nth(nr_stations, nr_channels, station, 0, 1);\
		ppf_complex_f32 sample[2];\
		const float* RESTRICT w  = weights;\
		for(int channel = 0; channel < nr_channels; ++channel)\
		{\
			CONVERT(input0, sample);\
			ppf_fir_inner_loop(nr_taps, delay_index, w, delay, output0, output1, sample);\
			w       += nr_taps;\
			input0  += PPF_NR_POLARIZATIONS;\
			delay   += nr_taps * PPF_NR_POLARIZATIONS;\
			output0 += 1;\
			output1 += 1;\
		}




/**********************************************************
 *	OPTIMIZED SSE CODE
 **********************************************************/
#else

/**
 *	The inner loop for SSE is the same for all input sample formats.
 */
static inline void ppf_fir_inner_loop(int nr_taps, int delay_index, const float* RESTRICT weights,
	ppf_complex_f32* RESTRICT delay, ppf_complex_f32* RESTRICT output0, ppf_complex_f32* RESTRICT output1,  __m128 msum0)
{
	ppf_complex_f32* RESTRICT delay0 = delay + delay_index * PPF_NR_POLARIZATIONS;
	__m128 mwght, mtmp0;

	// store the samples in the delay line
	_mm_store_ps((float* RESTRICT)delay0, msum0);

	// delay # 0
	mwght = _mm_set1_ps(*weights++);
	msum0 = _mm_mul_ps(msum0, mwght); // 4 fp ops
	delay0 += PPF_NR_POLARIZATIONS;

	// delay # 1 .. delay_index-1
#ifndef UNROLL_LOOPS
	for(int i = delay_index + 1; i < nr_taps; ++i)
	{
		mwght = _mm_set1_ps(*weights++);
		mtmp0 = _mm_load_ps((const float* RESTRICT)delay0);
		mtmp0 = _mm_mul_ps(mtmp0, mwght); // 4 fp ops
		msum0 = _mm_add_ps(msum0, mtmp0); // 4 fp ops
		delay0 += PPF_NR_POLARIZATIONS;
	}
#else
	int i = delay_index + 1;
	if( i & 1 )
	{
		mwght = _mm_set1_ps(*weights++);
		mtmp0 = _mm_load_ps((const float* RESTRICT)delay0);
		mtmp0 = _mm_mul_ps(mtmp0, mwght);
		msum0 = _mm_add_ps(msum0, mtmp0);
		delay0 += PPF_NR_POLARIZATIONS;
		i++;
	}

	for(; i < nr_taps; i += 2)
	{
		mwght = _mm_set1_ps(*weights++);
		mtmp0 = _mm_load_ps((const float* RESTRICT)delay0);
		mtmp0 = _mm_mul_ps(mtmp0, mwght);
		msum0 = _mm_add_ps(msum0, mtmp0);
		delay0 += PPF_NR_POLARIZATIONS;

		mwght = _mm_set1_ps(*weights++);
		mtmp0 = _mm_load_ps((const float* RESTRICT)delay0);
		mtmp0 = _mm_mul_ps(mtmp0, mwght);
		msum0 = _mm_add_ps(msum0, mtmp0);
		delay0 += PPF_NR_POLARIZATIONS;
	}
#endif

	// delay # delay_index .. nr_taps-1
	delay0 = delay;

#ifndef UNROLL_LOOPS
	for(int i = 0; i < delay_index; ++i)
	{
		mwght = _mm_set1_ps(*weights++);
		mtmp0 = _mm_load_ps((const float* RESTRICT)delay0);
		mtmp0 = _mm_mul_ps(mtmp0, mwght); // 4 fp ops
		msum0 = _mm_add_ps(msum0, mtmp0); // 4 fp ops
		delay0 += PPF_NR_POLARIZATIONS;
	}
#else
	for(int i = 0; i < (delay_index & ~1); i += 2)
	{
		mwght = _mm_set1_ps(*weights++);
		mtmp0 = _mm_load_ps((const float* RESTRICT)delay0);
		mtmp0 = _mm_mul_ps(mtmp0, mwght);
		msum0 = _mm_add_ps(msum0, mtmp0);
		delay0 += PPF_NR_POLARIZATIONS;

		mwght = _mm_set1_ps(*weights++);
		mtmp0 = _mm_load_ps((const float* RESTRICT)delay0);
		mtmp0 = _mm_mul_ps(mtmp0, mwght);
		msum0 = _mm_add_ps(msum0, mtmp0);
		delay0 += PPF_NR_POLARIZATIONS;
	}

	if( delay_index & 1 )
	{
		mwght = _mm_set1_ps(*weights++);
		mtmp0 = _mm_load_ps((const float* RESTRICT)delay0);
		mtmp0 = _mm_mul_ps(mtmp0, mwght);
		msum0 = _mm_add_ps(msum0, mtmp0);
	}
#endif

	_mm_storel_pi((__m64* RESTRICT)output0, msum0); // polarization 0
	_mm_storeh_pi((__m64* RESTRICT)output1, msum0); // polarization 1
}

static inline __m128 load_i4(const ppf_complex_i4* RESTRICT input)
{
	// The order of imag, real, imag, real is correct!
	__m64  a = _mm_set_pi16( ppf_c_imag(input[0]), ppf_c_real(input[0]), ppf_c_imag(input[1]), ppf_c_real(input[1]) );
	__m128 b = _mm_cvtpi16_ps(a);
	_mm_empty();
	return b;
}

static inline __m128 load_i8(const ppf_complex_i8* RESTRICT input)
{
	__m128 a = _mm_cvtpi8_ps( *((const __m64* RESTRICT)input) );
	return _mm_shuffle_ps(a, a, _MM_SHUFFLE(1, 0, 3, 2) );
}

static inline __m128 load_i16(const ppf_complex_i16* RESTRICT input)
{
	__m128 a = _mm_cvtpi16_ps( *((const __m64* RESTRICT)input) );
	return _mm_shuffle_ps(a, a, _MM_SHUFFLE(1, 0, 3, 2) );
}

static inline __m128 load_f32(const ppf_complex_f32* RESTRICT input)
{
	return _mm_load_ps( (const float* RESTRICT)input );
}

#define ppf_fir_inner_outer_loop(TYPE, CONVERT)\
		const TYPE* RESTRICT input0       = input + ppf_input_nth(station, nr_channels);\
		ppf_complex_f32* RESTRICT delay   = ppf_fir_nth_delay(fir, station, 0);\
		ppf_complex_f32* RESTRICT output0 = output + ppf_output_nth(nr_stations, nr_channels, station, 0, 0);\
		ppf_complex_f32* RESTRICT output1 = output + ppf_output_nth(nr_stations, nr_channels, station, 0, 1);\
		const float*  w  = weights;\
		for(int channel = 0; channel < nr_channels; ++channel)\
		{\
			__m128 msum0 = CONVERT(input0);\
			ppf_fir_inner_loop(nr_taps, delay_index, w, delay, output0, output1, msum0);\
			w       += nr_taps;\
			input0  += PPF_NR_POLARIZATIONS;\
			delay   += nr_taps * PPF_NR_POLARIZATIONS;\
			output0 += 1;\
			output1 += 1;\
		}


#endif


void ppf_fir_execute_i4(ppf_fir* fir, const float* RESTRICT weights, const ppf_complex_i4* RESTRICT input, ppf_complex_f32* RESTRICT output)
{
	int nr_stations = fir->nr_stations;
	int nr_channels = fir->nr_channels;
	int nr_taps     = fir->nr_taps;
	int delay_index = (fir->delay_index - 1) & (nr_taps - 1);

#	pragma omp parallel for if(nr_stations >= 2)
	for(int station = 0; station < nr_stations; ++station)
	{
		ppf_fir_inner_outer_loop(ppf_complex_i4, load_i4);
	}

	fir->delay_index = delay_index;
}

void ppf_fir_execute_i8(ppf_fir* fir, const float* RESTRICT weights, const ppf_complex_i8* RESTRICT input, ppf_complex_f32* RESTRICT output)
{
	int nr_stations = fir->nr_stations;
	int nr_channels = fir->nr_channels;
	int nr_taps     = fir->nr_taps;
	int delay_index = (fir->delay_index - 1) & (nr_taps - 1);

#	pragma omp parallel for if(nr_stations >= 2)
	for(int station = 0; station < nr_stations; ++station)
	{
		ppf_fir_inner_outer_loop(ppf_complex_i8, load_i8);
	}

	fir->delay_index = delay_index;
}

void ppf_fir_execute_i16(ppf_fir* fir, const float* RESTRICT weights, const ppf_complex_i16* RESTRICT input, ppf_complex_f32* RESTRICT output)
{
	int nr_stations = fir->nr_stations;
	int nr_channels = fir->nr_channels;
	int nr_taps     = fir->nr_taps;
	int delay_index = (fir->delay_index - 1) & (nr_taps - 1);

#	pragma omp parallel for if(nr_stations >= 2)
	for(int station = 0; station < nr_stations; ++station)
	{
		ppf_fir_inner_outer_loop(ppf_complex_i16, load_i16);
	}

	fir->delay_index = delay_index;
}

#if 0
void ppf_fir_execute_f32(ppf_fir* fir, const float* RESTRICT weights, const ppf_complex_f32* RESTRICT input, ppf_complex_f32* RESTRICT output)
{
	int nr_stations = fir->nr_stations;
	int nr_channels = fir->nr_channels;
	int nr_taps     = fir->nr_taps;
	int delay_index = (fir->delay_index - 1) & (nr_taps - 1);

#	pragma omp parallel for if(nr_stations >= 2)
	for(int station = 0; station < nr_stations; ++station)
	{
		ppf_fir_inner_outer_loop(ppf_complex_f32, load_f32);
	}

	fir->delay_index = delay_index;
}
#endif



void ppf_execute_i4(ppf_fir* fir, const ppf_fft* fft, const float* RESTRICT weights, const ppf_complex_i4* RESTRICT input, ppf_complex_f32* RESTRICT output)
{
	int nr_stations = fir->nr_stations;
	int nr_channels = fir->nr_channels;
	int nr_taps     = fir->nr_taps;
	int delay_index = (fir->delay_index - 1) & (nr_taps - 1);

#	pragma omp parallel for if(nr_stations >= 2)
	for(int station = 0; station < nr_stations; ++station)
	{
		ppf_fir_inner_outer_loop(ppf_complex_i4, load_i4);
		output0 = output + ppf_output_nth(nr_stations, nr_channels, station, 0, 0);
		output1 = output + ppf_output_nth(nr_stations, nr_channels, station, 0, 1);
		fftwf_execute_dft(fft->plan, (fftwf_complex* RESTRICT)output0, (fftwf_complex* RESTRICT)output0);
		fftwf_execute_dft(fft->plan, (fftwf_complex* RESTRICT)output1, (fftwf_complex* RESTRICT)output1);
	}

	fir->delay_index = delay_index;
}


void ppf_execute_i8(ppf_fir* fir, const ppf_fft* fft, const float* RESTRICT weights, const ppf_complex_i8* RESTRICT input, ppf_complex_f32* RESTRICT output)
{
	int nr_stations = fir->nr_stations;
	int nr_channels = fir->nr_channels;
	int nr_taps     = fir->nr_taps;
	int delay_index = (fir->delay_index - 1) & (nr_taps - 1);

#	pragma omp parallel for if(nr_stations >= 2)
	for(int station = 0; station < nr_stations; ++station)
	{
		ppf_fir_inner_outer_loop(ppf_complex_i8, load_i8);
		output0 = output + ppf_output_nth(nr_stations, nr_channels, station, 0, 0);
		output1 = output + ppf_output_nth(nr_stations, nr_channels, station, 0, 1);
		fftwf_execute_dft(fft->plan, (fftwf_complex* RESTRICT)output0, (fftwf_complex* RESTRICT)output0);
		fftwf_execute_dft(fft->plan, (fftwf_complex* RESTRICT)output1, (fftwf_complex* RESTRICT)output1);
	}

	fir->delay_index = delay_index;
}


void ppf_execute_i16(ppf_fir* fir, const ppf_fft* fft, const float* RESTRICT weights, const ppf_complex_i16* RESTRICT input, ppf_complex_f32* RESTRICT output)
{
	int nr_stations = fir->nr_stations;
	int nr_channels = fir->nr_channels;
	int nr_taps     = fir->nr_taps;
	int delay_index = (fir->delay_index - 1) & (nr_taps - 1);

#	pragma omp parallel for if(nr_stations >= 2)
	for(int station = 0; station < nr_stations; ++station)
	{
		ppf_fir_inner_outer_loop(ppf_complex_i16, load_i16);
		output0 = output + ppf_output_nth(nr_stations, nr_channels, station, 0, 0);
		output1 = output + ppf_output_nth(nr_stations, nr_channels, station, 0, 1);
		fftwf_execute_dft(fft->plan, (fftwf_complex* RESTRICT)output0, (fftwf_complex* RESTRICT)output0);
		fftwf_execute_dft(fft->plan, (fftwf_complex* RESTRICT)output1, (fftwf_complex* RESTRICT)output1);
	}

	fir->delay_index = delay_index;
}
