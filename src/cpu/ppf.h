/*
    A Polyphase Filter for GPUs and Multi-Core Processors
    Copyright (C) 2012 Karel van der Veldt (karel.vd.veldt@uva.nl)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __PPF_CPU_H
#define __PPF_CPU_H
#include <fftw3.h>

#define RESTRICT restrict


/**	\def ppf_malloc(type, count)
 *	Allocates sizeof(type) * count bytes which are aligned on a 16-byte boundary.
 */
#define ppf_malloc(type, count) ((type*)fftw_malloc(sizeof(type) * (count)))

/** \def ppf_free(p)
 *	Frees aligned memory.
 */
#define ppf_free(p) fftw_free(p)

#define PPF_NR_POLARIZATIONS 2	/**< Polarizations per channel. **/


/**
 *	4-bit integer complex number.
 */
typedef struct __attribute__((packed))
{
	char x : 4;		/*< Real part. */
	char y : 4;		/*< Imaginary part. */
} ppf_complex_i4;

/**
 *	8-bit integer complex number.
 */
typedef struct __attribute__((packed))
{
	char x;			/*< Real part. */
	char y;			/*< Imaginary part. */
} ppf_complex_i8;

/**
 *	16-bit integer complex number.
 */
typedef struct
{
	short x;		/*< Real part. */
	short y;		/*< Imaginary part. */
} ppf_complex_i16;

/**
 *	32-bit fp complex number.
 */
typedef struct
{
	float x;		/*< Real part. */
	float y;		/*< Imaginary part. */
} ppf_complex_f32;

/**
 *	\def ppc_c_real(cplx)
 *	Get the real part of a complex number.
 *	@param cplx		A complex number of type ppf_complex_*.
 */
#define ppf_c_real(cplx) ((cplx).x)

/**
 *	\def ppc_c_imag(cplx)
 *	Get the imaginary part of a complex number.
 *	@param cplx		A complex number of type ppf_complex_*.
 */
#define ppf_c_imag(cplx) ((cplx).y)


/**
 *	FIR data.
 */
typedef struct
{
	int nr_stations;					/**< Number of stations. **/
	int nr_channels;					/**< Number of channels per station. **/
	int nr_taps;						/**< Number of taps per channel. **/
	int delay_index;					/**< Index of the first tap in the delay line. **/
	ppf_complex_f32* RESTRICT delay;	/**< The delay lines for all channels. **/
} ppf_fir;

/**
 *	FFT plan data (for FFTW).
 */
typedef struct
{
	int nr_plans;						/**< How many plans to execute. **/
	int nr_stations_per_plan;			/**< How many stations are computed per plan. **/
	int nr_channels;					/**< Number of channels per station. **/
	fftwf_plan plan;					/**< The FFTW plan. **/
} ppf_fft;

/**
 *	Allocate memory for the FIR delay lines and output.
 *	The output is also the input/output for the FFT.
 *
 *	@param nr_stations		The number of stations.
 *	@param nr_channels		The number of channels per station.
 *	@param nr_taps			The number of taps per channel.
 *	@return					The allocated memory.
 */
ppf_fir* ppf_fir_new(int nr_stations, int nr_channels, int nr_taps);

/**
 *	Free the FIR memory.
 */
void ppf_fir_free(ppf_fir* fir);

/**
 *	Computes the address of the delay line of the given channel.
 */
static inline ppf_complex_f32* ppf_fir_nth_delay(const ppf_fir* fir, int station, int channel)
{
	// ppf_complex_f32 delay[nr_stations][nr_channels][nr_taps][nr_polarizations]
	// delay[s][c][t][p] = s * nr_channels * nr_taps * nr_polarizations + c * nr_taps * nr_polarizations + t * nr_polarizations + p
	//                   = ((s * nr_channels + c) * nr_taps + t) * nr_polarizations + p
	return fir->delay + ((station * fir->nr_channels + channel) * fir->nr_taps + 0) * PPF_NR_POLARIZATIONS + 0;
}

/**
 *	Allocates an input array for the given number of stations and channels.
 */
ppf_complex_i4*  ppf_input_new_i4 (int nr_stations, int nr_channels);
ppf_complex_i8*  ppf_input_new_i8 (int nr_stations, int nr_channels);
ppf_complex_i16* ppf_input_new_i16(int nr_stations, int nr_channels);
ppf_complex_f32* ppf_input_new_f32(int nr_stations, int nr_channels);

/**
 *	Frees the input array.
 */
void ppf_input_free_i4 (ppf_complex_i4*  input);
void ppf_input_free_i8 (ppf_complex_i8*  input);
void ppf_input_free_i16(ppf_complex_i16* input);
void ppf_input_free_f32(ppf_complex_f32* input);

/**
 *	Computes the starting address of the given station input.
 *	@param station		The station.
 *	@param nr_channels	The number of channels per station.
 *	@return				The starting address of the given station input.
 */
//#define ppf_input_nth(input, station, nr_channels) ((input) + (station) * (nr_channels) * PPF_NR_POLARIZATIONS)
static inline unsigned int ppf_input_nth(int station, int nr_channels)
{
	return station * nr_channels * PPF_NR_POLARIZATIONS;
}

/**
 *	Allocates an output array for the given number of stations and channels.
 */
ppf_complex_f32* ppf_output_new(int nr_stations, int nr_channels);

/**
 *	Frees the output array.
 */
void ppf_output_free(ppf_complex_f32* output);

/**
 *	Computes the starting address of the nth station of the given polarization.
 *	@param nr_stations	The total number of stations.
 *	@param nr_channels	The total number of channels per station.
 *	@param station		The nth station.
 *	@param polarization	The polarization.
 */
//#define ppf_output_nth(nr_stations, nr_channels, station, polarization) ( ((station) + ((polarization) * (nr_stations))) * (nr_channels) )
static inline unsigned int ppf_output_nth(int nr_stations, int nr_channels, int station, int channel, int polarization)
{
#if 1
	// out[nr_polarizations][nr_stations][nr_channels]
	return (polarization * nr_stations + station) * nr_channels + channel;
#else
	// out[nr_stations][nr_polarizations][nr_channels]
	// out[s][p][c] = s * nr_polarizations * nr_channels + p * nr_channels + c
	//              = (s * nr_polarizations + p) * nr_channels + c
	return (station * PPF_NR_POLARIZATIONS + polarization) * nr_channels + channel;
#endif
}

/**
 *	Creates FFTW plans for the FIR output for both polarizations.
 *	The plans must be executed with ppf_fft_execute_* if combined == 0, otherwise with ppf_execute_*.
 *
 *	@param nr_stations	Number of stations.
 *	@param nr_channels	Number of channels per station.
 *	@param nr_threads	The number of threads to run. Nr_stations must be divisible by nr_threads! If the value is zero, then the FIR and FFT must be executed with ppf_execute_*.
 *	@return				The FFTW plans.
 */
ppf_fft* ppf_fft_new(int nr_stations, int nr_channels, int nr_threads);

/**
 *	Frees the FFTW plans.
 */
void ppf_fft_free(ppf_fft* fft);

/**
 *	Computes the FFT of the input in-place and in parallel.
 *	@param fft		The FFT plan.
 *	@param inout	The input/output array.
 */
void ppf_fft_execute(const ppf_fft* fft, ppf_complex_f32* inout);

/**
 *	Computes the FIR of all stations and channels in parallel.
 *
 *	The order of elements in the input array is:
 *	chan0pol0r chan0pol0i chan0pol1r chan0pol1i chan1pol0r chan1pol0i chan1pol1r chan1pol1i ...
 *
 *	@param fir			The FIR data.
 *	@param weights		An array of (nr_taps * nr_channels) weights in normal order (not reversed).
 *	@param input		The input array.
 *	@param output		The output is stored here.
 */
void ppf_fir_execute_i4 (ppf_fir* fir, const float* RESTRICT weights, const ppf_complex_i4*  RESTRICT input, ppf_complex_f32* RESTRICT output);
void ppf_fir_execute_i8 (ppf_fir* fir, const float* RESTRICT weights, const ppf_complex_i8*  RESTRICT input, ppf_complex_f32* RESTRICT output);
void ppf_fir_execute_i16(ppf_fir* fir, const float* RESTRICT weights, const ppf_complex_i16* RESTRICT input, ppf_complex_f32* RESTRICT output);
void ppf_fir_execute_f32(ppf_fir* fir, const float* RESTRICT weights, const ppf_complex_f32* RESTRICT input, ppf_complex_f32* RESTRICT output);

/**
 *	Computes the FIR and FFT of all stations and channels in parallel.
 *	This is faster than doing the FIR and FFT seperately.
 *
 *	@param fir			The FIR data.
 *	@param fft			The FFT data.
 *	@param weights		An array of (nr_taps * nr_channels) weights in normal order (not reversed).
 *	@param input		The input array.
 *	@param output		The output is stored here.
 */
void ppf_execute_i4 (ppf_fir* fir, const ppf_fft* fft, const float* RESTRICT weights, const ppf_complex_i4*  RESTRICT input, ppf_complex_f32* RESTRICT output);
void ppf_execute_i8 (ppf_fir* fir, const ppf_fft* fft, const float* RESTRICT weights, const ppf_complex_i8*  RESTRICT input, ppf_complex_f32* RESTRICT output);
void ppf_execute_i16(ppf_fir* fir, const ppf_fft* fft, const float* RESTRICT weights, const ppf_complex_i16* RESTRICT input, ppf_complex_f32* RESTRICT output);

#endif
