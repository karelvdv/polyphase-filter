/*
    A Polyphase Filter for GPUs and Multi-Core Processors
    Copyright (C) 2012 Karel van der Veldt (karel.vd.veldt@uva.nl)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef __TIMER_H
#define __TIMER_H

typedef long long ticks;

static inline ticks get_ticks()
{
	unsigned eax, edx;
	asm volatile ("rdtsc" : "=a" (eax), "=d" (edx));
	return eax | ((ticks)edx << 32);
}

static inline double ticks_to_secs(ticks t0, double cpu_mhz)
{
	return (double)t0 / 1000000.0 / cpu_mhz;
}

extern double get_cpu_mhz();

#endif
